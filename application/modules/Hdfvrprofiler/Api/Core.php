<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Api_Core extends Core_Api_Abstract {

    public function getMyHdfvrprofilers($user_id) {
        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');


        $select = $table->select()
                ->where('id_user = ?', $user_id);

        $result = $table->fetchAll($select);
        return $result;
    }

    public function disableCurrentProfileVideo($user_id) {

        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');

        if ($table->update(array('status' => 0), array('id_user = ?' => $user_id))) {
            return false;
        } else {
            return true;
        }
    }

    public function insertNewProfileVideo($params) {
        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');
        $last = $table->insert(array('id_user' => $params['id_user'], 'filename' => $params['filename'], 'date_recorded' => $params['date_recorded']));

        //TODO pune if aici
        return $last;
    }

    public function getSubjectVideoProfile($user_id) {
        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');
        $select = $table->select()
                ->where('id_user = ?', $user_id)
                ->where('status  = 1');

        $result = $table->fetchAll($select);
        return $result;
    }

    public function getVideoProfileByhdfvrprofilerid($hdfvrprofiler_id) {
        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');
        $select = $table->select()
                ->where('hdfvrprofiler_id = ?', $hdfvrprofiler_id)
                ->where('status  = 1');

        $result = $table->fetchAll($select);
        return $result;
    }

}

