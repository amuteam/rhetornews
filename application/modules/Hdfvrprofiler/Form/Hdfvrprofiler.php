<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Form_Hdfvrprofiler extends Engine_Form
{
	public function init()
	{
		//$this->setTitle('Record Your Profile Video');
    	//$this->setDescription('');
    	$this->setAttrib('class', 'global_form_popup');

		$this->addElement('Hidden', 'stream_name', array(
			'order' => 990,
      		'value' => ''
		));
		$this->addElement('Hidden', 'stream_duration', array(
			'order' => 991,
      		'value' => ''
		));
		
		
		 // Init submit
		$this->addElement('Checkbox', 'submit_with_post', array(
				'label' => 'Post to Wall',
				'value' => '1',
				'type' => 'checkbox',
		));
	    $this->addElement('Button', 'submit_without_post', array(
	      'label' => 'Add Video To My Profile',
	      'type' => 'submit',
	      'style' => 'display:none'
	    ));
	    /*$this->addElement('Cancel', 'cancel', array(
	    		'label' => 'cancel',
	    		'link' => true,
	    		'prependText' => ' or ',
	    		'href' => '',
	    		'onclick' => 'parent.Smoothbox.close();',
	    		'decorators' => array(
	    				'ViewHelper'
	    		)
	    ));
	    */
	    
	    // Set default action
	    $this->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
	       'module' => 'hdfvrprofiler',
	       'controller' => 'index',
	       'action' => 'index',
	    ), 'default'));
	}
	
	
}
?>