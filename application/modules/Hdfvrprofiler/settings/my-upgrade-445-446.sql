DROP PROCEDURE IF EXISTS `rename_table_hdfvr_recordings_if_exists`;

CREATE PROCEDURE `rename_table_hdfvr_recordings_if_exists`(IN dbName tinytext)
BEGIN
IF (SELECT COUNT(*)
    FROM information_schema.tables
    WHERE table_name = 'engine4_hdfvr_recordings'
    ) > 0 THEN 
    		RENAME TABLE `engine4_hdfvr_recordings` TO `engine4_hdfvrprofiler_hdfvrprofilers` ;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'hdfvrprofiler_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` CHANGE `id` `hdfvrprofiler_id` INT( 10 ) NOT NULL AUTO_INCREMENT;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'photo_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN photo_id INT( 10 ) NOT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'owner_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN owner_id INT( 11 ) NOT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'owner_type')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN owner_type VARCHAR( 128 ) NOT NULL ;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'parent_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN parent_id INT( 11 ) DEFAULT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'parent_type')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN parent_type VARCHAR( 128 ) DEFAULT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'duration')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN duration INT( 10 ) DEFAULT NULL ;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'comment_count')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN comment_count INT( 10 ) NOT NULL ;
END IF;
END;


CALL rename_table_hdfvr_recordings_if_exists(Database());


UPDATE `engine4_core_menuitems` SET `label` = 'Video Profile Permissions' WHERE `engine4_core_menuitems`.`label` ='Member Level HDFVR Profiler Sett';

UPDATE `engine4_core_menuitems` SET `label` = 'Video Profile Settings' WHERE `engine4_core_menuitems`.`label` = 'HDFVR Profiler Settings';