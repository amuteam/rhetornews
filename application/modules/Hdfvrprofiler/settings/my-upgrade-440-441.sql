ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN photo_id INT( 10 ) NOT NULL;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN owner_type VARCHAR( 128 ) NOT NULL ;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN parent_id INT( 11 ) DEFAULT NULL;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN parent_type VARCHAR( 128 ) DEFAULT NULL;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN duration INT( 10 ) DEFAULT NULL ;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN comment_count INT( 10 ) NOT NULL ;
