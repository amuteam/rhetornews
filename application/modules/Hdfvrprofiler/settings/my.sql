INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('core_main_hdfvrprofiler', 'hdfvrprofiler', 'Record Video Profile', 'Hdfvrprofiler_Plugin_Menus', '', 'user_home', '', 9),
('core_main_hdfvrprofiler_view', 'hdfvrprofiler', 'View My Video Profile', 'Hdfvrprofiler_Plugin_Menus', '', 'user_home', '', 9),
('core_main_hdfvrprofiler_delete', 'hdfvrprofiler', 'Delete My Video Profile', 'Hdfvrprofiler_Plugin_Menus', '', 'user_home', '', 9),
('core_main_hdfvrprofiler_view_other', 'hdfvrprofiler', 'View Video Profile', 'Hdfvrprofiler_Plugin_Menus', '', 'user_profile', '', 9),
('hdfvrprofiler_admin_main_settings_general', 'hdfvrprofiler', 'Video Profile Settings', '', '{"route":"admin_default","module":"hdfvrprofiler","controller":"settings","action":"general"}', 'core_admin_main_settings', '', ''),
('hdfvrprofiler_admin_main_level', 'hdfvrprofiler', 'Video Profile Permissions', '', '{"route":"admin_default","module":"hdfvrprofiler","controller":"level","action":"edit"}', 'core_admin_main_manage', '', 3);



CREATE TABLE `engine4_hdfvrprofiler_hdfvrprofilers` (
`hdfvrprofiler_id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_user` INT( 10 ) NOT NULL ,
`photo_id` INT( 10 ) NOT NULL ,
`owner_id` INT( 11 ) NOT NULL ,
`owner_type` VARCHAR( 128 ) NOT NULL ,
`parent_id` INT( 11 ) DEFAULT NULL ,
`parent_type` VARCHAR( 128 ) DEFAULT NULL ,
`filename` VARCHAR( 255 ) NOT NULL ,
`date_recorded` DATETIME NOT NULL ,
`duration` INT( 10 ) DEFAULT NULL ,
`comment_count` INT( 10 ) NOT NULL ,
`status` TINYINT( 1 ) NOT NULL DEFAULT '1'
) ENGINE = MYISAM ;


INSERT INTO `engine4_core_settings` (`name`, `value`) VALUES
('hdfvrprofiler.rtmp.connectionstring', 'rtmp://'),
('hdfvrprofiler.buffer.size', '60'),
('hdfvrprofiler.playback.buffer', '30'),
('hdfvrprofiler.quality.profile', 'normal_320x240x30x90');


INSERT IGNORE INTO `engine4_authorization_permissions` (`level_id`, `type`, `name`, `value`, `params`) VALUES
('1', 'hdfvrprofiler', 'hdfvr_allow_rec', '1', 'NULL'),
('2', 'hdfvrprofiler', 'hdfvr_allow_rec', '1', 'NULL'),
('3', 'hdfvrprofiler', 'hdfvr_allow_rec', '1', 'NULL'),
('4', 'hdfvrprofiler', 'hdfvr_allow_rec', '1', 'NULL'),
('5', 'hdfvrprofiler', 'hdfvr_allow_rec', '0', 'NULL'),
('1', 'hdfvrprofiler', 'hdfvr_rectime', '3', '-1'),
('2', 'hdfvrprofiler', 'hdfvr_rectime', '3', '-1'),
('3', 'hdfvrprofiler', 'hdfvr_rectime', '3', '-1'),
('4', 'hdfvrprofiler', 'hdfvr_rectime', '3', '3'),
('5', 'hdfvrprofiler', 'hdfvr_rectime', '3', '0');

INSERT IGNORE INTO `engine4_authorization_permissions` (`level_id`, `type`, `name`, `value`, `params`) VALUES
('1', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('2', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('3', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('4', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('1', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('2', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('3', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('4', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('1', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('2', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('3', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('4', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('1', 'hdfvrprofiler', 'view', '1', 'NULL'),
('2', 'hdfvrprofiler', 'view', '1', 'NULL'),
('3', 'hdfvrprofiler', 'view', '1', 'NULL'),
('4', 'hdfvrprofiler', 'view', '1', 'NULL'),
('5', 'hdfvrprofiler', 'view', '1', 'NULL');


INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`,  `body`,  `enabled`,  `displayable`,  `attachable`,  `commentable`,  `shareable`, `is_generated`) VALUES
('recorded_video_new', 'hdfvrprofiler', '{item:$subject} posted a new profile video:', '1', '5', '1', '3', '1', 0);