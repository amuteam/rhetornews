<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Bootstrap extends Engine_Application_Bootstrap_Abstract
{
	 public function __construct($application){
	 	
	 	parent::__construct($application);
	 	
	 	 // Add view helper and action helper paths
	 	 $this->initViewHelperPath();
    	 $this->initActionHelperPath();
	 }
}