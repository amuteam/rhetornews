<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    HDFVRProfiler
 * @copyright  Copyright 2009-2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchat.net>
 */

/**
 * @category   Application_Extensions
 * @package    HDFVRProfiler
 * @copyright  Copyright 2009-2010 HDFVR.com
 * @license    http://avchat.net/license
 */
?>

<script type="text/javascript">
  var fetchLevelSettings = function(level_id) {
    window.location.href = en4.core.baseUrl + 'admin/hdfvrprofiler/level/edit/id/' + level_id;
    //alert(level_id);
  }
</script>

<h2>
  Video Profile Permissions
</h2>

<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php
      // Render the menu
      //->setUlClass()
      echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<div class='clear'>
  <div class='settings'>
    <?php echo $this->form->render($this) ?>
  </div>
</div>