<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_IndexController extends Core_Controller_Action_Standard {

    public function indexAction() {
        ini_set("gd.jpeg_ignore_warning", 1);
        $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
        $viewer = Engine_Api::_()->user()->getViewer();

        // Get table info
        //$table = Engine_Api::_()->getItemTable('hdfvr_recordings');
        //$searchTable = Engine_Api::_()->fields()->getTable('hdfvr_recordings', '');
        //$hdfvrRecordingsTableName = $table->info('name');

        if ($viewer_id != 0) {
            $this->view->form = $form = new Hdfvrprofiler_Form_Hdfvrprofiler();
            // Check if post and populate
            if (!empty($_GET["hash"])) {
                $_SESSION["skcustome"]["session_hash"] = $_GET["hash"];
            }
            if ($this->getRequest()->isPost()) {
                if ($form->isValid($this->getRequest()->getPost())) {
                    $db = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler')->getAdapter();


                    //Check if the current user has any other recordings in the database
                    $myRecordings = Engine_Api::_()->hdfvrprofiler()->getMyHdfvrprofilers($viewer_id);


                    if (count($myRecordings) != 0) {
                        //Mark with status 0 the current profile video
                        // Engine_Api::_()->hdfvrprofiler()->disableCurrentProfileVideo($viewer_id);
                    }


                    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
                        $values = $form->getValues();

                        $params = array();
                        $params['filename'] = $values['stream_name'];
                        $params['duration'] = $values['stream_duration'];
                        $params['id_user'] = $viewer_id;
                        $params['date_recorded'] = date('Y-m-d h:i:s');
                        $params['owner_id'] = $viewer->getIdentity();




                        $db->beginTransaction();
                        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');
                        $recording = $table->createRow();

                        $recording->setFromArray($params);
                        $recording->save();
//                        echo "recording=>";
//                        echo "id=>" . $recording->hdfvrprofiler_id;
//                        var_dump($recording);
//                        echo '<pre>';
//                        print_r($recording);
//                        echo '</pre>';
//                        die();
                        // if (!empty($_GET["hash"])) {
                        //  $_SESSION["skcustome"]["session_hash"] = $_GET["hash"];
                        $_SESSION["skcustome"]["hdfvrprofiler_id_last"] = $recording->hdfvrprofiler_id;
                        //}
                        ///////////////////////////////// Thumbnail
                        $thumbnail = APPLICATION_PATH . '/application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/snapshots/' . $values['stream_name'];
                        //$thumbnail = '/application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/snapshots/'.$values['stream_name'];

                        $tmp_file = $thumbnail . "_tmp.jpg";
                        $thumb_file = $thumbnail . "_thumb.jpg";


                        $src_fh = fopen($thumbnail . ".jpg", 'r');
                        $tmp_fh = fopen($tmp_file, 'w');
                        stream_copy_to_stream($src_fh, $tmp_fh, 1024 * 1024 * 2);

                        $image = Engine_Image::factory();
                        $image->open($tmp_file)
                                ->resize(120, 240)
                                ->write($thumb_file)
                                ->destroy();

                        try {
                            $thumbFileRow = Engine_Api::_()->storage()->create($thumb_file, array(
                                'parent_type' => $recording->getType(),
                                'parent_id' => $recording->getIdentity()
                            ));

                            // Remove temp file
                            @unlink($thumb_file);
                            @unlink($tmp_file);
                        } catch (Exception $e) {
                            //echo $e;
                            //die();
                        }

                        //$recording->duration = $values['stream_duration'];

                        $recording->photo_id = $thumbFileRow->file_id;
                        $recording->status = 1;
                        /////////////////////////////////////

                        $recording->save();

                        $db->commit();


                        //create authenticate

                        $auth = Engine_Api::_()->authorization()->context;

                        //VIEW ACTION
                        $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                        if (isset($values['auth_view']))
                            $auth_view = $values['auth_view'];
                        else
                            $auth_view = "registered";
                        $viewMax = array_search($auth_view, $roles);
                        foreach ($roles as $i => $role) {
                            //var_dump($role);
                            //var_dump($i <= $viewMax);
                            $auth->setAllowed($recording, $role, 'view', ($i <= $viewMax));
                        }

                        //COMMENT ACTION
                        $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                        if (isset($values['auth_comment']))
                            $auth_comment = $values['auth_comment'];
                        else
                            $auth_comment = "registered";
                        $commentMax = array_search($auth_comment, $roles);
                        foreach ($roles as $i => $role) {

                            $auth->setAllowed($recording, $role, 'comment', ($i <= $commentMax));
                        }
                        //var_dump($values['submit_with_post']);
                        //die();
                        if ($values['submit_with_post'] == "1") {

                            $db->beginTransaction();
                            try {
                                //var_dump($recording);
                                //die();
                                $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $recording, 'recorded_video_new');
                                if ($action != null) {
                                    Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $recording);
                                }
                                // Rebuild privacy
                                $actionTable = Engine_Api::_()->getDbtable('actions', 'activity');
                                foreach ($actionTable->getActionsByObject($recording) as $action) {
                                    $actionTable->resetActivityBindings($action);
                                }
                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollBack();
                                //throw $e;
                                echo $e;
                            }
                        }

                        $this->_helper->layout->setLayout('default-simple');
                        $message = 'Your Video Profile Has Been Submitted.';
                        $this->_forward('success', 'utility', 'core', array(
                            'smoothboxClose' => true,
                            'parentRefresh' => true,
                            'messages' => array($message)
                        ));
                    }
                }

                //We deliver the player
            } else {

                //If the form is not submitted, we deliver the Recorder

                $base_url = Zend_Controller_Front::getInstance()->getBaseUrl();

                if (isset($viewer->level_id) && $viewer_id != '0') {
                    //--------------------------
                    //Get level id
                    //--------------------------
                    $level_id = $viewer->level_id;


                    //------------------------------
                    //Get HDFVR Profiler Permissions
                    //------------------------------
                    $form = new Hdfvrprofiler_Form_Admin_Level_Edit();
                    $permissionsTable = Engine_Api::_()->getDbtable('permissions', 'authorization');
                    $level_hdfvrprofiler_permissions = $permissionsTable->getAllowed('hdfvrprofiler', $level_id, array_keys($form->getValues()));



                    //--------------------------
                    //Setup permissions
                    //--------------------------
                    $hdfvrprofiler_permissions = '';

                    foreach ($level_hdfvrprofiler_permissions as $key => $value) {
                        if ($hdfvrprofiler_permissions == '') {
                            $hdfvrprofiler_permissions = $key . '=' . $value;
                        } else {
                            $hdfvrprofiler_permissions.= '&' . $key . '=' . $value;
                        }
                    }
                    setcookie('hdfvrprofiler_permissions', $hdfvrprofiler_permissions, time() + 24 * 3600, '/');


                    //-----------------------------
                    //Get HDFVR Profiler General Settings
                    //-----------------------------
                    $general_hdfvrprofiler_settings = Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('hdfvrprofiler', array());


                    //--------------------------
                    //Setup settings
                    //--------------------------
                    $hdfvrprofiler_settings = 'userid=' . $viewer_id;
                    foreach ($general_hdfvrprofiler_settings as $key => $value) {
                        $hdfvrprofiler_settings.= '&' . $key . '=' . $value;
                    }
                    setcookie('hdfvrprofiler_settings', $hdfvrprofiler_settings, time() + 24 * 3600, '/');
                } else {
                    $level_id = '0';
                    setcookie('avchat3_permissions', '', time() + 24 * 3600, '/');
                    setcookie('avchat3_settings', '', time() + 24 * 3600, '/');
                }

                $this->view->title = 'Record your Video Profile';
                $this->view->recorder = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
				id="VideoRecorder" width="404" height="336"
				codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
				<param name="movie" value="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/VideoRecorder.swf?userId=' . $viewer_id . '" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#ffffff" />
				<param name="base" value="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/" />
				<param name="allowScriptAccess" value="sameDomain" />
				<embed src="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/VideoRecorder.swf?userId=' . $viewer_id . '" quality="high" bgcolor="#ffffff"
				width="404" height="336" name="VideoRecorder" align="middle"
				play="true"
				loop="false"
				quality="high"
				allowScriptAccess="sameDomain"
				type="application/x-shockwave-flash"
				base="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/"
				pluginspage="http://www.adobe.com/go/getflashplayer">
				</embed>
				</object>';
                $this->view->loggedIn = true;
            }
        } else {
            $this->view->loggedIn = false;
        }
        //$this->_helper->content->render();
    }

    public function viewAction() {
        $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
        $viewer = Engine_Api::_()->user()->getViewer();
        $base_url = Zend_Controller_Front::getInstance()->getBaseUrl();

        if (!Engine_Api::_()->core()->hasSubject()) {
            $video_id = $this->_getParam('hdfvrprofiler_id');
        }

        //Check if the current user has any video profiles active
        // $userVideoProfileInfos = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($viewer_id);
        $userVideoProfileInfos = Engine_Api::_()->hdfvrprofiler()->getVideoProfileByhdfvrprofilerid($video_id);
//        echo '<pre>';
//        print_r($userVideoProfileInfos);
//        echo '</pre>';
        //-----------------------------
        //Get HDFVR Profiler General Settings
        //-----------------------------
        $general_hdfvrprofiler_settings = Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('hdfvrprofiler', array());

        $this->view->title = 'View Video Profile';

        if (count($userVideoProfileInfos) != 0) {
            //--------------------------
            //Setup settings
            //--------------------------
            $hdfvrprofiler_settings = 'filename=' . $userVideoProfileInfos[0]['filename'];
            foreach ($general_hdfvrprofiler_settings as $key => $value) {
                $hdfvrprofiler_settings.= '&' . $key . '=' . $value;
            }
            setcookie('hdfvrprofilerplayer_settings', $hdfvrprofiler_settings, time() + 24 * 3600, '/');

            $current_id = $userVideoProfileInfos[0]['hdfvrprofiler_id']; // last known profile video_id

            $begin_sentance = "<div>";
            if ($video_id != null && $current_id != $video_id) {
                // $begin_sentance = $begin_sentance . "This video is no longer available. You recorded a NEW video profile. Here's the NEW video profile:";
            }
            $begin_sentance = $begin_sentance . "</div>";

            $this->view->title = 'View Video Profile';
            $this->view->player = $begin_sentance . '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
				id="VideoRecorder" width="404" height="336"
				codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
				<param name="movie" value="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/videoplayer.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#990000" />
				<param name="base" value="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/" />
				<param name="allowScriptAccess" value="sameDomain" />
				<embed src="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/videoplayer.swf" quality="high" bgcolor="#990000"
				width="404" height="336" name="VideoRecorder" align="middle"
				play="true"
				loop="false"
				quality="high"
				allowScriptAccess="sameDomain"
				type="application/x-shockwave-flash"
				base="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/"
				pluginspage="http://www.adobe.com/go/getflashplayer">
				</embed>
				</object>
				</br>
				</br>
				</br>
				';
        } else {
            $message = 'You do not have a video profile recorded yet<br /><a href="' . $base_url . '/hdfvrprofiler/">Record a video profile now!</a>
			<div>or <a name="cancel" id="cancel" type="button" href="javascript:void(0);" onclick="parent.Smoothbox.close();">close</a></div>';
            $this->_forward('success', 'utility', 'core', array(
                'smoothboxClose' => 10000,
                'parentRefresh' => false,
                'messages' => array($message)
            ));
            //$this->view->player = 'You do not have a video profile recorded yet<br /><a href="'.$base_url.'/hdfvrprofiler/">Record a video profile now!</a>
            //<div>or <a name="cancel" id="cancel" type="button" href="javascript:void(0);" onclick="parent.Smoothbox.close();">close</a></div>';
        }
    }

    public function viewfriendAction() {
        $viewer = Engine_Api::_()->user()->getViewer();
        //$activity_moderate = Engine_Api::_()->getDbtable('permissions', 'authorization')->getAllowed('user', $viewer->level_id, 'activity');
        //if( !$this->_helper->requireAuth()->setAuthParams('hdfvrprofiler', $viewer->level_id, 'view')->isValid() ) {
        //	return;
        //}


        if (!$this->_helper->requireUser()->isValid())
            return;

        $subject = null;
        $base_url = Zend_Controller_Front::getInstance()->getBaseUrl();
        if (!Engine_Api::_()->core()->hasSubject()) {
            list($user_id, $video_id) = explode("_", $this->_getParam('user_id'));
            //var_dump($user_id);
            //die();
            // use viewer ID if not specified
            //if( is_null($id) )
            //  $id = Engine_Api::_()->user()->getViewer()->getIdentity();

            if (null !== $user_id) {
                $subject = Engine_Api::_()->user()->getUser($user_id);
                if ($subject->getIdentity()) {
                    Engine_Api::_()->core()->setSubject($subject);
                }
            }
        }

        $activity_view = Engine_Api::_()->getDbtable('permissions', 'authorization')->getAllowed('hdfvrprofiler', $viewer->level_id, 'view');

        if (!$activity_view) {
            $this->view->player = 'You do not have permission to view ' . $subject . '\'s video profile!
			</br>
			</br>
			</br>
			<h3><a name="cancel" id="cancel" type="button" href="javascript:void(0);" onclick="parent.Smoothbox.close();">Close</a></h3>';
            return;
        }

        $this->_helper->requireSubject('user');
        $this->_helper->requireAuth()->setNoForward()->setAuthParams(
                $subject, Engine_Api::_()->user()->getViewer(), 'view'
        );


        $subject = Engine_Api::_()->core()->getSubject();

        //Check if the current user has any video profiles active
        $userVideoProfileInfos = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($user_id);




        $this->view->title = 'View Video Profile';

        if (count($userVideoProfileInfos) != 0) {
            //-----------------------------
            //Get HDFVR Profiler General Settings
            //-----------------------------
            $general_hdfvrprofiler_settings = Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('hdfvrprofiler', array());


            //--------------------------
            //Setup settings
            //--------------------------

            $current_id = $userVideoProfileInfos[0]['hdfvrprofiler_id']; // last known profile video_id

            $begin_sentance = "<div>";
            if ($video_id != null && $current_id != $video_id) {
                $begin_sentance = $begin_sentance . "This video is no longer available. The member has recorded a NEW video profile. Here's the NEW video profile:";
            }
            $begin_sentance = $begin_sentance . "</div>";
            $hdfvrprofiler_settings = 'filename=' . $userVideoProfileInfos[0]['filename'];
            foreach ($general_hdfvrprofiler_settings as $key => $value) {
                $hdfvrprofiler_settings.= '&' . $key . '=' . $value;
            }
            setcookie('hdfvrprofilerplayer_settings', $hdfvrprofiler_settings, time() + 60, $base_url);

            $this->view->player = $begin_sentance . '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
			id="VideoRecorder" width="404" height="336"
			codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
			<param name="movie" value="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/videoplayer.swf" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#990000" />
			<param name="base" value="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/" />
			<param name="allowScriptAccess" value="sameDomain" />
			<embed src="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/videoplayer.swf" quality="high" bgcolor="#990000"
			width="404" height="336" name="VideoRecorder" align="middle"
			play="true"
			loop="false"
			quality="high"
			allowScriptAccess="sameDomain"
			type="application/x-shockwave-flash"
			base="application/modules/Hdfvrprofiler/externals/hdfvr_videorecorder/videoplayer/"
			pluginspage="http://www.adobe.com/go/getflashplayer">
			</embed>
			</object>
			</br>
			</br>
			</br>
			';
        } else {
            $this->view->player = $subject . ' has no video profile!
			</br>
		    </br>
		    </br>
	  	    <h3><a name="cancel" id="cancel" type="button" href="javascript:void(0);" onclick="parent.Smoothbox.close();">Close</a></h3>';
            //$this->view->player =
        }
    }

    public function deleteActivity($myRecording) {

        $viewer = Engine_Api::_()->user()->getViewer();
        $viewer_id = $viewer->getIdentity();
        $activity_moderate = Engine_Api::_()->getDbtable('permissions', 'authorization')->getAllowed('user', $viewer->level_id, 'activity');

        if (!$this->_helper->requireUser()->isValid())
            return;

        //      = Engine_Api::_()->getDbtable('actions', 'activity')->getActionsByObject($myRecording);
        $actions = new Activity_Model_DbTable_Actions();

        //var_dump($myRecording->getType());
        //die();
        $action = $actions->getActivity($viewer)->getTable()->fetchAll(array('object_type = ?' => $myRecording->getType()));

        //if($action->current() == NULL){
        //	return false;
        //}
        //var_dump($action->current());
        //var_dump($action->getTable()->fetchAll(array('object_type = ?' => $myRecording->getType())));
        //die();
        $action = $action->current();
        //var_dump($action->getTable());
        //var_dump($viewer->getIdentity());
        //$action->
        //$action = $action->getTable()->fetchRow(array('object_type = ?' => $myRecording->getType()));
        //var_dump($myRecording);
        //var_dump($action->object_type);
        //var_dump($viewer_id);
        //var_dump($action->type);
        //var_dump($action->subject_id);
        //die();

        if (!$action) {

            return false;
        }
        //var_dump($action);
        //die('da');
        // Both the author and the person being written about get to delete the action_id
        if (
                $activity_moderate ||
                ('user' == $action->subject_type && $viewer->getIdentity() == $action->subject_id) || // owner of profile being commented on
                ('user' == $action->object_type && $viewer->getIdentity() == $action->object_id)) {   // commenter
            // Delete action item and all comments/likes
            $db = Engine_Api::_()->getDbtable('actions', 'activity')->getAdapter();
            $db->beginTransaction();
            try {
                // delete comments that are not linked items
                if ($action->getTypeInfo()->commentable <= 1) {
                    Engine_Api::_()->getDbtable('comments', 'activity')->delete(array(
                        'resource_id = ?' => $action->action_id,
                    ));

                    // delete all "likes"
                    Engine_Api::_()->getDbtable('likes', 'activity')->delete(array(
                        'resource_id = ?' => $action->action_id,
                    ));
                    //$action->_likes = null;
                }

                // lastly, delete item
                Engine_Api::_()->getDbtable('actions', 'activity')->delete(array(
                    'subject_id = ?' => $viewer_id,
                    'type = ?' => $action->type,
                ));
                $db->commit();

                return true;
            } catch (Exception $e) {
                $db->rollback();
                //$this->view->status = false;
            }
        } else {
            // neither the item owner, nor the item subject.  Denied!
            $this->_forward('requireauth', 'error', 'core');
        }
        return false;
    }

    public function deleteAction() {
        $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();


        $this->view->title = 'Delete Video Profile';

        //Check if the current user has any other recordings in the database
        $myRecordings = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($viewer_id);

        if ($this->getRequest()->isPost()) {
            $db = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler')->getAdapter();

            if (count($myRecordings) != 0) {
                $myRecording = $myRecordings['hdfvrprofiler_id'];

                //$delete_link = 'activity/index/delete/action_id/'.$myRecording_id;
                if ($this->deleteActivity($myRecording)) {

                }

                //Mark with status 0 the current profile video
                Engine_Api::_()->hdfvrprofiler()->disableCurrentProfileVideo($viewer_id);
            }

            $message = 'Your Video Profile Has Been Deleted.';

            $this->_helper->layout->setLayout('default-simple');
            $this->_forward('success', 'utility', 'core', array(
                'smoothboxClose' => true,
                'parentRefresh' => true,
                'messages' => array($message)
            ));
        } else {
            if (count($myRecordings) != 0) {
                $this->renderScript('index/delete.tpl');
            } else {
                $this->_helper->layout->setLayout('default-simple');
                $message = 'You do not have a video profile!';

                $this->_forward('success', 'utility', 'core', array(
                    'smoothboxClose' => true,
                    'parentRefresh' => false,
                    'messages' => array($message)
                ));
            }
        }
    }

    protected function _getParam($paramName, $default = null) {
        $value = $this->getRequest()->getParam($paramName);
        if ((null === $value) && (null !== $default)) {
            $value = $default;
        }

        return $value;
    }

}
