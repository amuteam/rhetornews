<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_AdminSettingsController extends Core_Controller_Action_Admin
{
  public function generalAction()
  {
	 $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
          ->getNavigation('hdfvrprofiler_admin_main', array(), 'hdfvrprofiler_admin_main_manage');
  	
  	$this->view->form = $form = new Hdfvrprofiler_Form_Admin_Settings_General();
	
    if( $this->getRequest()->isPost() ) {
    	
    	if( $form->isValid($this->getRequest()->getPost()) )
    	{
    		$this->_helper->api()->getApi('settings', 'core')->hdfvrprofiler = $form->getValues();
    		$form->addNotice('Your changes have been saved.');
    		
    	}
    	
    }else{
    	$settings = Engine_Api::_()->getApi('settings', 'core')->getFlatSetting('hdfvrprofiler', array());    	
    	$form->populate($settings);
    }

  }
}