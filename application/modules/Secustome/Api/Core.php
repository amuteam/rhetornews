<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Secustome_Api_Core extends Core_Api_Abstract {

    public function getRssNewsCommentByhash($hash) {
        $table = Engine_Api::_()->getDbtable('rssnews', 'secustome');
        $select = $table->select()
                ->where('hash = ?', $hash);

        $result = $table->fetchAll($select);
        $sql = $select->__toString();
//        echo "$sql\n";
//        die;
        return $result;
    }

    public function disableCurrentProfileVideo($user_id) {

        $table = Engine_Api::_()->getDbtable('rssnews', 'secustome');

        if ($table->update(array('status' => 0), array('id_user = ?' => $user_id))) {
            return false;
        } else {
            return true;
        }
    }

    public function insertRssNewsComment($params) {
        $table = Engine_Api::_()->getDbtable('rssnews', 'secustome');
        $last = $table->insert(array('hash' => $params['hash'], 'comment' => $params['comment'], 'video_id' => $params['video_id'], 'user_id' => $params['user_id'], 'date' => isset($params['date']) ? $params['date'] : date('Y-m-d H:i:s'), 'status' => isset($params['staus']) ? $params['staus'] : 1));
        //TODO pune if aici
        return $last;
    }

    public function getSubjectVideoProfile($user_id) {
        $table = Engine_Api::_()->getDbtable('rssnews', 'secustome');
        $select = $table->select()
                ->where('id_user = ?', $user_id)
                ->where('status  = 1');

        $result = $table->fetchAll($select);
        return $result;
    }

    public function getVideoThumb($hdfvrprofiler_id) {
        $table = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');
        $recording = $table->fetchRow($table->select()->where('hdfvrprofiler_id = ?', $hdfvrprofiler_id));

        $video_duration = "";
        if ($recording->duration) {
            if ($recording->duration >= 3600) {
                $duration = gmdate("H:i:s", $recording->duration);
            } else {
                $duration = gmdate("i:s", $recording->duration);
            }
            $duration = ltrim($duration, '0:');

            $video_duration = "<span class='video_length'>" . $duration . "</span>";
        }
        $thumb = Zend_Registry::get('Zend_View')->itemPhoto($recording, 'thumb.video.activity');

        if ($recording->photo_id) {
            $thumb = Zend_Registry::get('Zend_View')->itemPhoto($recording, 'thumb.video.activity');
        } else {
            $thumb = '<img alt="" src="' . Zend_Registry::get('StaticBaseUrl') . 'application/modules/Hdfvrprofiler/externals/images/video.png">';
        }
        //var_dump($video_duration);
        //return "";
        $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
        //$subject = Engine_Api::_()->core()->
        //var_dump($viewer_id);
        //var_dump($this->id_user);
        //return "";
        if ($viewer_id == $recording->id_user) {
            $thumb = '<a id="video_thumb_' . $recording->hdfvrprofiler_id . '" class="video_thumb smoothbox" href="hdfvrprofiler/index/view/hdfvrprofiler_id/' . $recording->hdfvrprofiler_id . '"> <div class="video_thumb_wrapper">' . $video_duration . $thumb . '</div>
			</a>';
        } else {
            $thumb = '<a id="video_thumb_' . $recording->hdfvrprofiler_id . '" class="video_thumb smoothbox" href="hdfvrprofiler/index/view_friend/user_id/' . $recording->id_user . '_' . $recording->hdfvrprofiler_id . '"> <div class="video_thumb_wrapper">' . $video_duration . $thumb . '</div>
                  </a>';
        }
        return $thumb;
    }

    public function deleteRssNewsComment($params) {

        $table = Engine_Api::_()->getDbtable('rssnews', 'secustome');
        $rssnews = $table->fetchRow($table->select()->where('rssnews_id = ?', (int) $params["rssnews_id"]));
        $video_id = $rssnews->video_id;
        $table->delete(array(
            'rssnews_id = ?' => (int) $params["rssnews_id"],
        ));

        $table2 = Engine_Api::_()->getDbtable('hdfvrprofilers', 'hdfvrprofiler');
        if ($video_id != NULL) {
            if ($table2->update(array('status' => 0), array('hdfvrprofiler_id = ?' => (int) $video_id))) {
                //return false;
            } else {
                //return true;
            }
        }
        return true;
    }

}

