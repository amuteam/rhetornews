<?php

class Secustome_IndexController extends Core_Controller_Action_Standard {

    public function indexAction() {
        $this->view->someVar = 'someVal';
    }

    public function textcommentAction() {
        $request = $this->getRequest();
        $viewer = Engine_Api::_()->user()->getViewer();
        $comment_hash = $request->getParam('comment_hash');
        $comment_text = $request->getParam('comment_text');
        //echo "commet=>" . $comment_hash . " " . $comment_text;
        $params["hash"] = $comment_hash;
        $params["comment"] = $comment_text;
        $params["video_id"] = null;
        $params["user_id"] = $viewer->getIdentity();

        //user thumb
        $member2 = Engine_Api::_()->user()->getUser($viewer->getIdentity());
        $user_href = $member2->getHref();
        $user_icon = Zend_Registry::get('Zend_View')->itemPhoto($member2, 'thumb.icon');

        $inserted = Engine_Api::_()->secustome()->insertRssNewsComment($params);
        $this->_helper->layout->disableLayout();
        //$this->getResponse()->setHeader('Content-Type', 'text/javascript');
//        $this->view->comment_text = $comment_text;
//        $this->view->user_href = $user_href;
//        $this->view->user_icon = $user_icon;
        //$this->_helper->viewRenderer->setNoRender(TRUE);
        //$this->view->subseg = $comment_text; //$SubSegArr;
        $html = '<div id="rssnews_' . $inserted . '" class="all_user">
    <div class="user_content_area">
        <div class="user feed_user">
            <span class="">
                <a href="' . $user_href . '">' . $user_icon . '</a>
            </span>
        </div>
        <div class="content">
            <p>' . $comment_text . '</p>
        </div>
         <div  onclick="btn_delete(this.id)"  id="id_' . $inserted . '" class="btn_delete"><a href="javascript://">delete</a></div>
    </div>
</div>';
        $this->view->html = $html;
        $this->view->hash = $comment_hash;
    }

    public function deletecommentAction() {
        $request = $this->getRequest();
        $viewer = Engine_Api::_()->user()->getViewer();
        $rssnews_id = $request->getParam('rssnews_id');
        // $hdfvrprofiler_id = $request->getParam('hdfvrprofiler_id');
        $params["rssnews_id"] = $rssnews_id;
        $inserted = Engine_Api::_()->secustome()->deleteRssNewsComment($params);
        $this->_helper->layout->disableLayout();
        $this->view->rssnews_id = $rssnews_id;
    }

}
