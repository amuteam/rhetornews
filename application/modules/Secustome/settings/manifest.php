<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'secustome',
    'version' => '1.0.0',
    'path' => 'application/modules/Secustome',
    'title' => 'SE Custome',
    'description' => 'This plugin is developed for the custom modification in Social engine.',
    'author' => 'AMU technologies',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Secustome',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/secustome.csv',
    ),
  ),
); ?>