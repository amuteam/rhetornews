<?php

/**
 * SocialEngine
 *
 * @category   Application_Widget
 * @package    Rss
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9771 2012-08-30 22:21:00Z matthew $
 * @author     John
 */

/**
 * @category   Application_Widget
 * @package    Rss
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 *
 * RSS Widget
 *
 * This is the controller file for the RSS Widget that is found in the Layout Editor.
 *
 */
class Widget_RssController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        // Zend_Feed_Reader requires DOMDocument
        if (!class_exists('DOMDocument', false)) {
            return $this->setNoRender();
        }

        // Get params
        $url = $this->_getParam('url');
        if (!$url) {
            return $this->setNoRender();
        }
        $this->view->url = $url;
        // Sets the maximum number of items to display from the feed
        $this->view->max = $max = $this->_getParam('max', 12);
        // Gets the parameter thats deterimines if the HTML is stripped from each feed item
        $this->view->strip = $strip = $this->_getParam('strip', true);
        $cacheTimeout = (int) $this->_getParam('timeout');

        // Caching
        $cache = Zend_Registry::get('Zend_Cache');
        if ($cache instanceof Zend_Cache_Core &&
                $cacheTimeout > 0) {
            $cacheId = get_class($this) . md5($url . $max . $strip);
            $channel = $cache->load($cacheId);
            if (!is_array($channel) || empty($channel)) {
                $channel = null;
            } else if (time() > $channel['fetched'] + $cacheTimeout) {
                $channel = null;
            }
        } else {
            $cacheId = null;
            $channel = null;
        }

        if (!$channel) {
            // Parse the RSS Feed
            $rss = Zend_Feed_Reader::import($url);
//            echo "<pre>";
//            print_r($rss);
//            die;
            // Prepare channel info
            $channel = array(
                'title' => $rss->getTitle(),
                'link' => $rss->getLink(),
                'description' => $rss->getDescription(),
                'image' => $rss->getImage(),
                'items' => array(),
                'fetched' => time(),
            );

            // Get link
            $link = $rss->getLink('self');
            if ($link) {
                if ($link instanceof DOMElement) {
                    $channel['link'] = $link->nodeValue;
                } else if (is_array($link)) {
                    foreach ($link as $subLink) {
                        $channel['link'] = $subLink->nodeValue;
                        if (!empty($channel['link'])) {
                            break;
                        }
                    }
                }
            }

            // Loop over each channel item and store relevant data
            $count = 0;
            $viewer = Engine_Api::_()->user()->getViewer();
            $userId = $viewer->getIdentity();
            foreach ($rss as $mkey => $item) {
                if ($count++ >= $max)
                    break;
                //Video comment logic start
                $hash = md5($item->getId());
                //video comment save logic
//                $_SESSION["skcustome"]["hdfvrprofiler_id_last"] = 0;
//                unset($_SESSION["skcustome"]["hdfvrprofiler_id_last"]);
//                unset($_SESSION["skcustome"]["hdfvrprofiler_id_last"]);
//                unset($_SESSION["skcustome"]["session_hash"]);
//                unset($_SESSION["skcustome"]["hdfvrprofiler_id_last"]);
//                unset($_SESSION["skcustome"]["session_hash"]);
                if (!empty($_SESSION["skcustome"]["hdfvrprofiler_id_last"]) && !empty($_SESSION["skcustome"]["session_hash"]) && $_SESSION["skcustome"]["session_hash"] == $hash) {

                    $params["hash"] = $hash;
                    $params["comment"] = "";
                    $params["video_id"] = $_SESSION["skcustome"]["hdfvrprofiler_id_last"];
                    $params["user_id"] = $viewer->getIdentity();

                    $inserted = Engine_Api::_()->secustome()->insertRssNewsComment($params);

                    //unset session
                    $_SESSION["skcustome"]["hdfvrprofiler_id_last"] = null;
                    $_SESSION["skcustome"]["session_hash"] = null;
                    unset($_SESSION["skcustome"]["hdfvrprofiler_id_last"]);
                    unset($_SESSION["skcustome"]["session_hash"]);
                }

                $comments = Engine_Api::_()->secustome()->getRssNewsCommentByhash($hash);
                $comment = array();

                foreach ($comments as $key => $value) {
                    $thumb = "";
                    $user_icon = "";
                    $video_id = null;
                    if ($value->video_id != NULL) {
                        $video_id = $value->video_id;
                        $thumb = Engine_Api::_()->secustome()->getVideoThumb($video_id);
                    }
                    $member2 = Engine_Api::_()->user()->getUser($value->user_id);
                    $user_href = $member2->getHref();
                    $user_icon = Zend_Registry::get('Zend_View')->itemPhoto($member2, 'thumb.icon');
                    $comment[$key]["rssnews_id"] = $value->rssnews_id;
                    $comment[$key]["hash"] = $value->hash;
                    $comment[$key]["comment"] = $value->comment;
                    $comment[$key]["video_id"] = $video_id;
                    $comment[$key]["user_id"] = $value->user_id;
                    $comment[$key]["date"] = $value->date;
                    $comment[$key]["thumb"] = $thumb;
                    $comment[$key]["user_href"] = $user_href;
                    $comment[$key]["user_icon"] = $user_icon;
                }
//                echo '<pre>';
//                print_r($comments);
//                echo '</pre>';
//                die;
                //die;
//                echo '<pre>';
//                var_dump($comments);
//                echo '</pre>';
//                die;
                //video comment save logic end
                //viwer
                // Not logged in
                $record_video = array();
                $video_url = null;
                if ($viewer->getIdentity()) {
                    //--------------------------
                    //Get level id
                    //--------------------------
                    $level_id = $viewer->level_id;
                    $level_hdfvrprofiler_permissions = $this->getPermisionsHdfvrprofiler($level_id);
                    if ($level_hdfvrprofiler_permissions['hdfvr_allow_rec']) {
                        $myRecordings = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($viewer->getIdentity());

                        if (count($myRecordings) > 0) {
                            $record_video = array(
                                'label' => 'Video Comment',
                                'icon' => '',
                                'class' => 'smoothbox video_comments',
                                'module' => 'hdfvrprofiler',
                                'route' => 'default',
                                'params' => array(
                                    'controller' => 'index',
                                    'action' => 'view'),
                            );
//                            $params = array(
//                                'controller' => 'hdfvrprofiler',
//                            );
                            $params = array();
                            $controller = 'hdfvrprofiler';
                            // 'news', array('action' => 'details', 'id' =>42)
                            $video_url = Zend_Controller_Front::getInstance()->getRouter()
                                    ->assemble(array(
                                'controller' => 'hdfvrprofiler',
                                    )
                                    , 'default');
                        }
                    }
                }


                $channel['items'][] = array(
                    'title' => $item->getTitle(),
                    'link' => $item->getPermaLink(),
                    'description' => $item->getDescription(),
                    'image' => $rss->getImage(),
                    'pubDate' => $item->getDateCreated(),
                    'guid' => $item->getId(),
                    'hash' => $hash,
                    'video_href' => $record_video,
                    'video_url' => $video_url . "?hash=" . $hash,
                    'comment' => $comment,
                    'loggedin_userId' => $userId,
                );
            }
            $this->view->isCached = false;

            // Caching
            if ($cacheId && !empty($channel)) {
                $cache->save($channel, $cacheId);
            }
        } else {
            $this->view->isCached = true;
        }
//        echo '<pre>';
//        print_r($channel);
//        echo '</pre>';
//        die;

        $this->view->channel = $channel;
    }

    public function getPermisionsHdfvrprofiler($level_id) {
        //getPermisionsHdfvrprofiler
        //------------------------------
        //Get HDFVR Profiler Permissions
        //------------------------------
        $form = new Hdfvrprofiler_Form_Admin_Level_Edit();
        $permissionsTable = Engine_Api::_()->getDbtable('permissions', 'authorization');
        return $permissionsTable->getAllowed('hdfvrprofiler', $level_id, array_keys($form->getValues()));
    }

}

