<?php
/**
* SocialEngine
*
* @category   Application_Widget
* @package    Rss
* @copyright  Copyright 2006-2010 Webligo Developments
* @license    http://www.socialengine.com/license/
* @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
* @author     John
*/
?>

<script type="text/jscript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/jquery.min.js"></script>
<script type="text/jscript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/pin.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#pinBoot').pinterest_grid({
            no_columns: 3,
            padding_x: 50,
            padding_y: 25,
            margin_bottom: 50,
            single_column_breakpoint: 700
        });
    });
    en4.core.runonce.add(function() {
        $$('.rss_desc').enableLinks();
    });


</script>

<style type="text/css">
    .video_comments{
        margin-top: 2%;
        margin-right: 2%;
        color: #fff !important;
        font-weight: bold;
        text-decoration: none;
    }
    a:hover{text-decoration: none;}
    .user_content_area{width:100%;}
    .video_thumb_wrapper img, .video_thumb_wrapper span.item_nophoto{ width:60px;}
    .feed_user{ background-color:unset; padding: 0px 5px;}
    .content{ color: #727176;
              font-family: robotoregular !important;
              font-size: 13px !important;}
    .small_content img.thumb_icon{height: 40px;
    width: 40px;}
    .btn_delete{ float: right; margin-right: 7px;}
</style>
<?php if( !empty($this->channel) ): ?>

<ul id="pinBoot">
    <?php $count=0;foreach( $this->channel['items'] as $item ): $count++ ?>
    <li class="rss_item white-panel">
        <div class="rss_item_<?php echo $count ?>">

            <div class="feed_img_div">
                <img src="<?php echo $item['image']['uri'] ?>" alt="<?php echo $item['image']['title'] ?>" title="<?php echo $item['image']['title'] ?>" >
            </div>
            <br>
            <?php echo $this->htmlLink($item['link'], $item['title'],
            array('target' => '_blank', 'class' => 'rss_link_' . $count .' feed_title')) ?>
            <p class="rss_desc">
                <?php if( $this->strip ): ?>
                <?php //echo $this->string()->truncate($this->string()->stripTags($item['description']), 350) ?>
                <?php else: ?>
                <?php //echo $item['description'] ?>
                <?php endif ?>
            </p>
        </div>
        <div class="rss_time">


            <div class="bottom_part">
                <div class="bottom_left">
                    <span class="font_icon">
                        <i class="fa fa-share"></i>
                    </span>
                    <span class="font_icon">
                        <i class="fa fa-thumbs-up"></i>
                    </span>
                    <span class="date">
                        <?php echo $this->locale()->toDate(strtotime($item['pubDate']), array('size' => 'long')) ?>
                    </span>
                </div>

                <div class="bottom_right">
                    <span class="read_more">
                        <?php echo $this->htmlLink($item['link'], 'Read More',
                        array('target' => '_blank', 'class' => 'rss_link_' . $count)) ?>
                    </span>
                </div>
            </div>


        </div>




        <div class="small_content">
            <div id="comment_block_<?php echo $item['hash']?>">
                <?php if(count($item['comment']) > 0) {
                foreach($item['comment'] as $comment){
                ?>
                <div id="rssnews_<?php echo $comment['rssnews_id'];?>" class="all_user">
                    <div class="user_content_area">
                        <div class="user feed_user">
                            <span class="">
                                <a href="<?php echo $comment['user_href'];?>"><?php echo $comment['user_icon'];?></a>
                                <!-- <img src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/images/user_image.png" alt=""/> -->
                            </span>
                        </div>
                        <div class="content">
                            <?php if(!isset($comment["video_id"])){ ?>
                            <p><?php echo $comment["comment"]; ?></p>
                            <?php } else{ ?>
                            <?php echo $comment["thumb"]; ?>
                            <?php } ?>
                        </div>
                        <?php if($comment["user_id"] == $item["loggedin_userId"]){ ?>
                        <div id="id_<?php echo $comment['rssnews_id'];?>" onclick="btn_delete(this.id)" class="btn_delete"><a href="javascript://">delete</a></div>
                        <?php } ?>
                    </div>
                </div>
                <?php }
                }?>
            </div>
            <!-- <div class="all_user">
                <div class="user_content_area">
                    <div class="user">
                        <span class="">

                            <img src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/images/user_image.png" alt=""/>
                        </span>

                    </div>
                    <div class="content">

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>

                    </div>
                </div>
            </div> -->

            <input type="hidden" id="commenthash_<?php echo $item['hash']; ?>" name="hash" value="<?php echo  $item['hash']; ?>" />
            <div class="textarea_one">
                <textarea id="commenttext_<?php echo $item['hash']; ?>" cols="0" rows="2" class="edit"></textarea>
            </div>

            <?php echo $this->htmlLink($item['video_url'], $item['video_href']['label'], array(
            'class' => '' . ( $item['video_href']['class'] ? ' ' . $item['video_href']['class'] : '' ))
            ); ?>

            <p class="video_comments_text">OR</p>
            <button id="submitcomment_<?php echo $item['hash']; ?>"  class="submit_comment send">send</button>

        </div>



    </li>
    <?php endforeach; ?>

</ul>

<?php endif; ?>

<?php if( false ): ?>
<br />
<span class="rss_fetched_timestamp">
    <?php if( $this->isCached ): ?>
    <?php echo $this->translate('Results last fetched at %1$s',
    $this->locale()->toDateTime($this->channel['fetched'])) ?>
    <?php else: ?>
    <?php echo $this->translate('Results are current') ?>
    <?php endif ?>
</span>
<?php endif ?>
<script>
    var base_path = "<?php echo $this->baseUrl();?>/";

    $('.submit_comment').click(function()
    {
            var bid = this.id;
            var hash = bid.split("_");
            var orginalHash = hash[1];
            var comment_hash = document.getElementById('commenthash_' + orginalHash).value;
            var comment_text = document.getElementById('commenttext_' + orginalHash).value;
            if (comment_text == null || comment_text == "") {
                alert("Please enter text comment!");
                return false;
            }

            $.ajax({
                url: base_path + "secustome/index/textcomment/",
                data: "comment_hash=" + comment_hash + "&comment_text=" + comment_text,
                type: 'POST',
                success: function(data) {
                    // alert("call");
                    var json = $.parseJSON(data);
                    console.log(json.html);
                    console.log(json.hash)
                    $("#comment_block_" + json.hash).append(json.html);
                    $("#commenttext_" + json.hash).val("");

                },
                error: function(e) {
                    console.log(e);
                    // alert('Error: ' + e);

                }
            });

        });



        function btn_delete(bid) {
            //  var bid = this.id;
            var hash = bid.split("_");
            var orginalId = hash[1];
            if (!confirm("Are you sure to delete?")) {
                return false;
            }

            $.ajax({
                url: base_path + "secustome/index/deletecomment/",
                data: "rssnews_id=" + orginalId,
                type: 'POST',
                success: function(data) {
                    // alert("call");
                    var json = $.parseJSON(data);
                    $("#rssnews_" + json.rssnews_id).slideUp();
                    //console.log(json.rssnews_id);
                },
                error: function(e) {
                    console.log(e);
                    // alert('Error: ' + e);
                }
            });
        }
</script>

