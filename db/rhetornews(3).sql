-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2016 at 10:19 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rhetornews`
--

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_actions`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_actions` (
  `action_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `subject_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `subject_id` int(11) unsigned NOT NULL,
  `object_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `params` text COLLATE utf8_unicode_ci,
  `date` datetime NOT NULL,
  `attachment_count` smallint(3) unsigned NOT NULL DEFAULT '0',
  `comment_count` mediumint(5) unsigned NOT NULL DEFAULT '0',
  `like_count` mediumint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`),
  KEY `SUBJECT` (`subject_type`,`subject_id`),
  KEY `OBJECT` (`object_type`,`object_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `engine4_activity_actions`
--

INSERT INTO `engine4_activity_actions` (`action_id`, `type`, `subject_type`, `subject_id`, `object_type`, `object_id`, `body`, `params`, `date`, `attachment_count`, `comment_count`, `like_count`) VALUES
(1, 'profile_photo_update', 'user', 1, 'user', 1, '{item:$subject} added a new profile photo.', '[]', '2016-01-01 09:54:38', 1, 0, 0),
(2, 'signup', 'user', 2, 'user', 2, '', '[]', '2016-01-01 10:25:13', 0, 0, 0),
(3, 'friends', 'user', 2, 'user', 1, '{item:$object} is now friends with {item:$subject}.', '[]', '2016-01-01 10:26:49', 0, 0, 0),
(4, 'friends', 'user', 1, 'user', 2, '{item:$object} is now friends with {item:$subject}.', '[]', '2016-01-01 10:26:49', 0, 0, 0),
(5, 'status', 'user', 1, 'user', 1, 'wscdw', '[]', '2016-01-01 12:20:53', 0, 0, 0),
(6, 'post_self', 'user', 1, 'user', 1, '', '[]', '2016-01-01 12:29:16', 1, 0, 0),
(7, 'signup', 'user', 3, 'user', 3, '', '[]', '2016-01-07 07:23:33', 0, 0, 0),
(8, 'signup', 'user', 4, 'user', 4, '', '[]', '2016-01-25 06:16:20', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_actionsettings`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_actionsettings` (
  `user_id` int(11) unsigned NOT NULL,
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_actiontypes`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_actiontypes` (
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `module` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `displayable` tinyint(1) NOT NULL DEFAULT '3',
  `attachable` tinyint(1) NOT NULL DEFAULT '1',
  `commentable` tinyint(1) NOT NULL DEFAULT '1',
  `shareable` tinyint(1) NOT NULL DEFAULT '1',
  `is_generated` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_activity_actiontypes`
--

INSERT INTO `engine4_activity_actiontypes` (`type`, `module`, `body`, `enabled`, `displayable`, `attachable`, `commentable`, `shareable`, `is_generated`) VALUES
('album_photo_new', 'album', '{item:$subject} added {var:$count} photo(s) to the album {item:$object}:', 1, 5, 1, 3, 1, 1),
('blog_new', 'blog', '{item:$subject} wrote a new blog entry:', 1, 5, 1, 3, 1, 1),
('classified_new', 'classified', '{item:$subject} posted a new classified listing:', 1, 5, 1, 3, 1, 1),
('comment_album', 'album', '{item:$subject} commented on {item:$owner}''s {item:$object:album}: {body:$body}', 1, 1, 1, 1, 1, 0),
('comment_album_photo', 'album', '{item:$subject} commented on {item:$owner}''s {item:$object:photo}: {body:$body}', 1, 1, 1, 1, 1, 0),
('comment_blog', 'blog', '{item:$subject} commented on {item:$owner}''s {item:$object:blog entry}: {body:$body}', 1, 1, 1, 1, 1, 0),
('comment_classified', 'classified', '{item:$subject} commented on {item:$owner}''s {item:$object:classified listing}: {body:$body}', 1, 1, 1, 1, 1, 0),
('comment_playlist', 'music', '{item:$subject} commented on {item:$owner}''s {item:$object:music_playlist}.', 1, 1, 1, 1, 1, 1),
('comment_poll', 'poll', '{item:$subject} commented on {item:$owner}''s {item:$object:poll}.', 1, 1, 1, 1, 1, 1),
('comment_video', 'video', '{item:$subject} commented on {item:$owner}''s {item:$object:video}: {body:$body}', 1, 1, 1, 1, 1, 0),
('event_create', 'event', '{item:$subject} created a new event:', 1, 5, 1, 1, 1, 1),
('event_join', 'event', '{item:$subject} joined the event {item:$object}', 1, 3, 1, 1, 1, 1),
('event_photo_upload', 'event', '{item:$subject} added {var:$count} photo(s).', 1, 3, 2, 1, 1, 1),
('event_topic_create', 'event', '{item:$subject} posted a {item:$object:topic} in the event {itemParent:$object:event}: {body:$body}', 1, 3, 1, 1, 1, 1),
('event_topic_reply', 'event', '{item:$subject} replied to a {item:$object:topic} in the event {itemParent:$object:event}: {body:$body}', 1, 3, 1, 1, 1, 1),
('forum_promote', 'forum', '{item:$subject} has been made a moderator for the forum {item:$object}', 1, 3, 1, 1, 1, 1),
('forum_topic_create', 'forum', '{item:$subject} posted a {item:$object:topic} in the forum {itemParent:$object:forum}: {body:$body}', 1, 3, 1, 1, 1, 1),
('forum_topic_reply', 'forum', '{item:$subject} replied to a {item:$object:topic} in the forum {itemParent:$object:forum}: {body:$body}', 1, 3, 1, 1, 1, 1),
('friends', 'user', '{item:$subject} is now friends with {item:$object}.', 1, 3, 0, 1, 1, 1),
('friends_follow', 'user', '{item:$subject} is now following {item:$object}.', 1, 3, 0, 1, 1, 1),
('group_create', 'group', '{item:$subject} created a new group:', 1, 5, 1, 1, 1, 1),
('group_join', 'group', '{item:$subject} joined the group {item:$object}', 1, 3, 1, 1, 1, 1),
('group_photo_upload', 'group', '{item:$subject} added {var:$count} photo(s).', 1, 3, 2, 1, 1, 1),
('group_promote', 'group', '{item:$subject} has been made an officer for the group {item:$object}', 1, 3, 1, 1, 1, 1),
('group_topic_create', 'group', '{item:$subject} posted a {itemChild:$object:topic:$child_id} in the group {item:$object}: {body:$body}', 1, 3, 1, 1, 1, 1),
('group_topic_reply', 'group', '{item:$subject} replied to a {itemChild:$object:topic:$child_id} in the group {item:$object}: {body:$body}', 1, 3, 1, 1, 1, 1),
('login', 'user', '{item:$subject} has signed in.', 0, 1, 0, 1, 1, 1),
('logout', 'user', '{item:$subject} has signed out.', 0, 1, 0, 1, 1, 1),
('music_playlist_new', 'music', '{item:$subject} created a new playlist: {item:$object}', 1, 5, 1, 3, 1, 1),
('network_join', 'network', '{item:$subject} joined the network {item:$object}', 1, 3, 1, 1, 1, 1),
('poll_new', 'poll', '{item:$subject} created a new poll:', 1, 5, 1, 3, 1, 1),
('post', 'user', '{actors:$subject:$object}: {body:$body}', 1, 7, 1, 4, 1, 0),
('post_self', 'user', '{item:$subject} {body:$body}', 1, 5, 1, 4, 1, 0),
('profile_photo_update', 'user', '{item:$subject} has added a new profile photo.', 1, 5, 1, 4, 1, 1),
('share', 'activity', '{item:$subject} shared {item:$object}''s {var:$type}. {body:$body}', 1, 5, 1, 1, 0, 1),
('signup', 'user', '{item:$subject} has just signed up. Say hello!', 1, 5, 0, 1, 1, 1),
('status', 'user', '{item:$subject} {body:$body}', 1, 5, 0, 1, 4, 0),
('tagged', 'user', '{item:$subject} tagged {item:$object} in a {var:$label}:', 1, 7, 1, 1, 0, 1),
('video_new', 'video', '{item:$subject} posted a new video:', 1, 5, 1, 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_attachments`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_attachments` (
  `attachment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` int(11) unsigned NOT NULL,
  `type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id` int(11) unsigned NOT NULL,
  `mode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attachment_id`),
  KEY `action_id` (`action_id`),
  KEY `type_id` (`type`,`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `engine4_activity_attachments`
--

INSERT INTO `engine4_activity_attachments` (`attachment_id`, `action_id`, `type`, `id`, `mode`) VALUES
(1, 1, 'storage_file', 1, 1),
(2, 6, 'video', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_comments`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_comments` (
  `comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` int(11) unsigned NOT NULL,
  `poster_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `poster_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `like_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `resource_type` (`resource_id`),
  KEY `poster_type` (`poster_type`,`poster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_likes`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_likes` (
  `like_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` int(11) unsigned NOT NULL,
  `poster_type` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `poster_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`like_id`),
  KEY `resource_id` (`resource_id`),
  KEY `poster_type` (`poster_type`,`poster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_notifications`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_notifications` (
  `notification_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `subject_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `subject_id` int(11) unsigned NOT NULL,
  `object_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `mitigated` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `LOOKUP` (`user_id`,`date`),
  KEY `subject` (`subject_type`,`subject_id`),
  KEY `object` (`object_type`,`object_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `engine4_activity_notifications`
--

INSERT INTO `engine4_activity_notifications` (`notification_id`, `user_id`, `subject_type`, `subject_id`, `object_type`, `object_id`, `type`, `params`, `read`, `mitigated`, `date`) VALUES
(1, 1, 'user', 2, 'user', 1, 'friend_request', NULL, 1, 1, '2016-01-01 10:26:09'),
(2, 2, 'user', 1, 'user', 2, 'friend_accepted', NULL, 0, 0, '2016-01-01 10:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_notificationsettings`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_notificationsettings` (
  `user_id` int(11) unsigned NOT NULL,
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `email` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_notificationtypes`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_notificationtypes` (
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `module` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `is_request` tinyint(1) NOT NULL DEFAULT '0',
  `handler` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `default` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_activity_notificationtypes`
--

INSERT INTO `engine4_activity_notificationtypes` (`type`, `module`, `body`, `is_request`, `handler`, `default`) VALUES
('blog_subscribed_new', 'blog', '{item:$subject} has posted a new blog entry: {item:$object}.', 0, '', 1),
('commented', 'activity', '{item:$subject} has commented on your {item:$object:$label}.', 0, '', 1),
('commented_commented', 'activity', '{item:$subject} has commented on a {item:$object:$label} you commented on.', 0, '', 1),
('event_accepted', 'event', 'Your request to join the event {item:$object} has been approved.', 0, '', 1),
('event_approve', 'event', '{item:$subject} has requested to join the event {item:$object}.', 0, '', 1),
('event_discussion_reply', 'event', '{item:$subject} has {item:$object:posted} on a {itemParent:$object::event topic} you posted on.', 0, '', 1),
('event_discussion_response', 'event', '{item:$subject} has {item:$object:posted} on a {itemParent:$object::event topic} you created.', 0, '', 1),
('event_invite', 'event', '{item:$subject} has invited you to the event {item:$object}.', 1, 'event.widget.request-event', 1),
('forum_promote', 'forum', 'You were promoted to moderator in the forum {item:$object}.', 0, '', 1),
('forum_topic_reply', 'forum', '{item:$subject} has {item:$object:posted:$url} on a {itemParent:$object::forum topic} posted on.', 0, '', 1),
('forum_topic_response', 'forum', '{item:$subject} has {item:$object:posted:$url} on a {itemParent:$object::forum topic} you created.', 0, '', 1),
('friend_accepted', 'user', 'You and {item:$subject} are now friends.', 0, '', 1),
('friend_follow', 'user', '{item:$subject} is now following you.', 0, '', 1),
('friend_follow_accepted', 'user', 'You are now following {item:$subject}.', 0, '', 1),
('friend_follow_request', 'user', '{item:$subject} has requested to follow you.', 1, 'user.friends.request-follow', 1),
('friend_request', 'user', '{item:$subject} has requested to be your friend.', 1, 'user.friends.request-friend', 1),
('group_accepted', 'group', 'Your request to join the group {item:$object} has been approved.', 0, '', 1),
('group_approve', 'group', '{item:$subject} has requested to join the group {item:$object}.', 0, '', 1),
('group_discussion_reply', 'group', '{item:$subject} has {item:$object:posted} on a {itemParent:$object::group topic} you posted on.', 0, '', 1),
('group_discussion_response', 'group', '{item:$subject} has {item:$object:posted} on a {itemParent:$object::group topic} you created.', 0, '', 1),
('group_invite', 'group', '{item:$subject} has invited you to the group {item:$object}.', 1, 'group.widget.request-group', 1),
('group_promote', 'group', 'You were promoted to officer in the group {item:$object}.', 0, '', 1),
('liked', 'activity', '{item:$subject} likes your {item:$object:$label}.', 0, '', 1),
('liked_commented', 'activity', '{item:$subject} has commented on a {item:$object:$label} you liked.', 0, '', 1),
('message_new', 'messages', '{item:$subject} has sent you a {item:$object:message}.', 0, '', 1),
('post_user', 'user', '{item:$subject} has posted on your {item:$object:profile}.', 0, '', 1),
('shared', 'activity', '{item:$subject} has shared your {item:$object:$label}.', 0, '', 1),
('tagged', 'user', '{item:$subject} tagged you in a {item:$object:$label}.', 0, '', 1),
('video_processed', 'video', 'Your {item:$object:video} is ready to be viewed.', 0, '', 1),
('video_processed_failed', 'video', 'Your {item:$object:video} has failed to process.', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_activity_stream`
--

CREATE TABLE IF NOT EXISTS `engine4_activity_stream` (
  `target_type` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `target_id` int(11) unsigned NOT NULL,
  `subject_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `subject_id` int(11) unsigned NOT NULL,
  `object_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `action_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`target_type`,`target_id`,`action_id`),
  KEY `SUBJECT` (`subject_type`,`subject_id`,`action_id`),
  KEY `OBJECT` (`object_type`,`object_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_activity_stream`
--

INSERT INTO `engine4_activity_stream` (`target_type`, `target_id`, `subject_type`, `subject_id`, `object_type`, `object_id`, `type`, `action_id`) VALUES
('everyone', 0, 'user', 1, 'user', 1, 'profile_photo_update', 1),
('everyone', 0, 'user', 2, 'user', 2, 'signup', 2),
('everyone', 0, 'user', 2, 'user', 1, 'friends', 3),
('everyone', 0, 'user', 1, 'user', 2, 'friends', 4),
('everyone', 0, 'user', 1, 'user', 1, 'status', 5),
('everyone', 0, 'user', 1, 'user', 1, 'post_self', 6),
('everyone', 0, 'user', 3, 'user', 3, 'signup', 7),
('everyone', 0, 'user', 4, 'user', 4, 'signup', 8),
('members', 1, 'user', 1, 'user', 1, 'profile_photo_update', 1),
('members', 1, 'user', 2, 'user', 1, 'friends', 3),
('members', 1, 'user', 1, 'user', 1, 'status', 5),
('members', 1, 'user', 1, 'user', 1, 'post_self', 6),
('members', 2, 'user', 2, 'user', 2, 'signup', 2),
('members', 2, 'user', 1, 'user', 2, 'friends', 4),
('members', 3, 'user', 3, 'user', 3, 'signup', 7),
('members', 4, 'user', 4, 'user', 4, 'signup', 8),
('owner', 1, 'user', 1, 'user', 1, 'profile_photo_update', 1),
('owner', 1, 'user', 1, 'user', 2, 'friends', 4),
('owner', 1, 'user', 1, 'user', 1, 'status', 5),
('owner', 1, 'user', 1, 'user', 1, 'post_self', 6),
('owner', 2, 'user', 2, 'user', 2, 'signup', 2),
('owner', 2, 'user', 2, 'user', 1, 'friends', 3),
('owner', 3, 'user', 3, 'user', 3, 'signup', 7),
('owner', 4, 'user', 4, 'user', 4, 'signup', 8),
('parent', 1, 'user', 1, 'user', 1, 'profile_photo_update', 1),
('parent', 1, 'user', 2, 'user', 1, 'friends', 3),
('parent', 1, 'user', 1, 'user', 1, 'status', 5),
('parent', 1, 'user', 1, 'user', 1, 'post_self', 6),
('parent', 2, 'user', 2, 'user', 2, 'signup', 2),
('parent', 2, 'user', 1, 'user', 2, 'friends', 4),
('parent', 3, 'user', 3, 'user', 3, 'signup', 7),
('parent', 4, 'user', 4, 'user', 4, 'signup', 8),
('registered', 0, 'user', 1, 'user', 1, 'profile_photo_update', 1),
('registered', 0, 'user', 2, 'user', 2, 'signup', 2),
('registered', 0, 'user', 2, 'user', 1, 'friends', 3),
('registered', 0, 'user', 1, 'user', 2, 'friends', 4),
('registered', 0, 'user', 1, 'user', 1, 'status', 5),
('registered', 0, 'user', 1, 'user', 1, 'post_self', 6),
('registered', 0, 'user', 3, 'user', 3, 'signup', 7),
('registered', 0, 'user', 4, 'user', 4, 'signup', 8);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_album_albums`
--

CREATE TABLE IF NOT EXISTS `engine4_album_albums` (
  `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `type` enum('wall','profile','message','blog') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`album_id`),
  KEY `owner_type` (`owner_type`,`owner_id`),
  KEY `category_id` (`category_id`),
  KEY `search` (`search`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `engine4_album_albums`
--

INSERT INTO `engine4_album_albums` (`album_id`, `title`, `description`, `owner_type`, `owner_id`, `category_id`, `creation_date`, `modified_date`, `photo_id`, `view_count`, `comment_count`, `search`, `type`) VALUES
(1, 'Wall Photos', '', 'user', 3, 0, '2016-01-07 07:26:21', '2016-01-07 07:26:22', 1, 0, 0, 1, 'wall');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_album_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_album_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `category_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `engine4_album_categories`
--

INSERT INTO `engine4_album_categories` (`category_id`, `user_id`, `category_name`) VALUES
(0, 1, 'All Categories'),
(1, 1, 'Arts & Culture'),
(2, 1, 'Business'),
(3, 1, 'Entertainment'),
(5, 1, 'Family & Home'),
(6, 1, 'Health'),
(7, 1, 'Recreation'),
(8, 1, 'Personal'),
(9, 1, 'Shopping'),
(10, 1, 'Society'),
(11, 1, 'Sports'),
(12, 1, 'Technology'),
(13, 1, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_album_photos`
--

CREATE TABLE IF NOT EXISTS `engine4_album_photos` (
  `photo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `order` int(11) unsigned NOT NULL DEFAULT '0',
  `owner_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`photo_id`),
  KEY `album_id` (`album_id`),
  KEY `owner_type` (`owner_type`,`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `engine4_album_photos`
--

INSERT INTO `engine4_album_photos` (`photo_id`, `album_id`, `title`, `description`, `creation_date`, `modified_date`, `order`, `owner_type`, `owner_id`, `file_id`, `view_count`, `comment_count`) VALUES
(1, 1, '', '', '2016-01-07 07:26:21', '2016-01-07 07:26:22', 1, 'user', 3, 14, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_announcement_announcements`
--

CREATE TABLE IF NOT EXISTS `engine4_announcement_announcements` (
  `announcement_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `networks` text COLLATE utf8_unicode_ci,
  `member_levels` text COLLATE utf8_unicode_ci,
  `profile_types` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`announcement_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_authorization_allow`
--

CREATE TABLE IF NOT EXISTS `engine4_authorization_allow` (
  `resource_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `resource_id` int(11) unsigned NOT NULL,
  `action` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `role` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `role_id` int(11) unsigned NOT NULL DEFAULT '0',
  `value` tinyint(1) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`resource_type`,`resource_id`,`action`,`role`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_authorization_allow`
--

INSERT INTO `engine4_authorization_allow` (`resource_type`, `resource_id`, `action`, `role`, `role_id`, `value`, `params`) VALUES
('album', 1, 'comment', 'everyone', 0, 1, NULL),
('album', 1, 'view', 'everyone', 0, 1, NULL),
('album_photo', 1, 'comment', 'everyone', 0, 1, NULL),
('album_photo', 1, 'view', 'everyone', 0, 1, NULL),
('forum', 1, 'post.create', 'registered', 0, 1, NULL),
('forum', 1, 'topic.create', 'registered', 0, 1, NULL),
('forum', 1, 'topic.delete', 'forum_list', 1, 1, NULL),
('forum', 1, 'topic.edit', 'forum_list', 1, 1, NULL),
('forum', 1, 'view', 'everyone', 0, 1, NULL),
('forum', 2, 'post.create', 'registered', 0, 1, NULL),
('forum', 2, 'topic.create', 'registered', 0, 1, NULL),
('forum', 2, 'topic.delete', 'forum_list', 2, 1, NULL),
('forum', 2, 'topic.edit', 'forum_list', 2, 1, NULL),
('forum', 2, 'view', 'everyone', 0, 1, NULL),
('forum', 3, 'post.create', 'registered', 0, 1, NULL),
('forum', 3, 'topic.create', 'registered', 0, 1, NULL),
('forum', 3, 'topic.delete', 'forum_list', 3, 1, NULL),
('forum', 3, 'topic.edit', 'forum_list', 3, 1, NULL),
('forum', 3, 'view', 'everyone', 0, 1, NULL),
('forum', 4, 'post.create', 'registered', 0, 1, NULL),
('forum', 4, 'topic.create', 'registered', 0, 1, NULL),
('forum', 4, 'topic.delete', 'forum_list', 4, 1, NULL),
('forum', 4, 'topic.edit', 'forum_list', 4, 1, NULL),
('forum', 4, 'view', 'everyone', 0, 1, NULL),
('forum', 5, 'post.create', 'registered', 0, 1, NULL),
('forum', 5, 'topic.create', 'registered', 0, 1, NULL),
('forum', 5, 'topic.delete', 'forum_list', 5, 1, NULL),
('forum', 5, 'topic.edit', 'forum_list', 5, 1, NULL),
('forum', 5, 'view', 'everyone', 0, 1, NULL),
('user', 1, 'comment', 'everyone', 0, 1, NULL),
('user', 1, 'comment', 'member', 0, 1, NULL),
('user', 1, 'comment', 'network', 0, 1, NULL),
('user', 1, 'comment', 'registered', 0, 1, NULL),
('user', 1, 'view', 'everyone', 0, 1, NULL),
('user', 1, 'view', 'member', 0, 1, NULL),
('user', 1, 'view', 'network', 0, 1, NULL),
('user', 1, 'view', 'registered', 0, 1, NULL),
('user', 2, 'comment', 'everyone', 0, 1, NULL),
('user', 2, 'comment', 'member', 0, 1, NULL),
('user', 2, 'comment', 'network', 0, 1, NULL),
('user', 2, 'comment', 'registered', 0, 1, NULL),
('user', 2, 'view', 'everyone', 0, 1, NULL),
('user', 2, 'view', 'member', 0, 1, NULL),
('user', 2, 'view', 'network', 0, 1, NULL),
('user', 2, 'view', 'registered', 0, 1, NULL),
('user', 3, 'comment', 'everyone', 0, 1, NULL),
('user', 3, 'comment', 'member', 0, 1, NULL),
('user', 3, 'comment', 'network', 0, 1, NULL),
('user', 3, 'comment', 'registered', 0, 1, NULL),
('user', 3, 'view', 'everyone', 0, 1, NULL),
('user', 3, 'view', 'member', 0, 1, NULL),
('user', 3, 'view', 'network', 0, 1, NULL),
('user', 3, 'view', 'registered', 0, 1, NULL),
('user', 4, 'comment', 'everyone', 0, 1, NULL),
('user', 4, 'comment', 'member', 0, 1, NULL),
('user', 4, 'comment', 'network', 0, 1, NULL),
('user', 4, 'comment', 'registered', 0, 1, NULL),
('user', 4, 'view', 'everyone', 0, 1, NULL),
('user', 4, 'view', 'member', 0, 1, NULL),
('user', 4, 'view', 'network', 0, 1, NULL),
('user', 4, 'view', 'registered', 0, 1, NULL),
('video', 1, 'comment', 'everyone', 0, 1, NULL),
('video', 1, 'comment', 'owner_member', 0, 1, NULL),
('video', 1, 'comment', 'owner_member_member', 0, 1, NULL),
('video', 1, 'comment', 'owner_network', 0, 1, NULL),
('video', 1, 'comment', 'registered', 0, 1, NULL),
('video', 1, 'view', 'everyone', 0, 1, NULL),
('video', 1, 'view', 'owner_member', 0, 1, NULL),
('video', 1, 'view', 'owner_member_member', 0, 1, NULL),
('video', 1, 'view', 'owner_network', 0, 1, NULL),
('video', 1, 'view', 'registered', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_authorization_levels`
--

CREATE TABLE IF NOT EXISTS `engine4_authorization_levels` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('public','user','moderator','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `flag` enum('default','superadmin','public') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `engine4_authorization_levels`
--

INSERT INTO `engine4_authorization_levels` (`level_id`, `title`, `description`, `type`, `flag`) VALUES
(1, 'Superadmins', 'Users of this level can modify all of your settings and data.  This level cannot be modified or deleted.', 'admin', 'superadmin'),
(2, 'Admins', 'Users of this level have full access to all of your network settings and data.', 'admin', ''),
(3, 'Moderators', 'Users of this level may edit user-side content.', 'moderator', ''),
(4, 'Default Level', 'This is the default user level.  New users are assigned to it automatically.', 'user', 'default'),
(5, 'Public', 'Settings for this level apply to users who have not logged in.', 'public', 'public');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_authorization_permissions`
--

CREATE TABLE IF NOT EXISTS `engine4_authorization_permissions` (
  `level_id` int(11) unsigned NOT NULL,
  `type` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `name` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `value` tinyint(3) NOT NULL DEFAULT '0',
  `params` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`level_id`,`type`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_authorization_permissions`
--

INSERT INTO `engine4_authorization_permissions` (`level_id`, `type`, `name`, `value`, `params`) VALUES
(1, 'admin', 'view', 1, NULL),
(1, 'album', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'album', 'auth_tag', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'album', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'album', 'comment', 2, NULL),
(1, 'album', 'create', 1, NULL),
(1, 'album', 'delete', 2, NULL),
(1, 'album', 'edit', 2, NULL),
(1, 'album', 'tag', 2, NULL),
(1, 'album', 'view', 2, NULL),
(1, 'announcement', 'create', 1, NULL),
(1, 'announcement', 'delete', 2, NULL),
(1, 'announcement', 'edit', 2, NULL),
(1, 'announcement', 'view', 2, NULL),
(1, 'blog', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'blog', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(1, 'blog', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'blog', 'comment', 2, NULL),
(1, 'blog', 'create', 1, NULL),
(1, 'blog', 'delete', 2, NULL),
(1, 'blog', 'edit', 2, NULL),
(1, 'blog', 'max', 3, '1000'),
(1, 'blog', 'style', 1, NULL),
(1, 'blog', 'view', 2, NULL),
(1, 'chat', 'chat', 1, NULL),
(1, 'chat', 'im', 1, NULL),
(1, 'classified', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'classified', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr'),
(1, 'classified', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'classified', 'comment', 2, NULL),
(1, 'classified', 'create', 1, NULL),
(1, 'classified', 'css', 2, NULL),
(1, 'classified', 'delete', 2, NULL),
(1, 'classified', 'edit', 2, NULL),
(1, 'classified', 'max', 3, '1000'),
(1, 'classified', 'photo', 1, NULL),
(1, 'classified', 'style', 2, NULL),
(1, 'classified', 'view', 2, NULL),
(1, 'core_link', 'create', 1, NULL),
(1, 'core_link', 'delete', 2, NULL),
(1, 'core_link', 'view', 2, NULL),
(1, 'event', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(1, 'event', 'auth_photo', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(1, 'event', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(1, 'event', 'comment', 2, NULL),
(1, 'event', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(1, 'event', 'create', 1, NULL),
(1, 'event', 'delete', 2, NULL),
(1, 'event', 'edit', 2, NULL),
(1, 'event', 'invite', 1, NULL),
(1, 'event', 'photo', 1, NULL),
(1, 'event', 'style', 1, NULL),
(1, 'event', 'view', 2, NULL),
(1, 'forum', 'comment', 2, NULL),
(1, 'forum', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(1, 'forum', 'create', 2, NULL),
(1, 'forum', 'delete', 2, NULL),
(1, 'forum', 'edit', 2, NULL),
(1, 'forum', 'post.create', 2, NULL),
(1, 'forum', 'post.delete', 2, NULL),
(1, 'forum', 'post.edit', 2, NULL),
(1, 'forum', 'topic.create', 2, NULL),
(1, 'forum', 'topic.delete', 2, NULL),
(1, 'forum', 'topic.edit', 2, NULL),
(1, 'forum', 'view', 2, NULL),
(1, 'forum_post', 'create', 2, NULL),
(1, 'forum_post', 'delete', 2, NULL),
(1, 'forum_post', 'edit', 2, NULL),
(1, 'forum_topic', 'create', 2, NULL),
(1, 'forum_topic', 'delete', 2, NULL),
(1, 'forum_topic', 'edit', 2, NULL),
(1, 'forum_topic', 'move', 2, NULL),
(1, 'general', 'activity', 2, NULL),
(1, 'general', 'style', 2, NULL),
(1, 'group', 'auth_comment', 5, '["registered", "member", "officer"]'),
(1, 'group', 'auth_event', 5, '["registered", "member", "officer"]'),
(1, 'group', 'auth_photo', 5, '["registered", "member", "officer"]'),
(1, 'group', 'auth_view', 5, '["everyone", "registered", "member"]'),
(1, 'group', 'comment', 2, NULL),
(1, 'group', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(1, 'group', 'create', 1, NULL),
(1, 'group', 'delete', 2, NULL),
(1, 'group', 'edit', 2, NULL),
(1, 'group', 'event', 1, NULL),
(1, 'group', 'invite', 1, NULL),
(1, 'group', 'photo', 1, NULL),
(1, 'group', 'photo.edit', 2, NULL),
(1, 'group', 'style', 1, NULL),
(1, 'group', 'topic.edit', 2, NULL),
(1, 'group', 'view', 2, NULL),
(1, 'messages', 'auth', 3, 'friends'),
(1, 'messages', 'create', 1, NULL),
(1, 'messages', 'editor', 3, 'plaintext'),
(1, 'music_playlist', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'music_playlist', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'music_playlist', 'comment', 2, NULL),
(1, 'music_playlist', 'create', 1, NULL),
(1, 'music_playlist', 'delete', 2, NULL),
(1, 'music_playlist', 'edit', 2, NULL),
(1, 'music_playlist', 'view', 2, NULL),
(1, 'poll', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'poll', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'poll', 'comment', 2, NULL),
(1, 'poll', 'create', 1, NULL),
(1, 'poll', 'delete', 2, NULL),
(1, 'poll', 'edit', 2, NULL),
(1, 'poll', 'view', 2, NULL),
(1, 'poll', 'vote', 2, NULL),
(1, 'user', 'activity', 1, NULL),
(1, 'user', 'auth_comment', 5, '["everyone","registered","network","member","owner"]'),
(1, 'user', 'auth_view', 5, '["everyone","registered","network","member","owner"]'),
(1, 'user', 'block', 1, NULL),
(1, 'user', 'comment', 2, NULL),
(1, 'user', 'create', 1, NULL),
(1, 'user', 'delete', 2, NULL),
(1, 'user', 'edit', 2, NULL),
(1, 'user', 'search', 1, NULL),
(1, 'user', 'status', 1, NULL),
(1, 'user', 'style', 2, NULL),
(1, 'user', 'username', 2, NULL),
(1, 'user', 'view', 2, NULL),
(1, 'video', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'video', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(1, 'video', 'comment', 2, NULL),
(1, 'video', 'create', 1, NULL),
(1, 'video', 'delete', 2, NULL),
(1, 'video', 'edit', 2, NULL),
(1, 'video', 'max', 3, '20'),
(1, 'video', 'upload', 1, NULL),
(1, 'video', 'view', 2, NULL),
(2, 'admin', 'view', 1, NULL),
(2, 'album', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'album', 'auth_tag', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'album', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'album', 'comment', 2, NULL),
(2, 'album', 'create', 1, NULL),
(2, 'album', 'delete', 2, NULL),
(2, 'album', 'edit', 2, NULL),
(2, 'album', 'tag', 2, NULL),
(2, 'album', 'view', 2, NULL),
(2, 'announcement', 'create', 1, NULL),
(2, 'announcement', 'delete', 2, NULL),
(2, 'announcement', 'edit', 2, NULL),
(2, 'announcement', 'view', 2, NULL),
(2, 'blog', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'blog', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(2, 'blog', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'blog', 'comment', 2, NULL),
(2, 'blog', 'create', 1, NULL),
(2, 'blog', 'delete', 2, NULL),
(2, 'blog', 'edit', 2, NULL),
(2, 'blog', 'max', 3, '1000'),
(2, 'blog', 'style', 1, NULL),
(2, 'blog', 'view', 2, NULL),
(2, 'chat', 'chat', 1, NULL),
(2, 'chat', 'im', 1, NULL),
(2, 'classified', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'classified', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr'),
(2, 'classified', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'classified', 'comment', 2, NULL),
(2, 'classified', 'create', 1, NULL),
(2, 'classified', 'css', 2, NULL),
(2, 'classified', 'delete', 2, NULL),
(2, 'classified', 'edit', 2, NULL),
(2, 'classified', 'max', 3, '1000'),
(2, 'classified', 'photo', 1, NULL),
(2, 'classified', 'style', 2, NULL),
(2, 'classified', 'view', 2, NULL),
(2, 'core_link', 'create', 1, NULL),
(2, 'core_link', 'delete', 2, NULL),
(2, 'core_link', 'view', 2, NULL),
(2, 'event', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(2, 'event', 'auth_photo', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(2, 'event', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(2, 'event', 'comment', 2, NULL),
(2, 'event', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(2, 'event', 'create', 1, NULL),
(2, 'event', 'delete', 2, NULL),
(2, 'event', 'edit', 2, NULL),
(2, 'event', 'invite', 1, NULL),
(2, 'event', 'photo', 1, NULL),
(2, 'event', 'style', 1, NULL),
(2, 'event', 'view', 2, NULL),
(2, 'forum', 'comment', 2, NULL),
(2, 'forum', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(2, 'forum', 'create', 2, NULL),
(2, 'forum', 'delete', 2, NULL),
(2, 'forum', 'edit', 2, NULL),
(2, 'forum', 'post.create', 2, NULL),
(2, 'forum', 'post.delete', 2, NULL),
(2, 'forum', 'post.edit', 2, NULL),
(2, 'forum', 'topic.create', 2, NULL),
(2, 'forum', 'topic.delete', 2, NULL),
(2, 'forum', 'topic.edit', 2, NULL),
(2, 'forum', 'view', 2, NULL),
(2, 'forum_post', 'create', 2, NULL),
(2, 'forum_post', 'delete', 2, NULL),
(2, 'forum_post', 'edit', 2, NULL),
(2, 'forum_topic', 'create', 2, NULL),
(2, 'forum_topic', 'delete', 2, NULL),
(2, 'forum_topic', 'edit', 2, NULL),
(2, 'forum_topic', 'move', 2, NULL),
(2, 'general', 'activity', 2, NULL),
(2, 'general', 'style', 2, NULL),
(2, 'group', 'auth_comment', 5, '["registered", "member", "officer"]'),
(2, 'group', 'auth_event', 5, '["registered", "member", "officer"]'),
(2, 'group', 'auth_photo', 5, '["registered", "member", "officer"]'),
(2, 'group', 'auth_view', 5, '["everyone", "registered", "member"]'),
(2, 'group', 'comment', 2, NULL),
(2, 'group', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(2, 'group', 'create', 1, NULL),
(2, 'group', 'delete', 2, NULL),
(2, 'group', 'edit', 2, NULL),
(2, 'group', 'event', 1, NULL),
(2, 'group', 'invite', 1, NULL),
(2, 'group', 'photo', 1, NULL),
(2, 'group', 'photo.edit', 2, NULL),
(2, 'group', 'style', 1, NULL),
(2, 'group', 'topic.edit', 2, NULL),
(2, 'group', 'view', 2, NULL),
(2, 'messages', 'auth', 3, 'friends'),
(2, 'messages', 'create', 1, NULL),
(2, 'messages', 'editor', 3, 'plaintext'),
(2, 'music_playlist', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'music_playlist', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'music_playlist', 'comment', 2, NULL),
(2, 'music_playlist', 'create', 1, NULL),
(2, 'music_playlist', 'delete', 2, NULL),
(2, 'music_playlist', 'edit', 2, NULL),
(2, 'music_playlist', 'view', 2, NULL),
(2, 'poll', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'poll', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'poll', 'comment', 2, NULL),
(2, 'poll', 'create', 1, NULL),
(2, 'poll', 'delete', 2, NULL),
(2, 'poll', 'edit', 2, NULL),
(2, 'poll', 'view', 2, NULL),
(2, 'poll', 'vote', 2, NULL),
(2, 'user', 'activity', 1, NULL),
(2, 'user', 'auth_comment', 5, '["everyone","registered","network","member","owner"]'),
(2, 'user', 'auth_view', 5, '["everyone","registered","network","member","owner"]'),
(2, 'user', 'block', 1, NULL),
(2, 'user', 'comment', 2, NULL),
(2, 'user', 'create', 1, NULL),
(2, 'user', 'delete', 2, NULL),
(2, 'user', 'edit', 2, NULL),
(2, 'user', 'search', 1, NULL),
(2, 'user', 'status', 1, NULL),
(2, 'user', 'style', 2, NULL),
(2, 'user', 'username', 2, NULL),
(2, 'user', 'view', 2, NULL),
(2, 'video', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'video', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(2, 'video', 'comment', 2, NULL),
(2, 'video', 'create', 1, NULL),
(2, 'video', 'delete', 2, NULL),
(2, 'video', 'edit', 2, NULL),
(2, 'video', 'max', 3, '20'),
(2, 'video', 'upload', 1, NULL),
(2, 'video', 'view', 2, NULL),
(3, 'album', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'album', 'auth_tag', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'album', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'album', 'comment', 2, NULL),
(3, 'album', 'create', 1, NULL),
(3, 'album', 'delete', 2, NULL),
(3, 'album', 'edit', 2, NULL),
(3, 'album', 'tag', 2, NULL),
(3, 'album', 'view', 2, NULL),
(3, 'announcement', 'view', 1, NULL),
(3, 'blog', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'blog', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(3, 'blog', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'blog', 'comment', 2, NULL),
(3, 'blog', 'create', 1, NULL),
(3, 'blog', 'delete', 2, NULL),
(3, 'blog', 'edit', 2, NULL),
(3, 'blog', 'max', 3, '1000'),
(3, 'blog', 'style', 1, NULL),
(3, 'blog', 'view', 2, NULL),
(3, 'chat', 'chat', 1, NULL),
(3, 'chat', 'im', 1, NULL),
(3, 'classified', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'classified', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr'),
(3, 'classified', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'classified', 'comment', 2, NULL),
(3, 'classified', 'create', 1, NULL),
(3, 'classified', 'css', 2, NULL),
(3, 'classified', 'delete', 2, NULL),
(3, 'classified', 'edit', 2, NULL),
(3, 'classified', 'max', 3, '1000'),
(3, 'classified', 'photo', 1, NULL),
(3, 'classified', 'style', 2, NULL),
(3, 'classified', 'view', 2, NULL),
(3, 'core_link', 'create', 1, NULL),
(3, 'core_link', 'delete', 2, NULL),
(3, 'core_link', 'view', 2, NULL),
(3, 'event', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(3, 'event', 'auth_photo', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(3, 'event', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(3, 'event', 'comment', 2, NULL),
(3, 'event', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(3, 'event', 'create', 1, NULL),
(3, 'event', 'delete', 2, NULL),
(3, 'event', 'edit', 2, NULL),
(3, 'event', 'invite', 1, NULL),
(3, 'event', 'photo', 1, NULL),
(3, 'event', 'style', 1, NULL),
(3, 'event', 'view', 2, NULL),
(3, 'forum', 'comment', 2, NULL),
(3, 'forum', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(3, 'forum', 'create', 2, NULL),
(3, 'forum', 'delete', 2, NULL),
(3, 'forum', 'edit', 2, NULL),
(3, 'forum', 'post.create', 2, NULL),
(3, 'forum', 'post.delete', 2, NULL),
(3, 'forum', 'post.edit', 2, NULL),
(3, 'forum', 'topic.create', 2, NULL),
(3, 'forum', 'topic.delete', 2, NULL),
(3, 'forum', 'topic.edit', 2, NULL),
(3, 'forum', 'view', 2, NULL),
(3, 'forum_post', 'create', 2, NULL),
(3, 'forum_post', 'delete', 2, NULL),
(3, 'forum_post', 'edit', 2, NULL),
(3, 'forum_topic', 'create', 2, NULL),
(3, 'forum_topic', 'delete', 2, NULL),
(3, 'forum_topic', 'edit', 2, NULL),
(3, 'forum_topic', 'move', 2, NULL),
(3, 'general', 'activity', 2, NULL),
(3, 'general', 'style', 2, NULL),
(3, 'group', 'auth_comment', 5, '["registered", "member", "officer"]'),
(3, 'group', 'auth_event', 5, '["registered", "member", "officer"]'),
(3, 'group', 'auth_photo', 5, '["registered", "member", "officer"]'),
(3, 'group', 'auth_view', 5, '["everyone", "registered", "member"]'),
(3, 'group', 'comment', 2, NULL),
(3, 'group', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(3, 'group', 'create', 1, NULL),
(3, 'group', 'delete', 2, NULL),
(3, 'group', 'edit', 2, NULL),
(3, 'group', 'event', 1, NULL),
(3, 'group', 'invite', 1, NULL),
(3, 'group', 'photo', 1, NULL),
(3, 'group', 'photo.edit', 2, NULL),
(3, 'group', 'style', 1, NULL),
(3, 'group', 'topic.edit', 2, NULL),
(3, 'group', 'view', 2, NULL),
(3, 'messages', 'auth', 3, 'friends'),
(3, 'messages', 'create', 1, NULL),
(3, 'messages', 'editor', 3, 'plaintext'),
(3, 'music_playlist', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'music_playlist', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'music_playlist', 'comment', 2, NULL),
(3, 'music_playlist', 'create', 1, NULL),
(3, 'music_playlist', 'delete', 2, NULL),
(3, 'music_playlist', 'edit', 2, NULL),
(3, 'music_playlist', 'view', 2, NULL),
(3, 'poll', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'poll', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'poll', 'comment', 2, NULL),
(3, 'poll', 'create', 1, NULL),
(3, 'poll', 'delete', 2, NULL),
(3, 'poll', 'edit', 2, NULL),
(3, 'poll', 'view', 2, NULL),
(3, 'poll', 'vote', 2, NULL),
(3, 'user', 'activity', 1, NULL),
(3, 'user', 'auth_comment', 5, '["everyone","registered","network","member","owner"]'),
(3, 'user', 'auth_view', 5, '["everyone","registered","network","member","owner"]'),
(3, 'user', 'block', 1, NULL),
(3, 'user', 'comment', 2, NULL),
(3, 'user', 'create', 1, NULL),
(3, 'user', 'delete', 2, NULL),
(3, 'user', 'edit', 2, NULL),
(3, 'user', 'search', 1, NULL),
(3, 'user', 'status', 1, NULL),
(3, 'user', 'style', 2, NULL),
(3, 'user', 'username', 2, NULL),
(3, 'user', 'view', 2, NULL),
(3, 'video', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'video', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(3, 'video', 'comment', 2, NULL),
(3, 'video', 'create', 1, NULL),
(3, 'video', 'delete', 2, NULL),
(3, 'video', 'edit', 2, NULL),
(3, 'video', 'max', 3, '20'),
(3, 'video', 'upload', 1, NULL),
(3, 'video', 'view', 2, NULL),
(4, 'album', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'album', 'auth_tag', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'album', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'album', 'comment', 1, NULL),
(4, 'album', 'create', 1, NULL),
(4, 'album', 'delete', 1, NULL),
(4, 'album', 'edit', 1, NULL),
(4, 'album', 'tag', 1, NULL),
(4, 'album', 'view', 1, NULL),
(4, 'announcement', 'view', 1, NULL),
(4, 'blog', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'blog', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(4, 'blog', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'blog', 'comment', 1, NULL),
(4, 'blog', 'create', 1, NULL),
(4, 'blog', 'delete', 1, NULL),
(4, 'blog', 'edit', 1, NULL),
(4, 'blog', 'max', 3, '50'),
(4, 'blog', 'style', 1, NULL),
(4, 'blog', 'view', 1, NULL),
(4, 'chat', 'chat', 1, NULL),
(4, 'chat', 'im', 1, NULL),
(4, 'classified', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'classified', 'auth_html', 3, 'strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr'),
(4, 'classified', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'classified', 'comment', 1, NULL),
(4, 'classified', 'create', 1, NULL),
(4, 'classified', 'css', 1, NULL),
(4, 'classified', 'delete', 1, NULL),
(4, 'classified', 'edit', 1, NULL),
(4, 'classified', 'max', 3, '50'),
(4, 'classified', 'photo', 1, NULL),
(4, 'classified', 'style', 1, NULL),
(4, 'classified', 'view', 1, NULL),
(4, 'core_link', 'create', 1, NULL),
(4, 'core_link', 'delete', 1, NULL),
(4, 'core_link', 'view', 1, NULL),
(4, 'event', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(4, 'event', 'auth_photo', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(4, 'event', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","parent_member","member","owner"]'),
(4, 'event', 'comment', 1, NULL),
(4, 'event', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(4, 'event', 'create', 1, NULL),
(4, 'event', 'delete', 1, NULL),
(4, 'event', 'edit', 1, NULL),
(4, 'event', 'invite', 1, NULL),
(4, 'event', 'photo', 1, NULL),
(4, 'event', 'style', 1, NULL),
(4, 'event', 'view', 1, NULL),
(4, 'forum', 'comment', 1, NULL),
(4, 'forum', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(4, 'forum', 'post.create', 2, NULL),
(4, 'forum', 'post.delete', 1, NULL),
(4, 'forum', 'post.edit', 1, NULL),
(4, 'forum', 'topic.create', 1, NULL),
(4, 'forum', 'topic.delete', 1, NULL),
(4, 'forum', 'topic.edit', 1, NULL),
(4, 'forum', 'view', 1, NULL),
(4, 'forum_post', 'create', 1, NULL),
(4, 'forum_post', 'delete', 1, NULL),
(4, 'forum_post', 'edit', 1, NULL),
(4, 'forum_topic', 'create', 1, NULL),
(4, 'forum_topic', 'delete', 1, NULL),
(4, 'forum_topic', 'edit', 1, NULL),
(4, 'general', 'style', 1, NULL),
(4, 'group', 'auth_comment', 5, '["registered", "member", "officer"]'),
(4, 'group', 'auth_event', 5, '["registered", "member", "officer"]'),
(4, 'group', 'auth_photo', 5, '["registered", "member", "officer"]'),
(4, 'group', 'auth_view', 5, '["everyone", "registered", "member"]'),
(4, 'group', 'comment', 1, NULL),
(4, 'group', 'commentHtml', 3, 'blockquote, strong, b, em, i, u, strike, sub, sup, p, div, pre, address, h1, h2, h3, h4, h5, h6, span, ol, li, ul, a, img, embed, br, hr, iframe'),
(4, 'group', 'create', 1, NULL),
(4, 'group', 'delete', 1, NULL),
(4, 'group', 'edit', 1, NULL),
(4, 'group', 'event', 1, NULL),
(4, 'group', 'invite', 1, NULL),
(4, 'group', 'photo', 1, NULL),
(4, 'group', 'photo.edit', 1, NULL),
(4, 'group', 'style', 1, NULL),
(4, 'group', 'topic.edit', 1, NULL),
(4, 'group', 'view', 1, NULL),
(4, 'messages', 'auth', 3, 'friends'),
(4, 'messages', 'create', 1, NULL),
(4, 'messages', 'editor', 3, 'plaintext'),
(4, 'music_playlist', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'music_playlist', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'music_playlist', 'comment', 1, NULL),
(4, 'music_playlist', 'create', 1, NULL),
(4, 'music_playlist', 'delete', 1, NULL),
(4, 'music_playlist', 'edit', 1, NULL),
(4, 'music_playlist', 'view', 1, NULL),
(4, 'poll', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'poll', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'poll', 'comment', 1, NULL),
(4, 'poll', 'create', 1, NULL),
(4, 'poll', 'delete', 1, NULL),
(4, 'poll', 'edit', 1, NULL),
(4, 'poll', 'view', 1, NULL),
(4, 'poll', 'vote', 1, NULL),
(4, 'user', 'auth_comment', 5, '["everyone","registered","network","member","owner"]'),
(4, 'user', 'auth_view', 5, '["everyone","registered","network","member","owner"]'),
(4, 'user', 'block', 1, NULL),
(4, 'user', 'comment', 1, NULL),
(4, 'user', 'create', 1, NULL),
(4, 'user', 'delete', 1, NULL),
(4, 'user', 'edit', 1, NULL),
(4, 'user', 'search', 1, NULL),
(4, 'user', 'status', 1, NULL),
(4, 'user', 'style', 1, NULL),
(4, 'user', 'username', 1, NULL),
(4, 'user', 'view', 1, NULL),
(4, 'video', 'auth_comment', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'video', 'auth_view', 5, '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
(4, 'video', 'comment', 1, NULL),
(4, 'video', 'create', 1, NULL),
(4, 'video', 'delete', 1, NULL),
(4, 'video', 'edit', 1, NULL),
(4, 'video', 'max', 3, '20'),
(4, 'video', 'upload', 1, NULL),
(4, 'video', 'view', 1, NULL),
(5, 'album', 'tag', 0, NULL),
(5, 'album', 'view', 1, NULL),
(5, 'announcement', 'view', 1, NULL),
(5, 'blog', 'view', 1, NULL),
(5, 'classified', 'view', 1, NULL),
(5, 'core_link', 'view', 1, NULL),
(5, 'event', 'view', 1, NULL),
(5, 'forum', 'view', 1, NULL),
(5, 'group', 'view', 1, NULL),
(5, 'music_playlist', 'view', 1, NULL),
(5, 'poll', 'view', 1, NULL),
(5, 'user', 'view', 1, NULL),
(5, 'video', 'view', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_blog_blogs`
--

CREATE TABLE IF NOT EXISTS `engine4_blog_blogs` (
  `blog_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `draft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_id`),
  KEY `owner_type` (`owner_type`,`owner_id`),
  KEY `search` (`search`,`creation_date`),
  KEY `owner_id` (`owner_id`,`draft`),
  KEY `draft` (`draft`,`search`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_blog_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_blog_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `category_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`,`category_name`),
  KEY `category_name` (`category_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `engine4_blog_categories`
--

INSERT INTO `engine4_blog_categories` (`category_id`, `user_id`, `category_name`) VALUES
(1, 1, 'Arts & Culture'),
(2, 1, 'Business'),
(3, 1, 'Entertainment'),
(5, 1, 'Family & Home'),
(6, 1, 'Health'),
(7, 1, 'Recreation'),
(8, 1, 'Personal'),
(9, 1, 'Shopping'),
(10, 1, 'Society'),
(11, 1, 'Sports'),
(12, 1, 'Technology'),
(13, 1, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_blog_subscriptions`
--

CREATE TABLE IF NOT EXISTS `engine4_blog_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `subscriber_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `user_id` (`user_id`,`subscriber_user_id`),
  KEY `subscriber_user_id` (`subscriber_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_bans`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_bans` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ban_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_events`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_events` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `user_id` (`user_id`,`date`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_messages`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `room_id` (`room_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_rooms`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_count` smallint(6) NOT NULL,
  `modified_date` datetime NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`room_id`),
  KEY `public` (`public`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `engine4_chat_rooms`
--

INSERT INTO `engine4_chat_rooms` (`room_id`, `title`, `user_count`, `modified_date`, `public`) VALUES
(1, 'General Chat', 0, '2010-02-02 00:44:04', 1),
(2, 'Introduce Yourself', 0, '2010-02-02 00:44:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_roomusers`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_roomusers` (
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  PRIMARY KEY (`room_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `date` (`date`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_users`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_users` (
  `user_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  `event_count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `date` (`date`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_chat_users`
--

INSERT INTO `engine4_chat_users` (`user_id`, `state`, `date`, `event_count`) VALUES
(1, 1, '2016-02-08 09:19:12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_chat_whispers`
--

CREATE TABLE IF NOT EXISTS `engine4_chat_whispers` (
  `whisper_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `recipient_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `recipient_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `sender_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`whisper_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`),
  KEY `recipient_deleted` (`recipient_deleted`,`sender_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_albums`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_albums` (
  `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `collectible_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`album_id`),
  KEY `classified_id` (`classified_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `category_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `engine4_classified_categories`
--

INSERT INTO `engine4_classified_categories` (`category_id`, `user_id`, `category_name`) VALUES
(1, 1, 'Arts & Culture'),
(2, 1, 'Business'),
(3, 1, 'Entertainment'),
(5, 1, 'Family & Home'),
(6, 1, 'Health'),
(7, 1, 'Recreation'),
(8, 1, 'Personal'),
(9, 1, 'Shopping'),
(10, 1, 'Society'),
(11, 1, 'Sports'),
(12, 1, 'Technology'),
(13, 1, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_classifieds`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_classifieds` (
  `classified_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `photo_id` int(10) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classified_id`),
  KEY `owner_id` (`owner_id`),
  KEY `search` (`search`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_fields_maps`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_fields_maps` (
  `field_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `order` smallint(6) NOT NULL,
  PRIMARY KEY (`field_id`,`option_id`,`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_classified_fields_maps`
--

INSERT INTO `engine4_classified_fields_maps` (`field_id`, `option_id`, `child_id`, `order`) VALUES
(0, 0, 2, 2),
(0, 0, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_fields_meta`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_fields_meta` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `label` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `display` tinyint(1) unsigned NOT NULL,
  `search` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `order` smallint(3) unsigned NOT NULL DEFAULT '999',
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  `validators` text COLLATE utf8_unicode_ci,
  `filters` text COLLATE utf8_unicode_ci,
  `style` text COLLATE utf8_unicode_ci,
  `error` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `engine4_classified_fields_meta`
--

INSERT INTO `engine4_classified_fields_meta` (`field_id`, `type`, `label`, `description`, `alias`, `required`, `display`, `search`, `show`, `order`, `config`, `validators`, `filters`, `style`, `error`) VALUES
(2, 'currency', 'Price', '', 'price', 0, 1, 1, 1, 999, '{"unit":"USD"}', NULL, NULL, NULL, NULL),
(3, 'location', 'Location', '', 'location', 0, 1, 1, 1, 999, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_fields_options`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_fields_options` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '999',
  PRIMARY KEY (`option_id`),
  KEY `field_id` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_fields_search`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_fields_search` (
  `item_id` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `price` (`price`),
  KEY `location` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_fields_values`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_fields_values` (
  `item_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `index` smallint(3) NOT NULL DEFAULT '0',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`item_id`,`field_id`,`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_classified_photos`
--

CREATE TABLE IF NOT EXISTS `engine4_classified_photos` (
  `photo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) unsigned NOT NULL,
  `classified_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `album_id` (`album_id`),
  KEY `classified_id` (`classified_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_adcampaigns`
--

CREATE TABLE IF NOT EXISTS `engine4_core_adcampaigns` (
  `adcampaign_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `end_settings` tinyint(4) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `limit_view` int(11) unsigned NOT NULL DEFAULT '0',
  `limit_click` int(11) unsigned NOT NULL DEFAULT '0',
  `limit_ctr` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `views` int(11) unsigned NOT NULL DEFAULT '0',
  `clicks` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adcampaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `engine4_core_adcampaigns`
--

INSERT INTO `engine4_core_adcampaigns` (`adcampaign_id`, `end_settings`, `name`, `start_time`, `end_time`, `limit_view`, `limit_click`, `limit_ctr`, `network`, `level`, `views`, `clicks`, `status`) VALUES
(1, 0, 'test ad', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0', '["1","2","3","4","5","6","7"]', '["1","2","3","4","5"]', 998, 36, 1),
(3, 0, 'fb', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0', '["1","2","3","4","5","6","7"]', '["1","2","3","4","5"]', 254, 0, 1),
(4, 0, 'twitter', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0', '["1","2","3","4","5","6","7"]', '["1","2","3","4","5"]', 254, 0, 1),
(5, 0, 'youtube', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0', '["1","2","3","4","5","6","7"]', '["1","2","3","4","5"]', 252, 0, 1),
(6, 0, 'rss', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0', '["1","2","3","4","5","6","7"]', '["1","2","3","4","5"]', 252, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_adphotos`
--

CREATE TABLE IF NOT EXISTS `engine4_core_adphotos` (
  `adphoto_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ad_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`adphoto_id`),
  KEY `ad_id` (`ad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `engine4_core_adphotos`
--

INSERT INTO `engine4_core_adphotos` (`adphoto_id`, `ad_id`, `title`, `description`, `file_id`, `creation_date`, `modified_date`) VALUES
(1, 0, '', '', 16, '2016-01-20 10:30:40', '2016-01-20 10:30:40'),
(5, 0, '', '', 24, '2016-01-20 10:51:24', '2016-01-20 10:51:24'),
(6, 0, '', '', 26, '2016-01-20 10:51:36', '2016-01-20 10:51:36'),
(7, 0, '', '', 28, '2016-01-20 10:51:51', '2016-01-20 10:51:51'),
(11, 0, '', '', 36, '2016-01-20 11:03:12', '2016-01-20 11:03:12'),
(12, 0, '', '', 38, '2016-01-20 11:04:45', '2016-01-20 11:04:45'),
(13, 0, '', '', 40, '2016-01-20 11:05:37', '2016-01-20 11:05:37'),
(14, 0, '', '', 42, '2016-01-20 11:06:27', '2016-01-20 11:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_ads`
--

CREATE TABLE IF NOT EXISTS `engine4_core_ads` (
  `ad_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `ad_campaign` int(11) unsigned NOT NULL,
  `views` int(11) unsigned NOT NULL DEFAULT '0',
  `clicks` int(11) unsigned NOT NULL DEFAULT '0',
  `media_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `html_code` text COLLATE utf8_unicode_ci NOT NULL,
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ad_id`),
  KEY `ad_campaign` (`ad_campaign`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `engine4_core_ads`
--

INSERT INTO `engine4_core_ads` (`ad_id`, `name`, `ad_campaign`, `views`, `clicks`, `media_type`, `html_code`, `photo_id`) VALUES
(1, 'banner ad', 1, 995, 36, '0', '<a href='''' target=''_blank''><img src=''/Rhetornews/public/user/10/0010_23fb.png?c=7bbf''/></a>', 1),
(5, 'fb_icon', 3, 254, 0, '0', '<a class=''social_icon'' href='''' target=''_blank''><img src=''/Rhetornews/public/user/24/0024_276f.png?c=1158''/></a>', 11),
(6, 'twitter icon', 4, 254, 0, '0', '<a class=''social_icon'' href='''' target=''_blank''><img src=''/Rhetornews/public/user/26/0026_b395.png?c=ccd5''/></a>', 12),
(7, 'youtube icon', 5, 251, 0, '0', '<a class=''social_icon'' href='''' target=''_blank''><img src=''/Rhetornews/public/user/28/0028_7f7b.png?c=ab7a''/></a>', 13),
(8, 'rss icon', 6, 251, 0, '0', '<a class=''social_icon'' href='''' target=''_blank''><img src=''/Rhetornews/public/user/2a/002a_a506.png?c=8539''/></a>', 14);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_auth`
--

CREATE TABLE IF NOT EXISTS `engine4_core_auth` (
  `id` varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `expires` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_bannedemails`
--

CREATE TABLE IF NOT EXISTS `engine4_core_bannedemails` (
  `bannedemail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bannedemail_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_bannedips`
--

CREATE TABLE IF NOT EXISTS `engine4_core_bannedips` (
  `bannedip_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start` varbinary(16) NOT NULL,
  `stop` varbinary(16) NOT NULL,
  PRIMARY KEY (`bannedip_id`),
  UNIQUE KEY `start` (`start`,`stop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_bannedusernames`
--

CREATE TABLE IF NOT EXISTS `engine4_core_bannedusernames` (
  `bannedusername_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bannedusername_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_bannedwords`
--

CREATE TABLE IF NOT EXISTS `engine4_core_bannedwords` (
  `bannedword_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `word` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bannedword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_comments`
--

CREATE TABLE IF NOT EXISTS `engine4_core_comments` (
  `comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resource_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `resource_id` int(11) unsigned NOT NULL,
  `poster_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `poster_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `like_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `resource_type` (`resource_type`,`resource_id`),
  KEY `poster_type` (`poster_type`,`poster_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `engine4_core_comments`
--

INSERT INTO `engine4_core_comments` (`comment_id`, `resource_type`, `resource_id`, `poster_type`, `poster_id`, `body`, `creation_date`, `like_count`) VALUES
(1, 'video', 1, 'user', 1, 'hello', '2016-01-01 12:29:31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_content`
--

CREATE TABLE IF NOT EXISTS `engine4_core_content` (
  `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'widget',
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `parent_content_id` int(11) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `params` text COLLATE utf8_unicode_ci,
  `attribs` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`content_id`),
  KEY `page_id` (`page_id`,`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1096 ;

--
-- Dumping data for table `engine4_core_content`
--

INSERT INTO `engine4_core_content` (`content_id`, `page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`) VALUES
(100, 1, 'container', 'main', NULL, 2, '[""]', NULL),
(110, 1, 'widget', 'core.menu-mini', 100, 4, '[""]', NULL),
(111, 1, 'widget', 'core.menu-logo', 100, 3, '{"title":"","name":"core.menu-logo","logo":"public\\/admin\\/logo.png","nomobile":"0"}', NULL),
(200, 2, 'container', 'main', NULL, 1, '', NULL),
(210, 2, 'widget', 'core.menu-footer', 200, 2, '', NULL),
(300, 3, 'container', 'main', NULL, 2, '[""]', NULL),
(312, 3, 'container', 'middle', 300, 6, '[""]', NULL),
(400, 4, 'container', 'main', NULL, 2, '[""]', NULL),
(410, 4, 'container', 'left', 400, 4, '[""]', NULL),
(411, 4, 'container', 'right', 400, 5, '[""]', NULL),
(412, 4, 'container', 'middle', 400, 6, '[""]', NULL),
(420, 4, 'widget', 'user.home-photo', 410, 7, '[""]', NULL),
(421, 4, 'widget', 'user.home-links', 410, 8, '[""]', NULL),
(422, 4, 'widget', 'user.list-online', 410, 9, '{"title":"%s Members Online"}', NULL),
(423, 4, 'widget', 'core.statistics', 410, 10, '{"title":"Network Stats"}', NULL),
(430, 4, 'widget', 'activity.list-requests', 411, 15, '{"title":"Requests"}', NULL),
(431, 4, 'widget', 'user.list-signups', 411, 17, '{"title":"Newest Members"}', NULL),
(432, 4, 'widget', 'user.list-popular', 411, 18, '{"title":"Popular Members"}', NULL),
(440, 4, 'widget', 'announcement.list-announcements', 412, 12, '{"0":"","title":"Announcements"}', NULL),
(441, 4, 'widget', 'activity.feed', 412, 13, '{"title":"What''s New"}', NULL),
(500, 5, 'container', 'main', NULL, 2, '[""]', NULL),
(511, 5, 'container', 'middle', 500, 6, '[""]', NULL),
(541, 5, 'widget', 'user.profile-fields', 511, 7, '{"title":"Info"}', NULL),
(547, 6, 'container', 'main', NULL, 1, NULL, NULL),
(548, 6, 'container', 'middle', 547, 2, NULL, NULL),
(549, 6, 'widget', 'core.content', 548, 1, NULL, NULL),
(550, 7, 'container', 'main', NULL, 1, NULL, NULL),
(551, 7, 'container', 'middle', 550, 2, NULL, NULL),
(552, 7, 'widget', 'core.content', 551, 1, NULL, NULL),
(553, 8, 'container', 'main', NULL, 1, NULL, NULL),
(554, 8, 'container', 'middle', 553, 2, NULL, NULL),
(555, 8, 'widget', 'core.content', 554, 1, NULL, NULL),
(556, 9, 'container', 'main', NULL, 1, NULL, NULL),
(557, 9, 'container', 'middle', 556, 1, NULL, NULL),
(558, 9, 'widget', 'core.content', 557, 1, NULL, NULL),
(559, 10, 'container', 'main', NULL, 1, NULL, NULL),
(560, 10, 'container', 'middle', 559, 1, NULL, NULL),
(561, 10, 'widget', 'core.content', 560, 1, NULL, NULL),
(562, 11, 'container', 'main', NULL, 2, '[]', NULL),
(563, 11, 'container', 'middle', 562, 6, '[]', NULL),
(564, 11, 'widget', 'core.content', 563, 7, '[]', NULL),
(565, 12, 'container', 'main', NULL, 2, '[]', NULL),
(566, 12, 'container', 'middle', 565, 6, '[]', NULL),
(567, 12, 'widget', 'core.content', 566, 7, '[]', NULL),
(568, 13, 'container', 'main', NULL, 1, NULL, NULL),
(569, 13, 'container', 'middle', 568, 1, NULL, NULL),
(570, 13, 'widget', 'core.content', 569, 1, NULL, NULL),
(571, 14, 'container', 'top', NULL, 1, NULL, NULL),
(572, 14, 'container', 'main', NULL, 2, NULL, NULL),
(573, 14, 'container', 'middle', 571, 1, NULL, NULL),
(574, 14, 'container', 'middle', 572, 2, NULL, NULL),
(575, 14, 'widget', 'user.settings-menu', 573, 1, NULL, NULL),
(576, 14, 'widget', 'core.content', 574, 1, NULL, NULL),
(577, 15, 'container', 'top', NULL, 1, NULL, NULL),
(578, 15, 'container', 'main', NULL, 2, NULL, NULL),
(579, 15, 'container', 'middle', 577, 1, NULL, NULL),
(580, 15, 'container', 'middle', 578, 2, NULL, NULL),
(581, 15, 'widget', 'user.settings-menu', 579, 1, NULL, NULL),
(582, 15, 'widget', 'core.content', 580, 1, NULL, NULL),
(583, 16, 'container', 'top', NULL, 1, NULL, NULL),
(584, 16, 'container', 'main', NULL, 2, NULL, NULL),
(585, 16, 'container', 'middle', 583, 1, NULL, NULL),
(586, 16, 'container', 'middle', 584, 2, NULL, NULL),
(587, 16, 'widget', 'user.settings-menu', 585, 1, NULL, NULL),
(588, 16, 'widget', 'core.content', 586, 1, NULL, NULL),
(589, 17, 'container', 'top', NULL, 1, NULL, NULL),
(590, 17, 'container', 'main', NULL, 2, NULL, NULL),
(591, 17, 'container', 'middle', 589, 1, NULL, NULL),
(592, 17, 'container', 'middle', 590, 2, NULL, NULL),
(593, 17, 'widget', 'user.settings-menu', 591, 1, NULL, NULL),
(594, 17, 'widget', 'core.content', 592, 1, NULL, NULL),
(595, 18, 'container', 'top', NULL, 1, NULL, NULL),
(596, 18, 'container', 'main', NULL, 2, NULL, NULL),
(597, 18, 'container', 'middle', 595, 1, NULL, NULL),
(598, 18, 'container', 'middle', 596, 2, NULL, NULL),
(599, 18, 'widget', 'user.settings-menu', 597, 1, NULL, NULL),
(600, 18, 'widget', 'core.content', 598, 1, NULL, NULL),
(601, 19, 'container', 'top', NULL, 1, NULL, NULL),
(602, 19, 'container', 'main', NULL, 2, NULL, NULL),
(603, 19, 'container', 'middle', 601, 1, NULL, NULL),
(604, 19, 'container', 'middle', 602, 2, NULL, NULL),
(605, 19, 'widget', 'user.settings-menu', 603, 1, NULL, NULL),
(606, 19, 'widget', 'core.content', 604, 1, NULL, NULL),
(607, 20, 'container', 'top', NULL, 1, NULL, NULL),
(608, 20, 'container', 'main', NULL, 2, NULL, NULL),
(609, 20, 'container', 'middle', 607, 1, NULL, NULL),
(610, 20, 'container', 'middle', 608, 2, NULL, NULL),
(611, 20, 'container', 'left', 608, 1, NULL, NULL),
(612, 20, 'widget', 'user.browse-menu', 609, 1, NULL, NULL),
(613, 20, 'widget', 'core.content', 610, 1, NULL, NULL),
(614, 20, 'widget', 'user.browse-search', 611, 1, NULL, NULL),
(615, 21, 'container', 'main', NULL, 1, NULL, NULL),
(616, 21, 'container', 'middle', 615, 1, NULL, NULL),
(617, 21, 'widget', 'core.content', 616, 1, NULL, NULL),
(618, 22, 'container', 'main', NULL, 1, NULL, NULL),
(619, 22, 'container', 'middle', 618, 1, NULL, NULL),
(620, 22, 'widget', 'core.content', 619, 2, NULL, NULL),
(621, 22, 'widget', 'messages.menu', 619, 1, NULL, NULL),
(622, 23, 'container', 'main', NULL, 1, NULL, NULL),
(623, 23, 'container', 'middle', 622, 1, NULL, NULL),
(624, 23, 'widget', 'core.content', 623, 2, NULL, NULL),
(625, 23, 'widget', 'messages.menu', 623, 1, NULL, NULL),
(626, 24, 'container', 'main', NULL, 1, NULL, NULL),
(627, 24, 'container', 'middle', 626, 1, NULL, NULL),
(628, 24, 'widget', 'core.content', 627, 2, NULL, NULL),
(629, 24, 'widget', 'messages.menu', 627, 1, NULL, NULL),
(630, 25, 'container', 'main', NULL, 1, NULL, NULL),
(631, 25, 'container', 'middle', 630, 1, NULL, NULL),
(632, 25, 'widget', 'core.content', 631, 2, NULL, NULL),
(633, 25, 'widget', 'messages.menu', 631, 1, NULL, NULL),
(634, 26, 'container', 'main', NULL, 1, NULL, NULL),
(635, 26, 'container', 'middle', 634, 1, NULL, NULL),
(636, 26, 'widget', 'core.content', 635, 2, NULL, NULL),
(637, 26, 'widget', 'messages.menu', 635, 1, NULL, NULL),
(638, 27, 'container', 'main', NULL, 1, NULL, NULL),
(639, 27, 'container', 'middle', 638, 2, NULL, NULL),
(640, 27, 'widget', 'core.content', 639, 1, NULL, NULL),
(641, 27, 'widget', 'core.comments', 639, 2, NULL, NULL),
(642, 28, 'container', 'main', NULL, 1, NULL, NULL),
(643, 28, 'container', 'middle', 642, 2, NULL, NULL),
(644, 28, 'widget', 'core.content', 643, 1, NULL, NULL),
(645, 28, 'widget', 'core.comments', 643, 2, NULL, NULL),
(646, 29, 'container', 'top', NULL, 1, NULL, NULL),
(647, 29, 'container', 'main', NULL, 2, NULL, NULL),
(648, 29, 'container', 'middle', 646, 1, NULL, NULL),
(649, 29, 'container', 'middle', 647, 2, NULL, NULL),
(650, 29, 'container', 'right', 647, 1, NULL, NULL),
(651, 29, 'widget', 'album.browse-menu', 648, 1, NULL, NULL),
(652, 29, 'widget', 'core.content', 649, 1, NULL, NULL),
(653, 29, 'widget', 'album.browse-search', 650, 1, NULL, NULL),
(654, 29, 'widget', 'album.browse-menu-quick', 650, 2, NULL, NULL),
(656, 30, 'container', 'top', NULL, 1, NULL, NULL),
(657, 30, 'container', 'main', NULL, 2, NULL, NULL),
(658, 30, 'container', 'middle', 656, 1, NULL, NULL),
(659, 30, 'container', 'middle', 657, 2, NULL, NULL),
(660, 30, 'widget', 'album.browse-menu', 658, 1, NULL, NULL),
(661, 30, 'widget', 'core.content', 659, 1, NULL, NULL),
(662, 31, 'container', 'top', NULL, 1, NULL, NULL),
(663, 31, 'container', 'main', NULL, 2, NULL, NULL),
(664, 31, 'container', 'middle', 662, 1, NULL, NULL),
(665, 31, 'container', 'middle', 663, 2, NULL, NULL),
(666, 31, 'container', 'right', 663, 1, NULL, NULL),
(667, 31, 'widget', 'album.browse-menu', 664, 1, NULL, NULL),
(668, 31, 'widget', 'core.content', 665, 1, NULL, NULL),
(669, 31, 'widget', 'album.browse-search', 666, 1, NULL, NULL),
(670, 31, 'widget', 'album.browse-menu-quick', 666, 2, NULL, NULL),
(672, 32, 'container', 'main', NULL, 1, NULL, NULL),
(673, 32, 'container', 'left', 672, 1, NULL, NULL),
(674, 32, 'container', 'middle', 672, 2, NULL, NULL),
(675, 32, 'widget', 'blog.gutter-photo', 673, 1, NULL, NULL),
(676, 32, 'widget', 'blog.gutter-menu', 673, 2, NULL, NULL),
(677, 32, 'widget', 'blog.gutter-search', 673, 3, NULL, NULL),
(678, 32, 'widget', 'core.content', 674, 1, NULL, NULL),
(679, 33, 'container', 'main', NULL, 1, NULL, NULL),
(680, 33, 'container', 'left', 679, 1, NULL, NULL),
(681, 33, 'container', 'middle', 679, 2, NULL, NULL),
(682, 33, 'widget', 'blog.gutter-photo', 680, 1, NULL, NULL),
(683, 33, 'widget', 'blog.gutter-menu', 680, 2, NULL, NULL),
(684, 33, 'widget', 'blog.gutter-search', 680, 3, NULL, NULL),
(685, 33, 'widget', 'core.content', 681, 1, NULL, NULL),
(686, 33, 'widget', 'core.comments', 681, 2, NULL, NULL),
(687, 34, 'container', 'top', NULL, 1, NULL, NULL),
(688, 34, 'container', 'main', NULL, 2, NULL, NULL),
(689, 34, 'container', 'middle', 687, 1, NULL, NULL),
(690, 34, 'container', 'middle', 688, 2, NULL, NULL),
(691, 34, 'container', 'right', 688, 1, NULL, NULL),
(692, 34, 'widget', 'blog.browse-menu', 689, 1, NULL, NULL),
(693, 34, 'widget', 'core.content', 690, 1, NULL, NULL),
(694, 34, 'widget', 'blog.browse-search', 691, 1, NULL, NULL),
(695, 34, 'widget', 'blog.browse-menu-quick', 691, 2, NULL, NULL),
(696, 35, 'container', 'top', NULL, 1, NULL, NULL),
(697, 35, 'container', 'main', NULL, 2, NULL, NULL),
(698, 35, 'container', 'middle', 696, 1, NULL, NULL),
(699, 35, 'container', 'middle', 697, 2, NULL, NULL),
(700, 35, 'widget', 'blog.browse-menu', 698, 1, NULL, NULL),
(701, 35, 'widget', 'core.content', 699, 1, NULL, NULL),
(702, 36, 'container', 'top', NULL, 1, NULL, NULL),
(703, 36, 'container', 'main', NULL, 2, NULL, NULL),
(704, 36, 'container', 'middle', 702, 1, NULL, NULL),
(705, 36, 'container', 'middle', 703, 2, NULL, NULL),
(706, 36, 'container', 'right', 703, 1, NULL, NULL),
(707, 36, 'widget', 'blog.browse-menu', 704, 1, NULL, NULL),
(708, 36, 'widget', 'core.content', 705, 1, NULL, NULL),
(709, 36, 'widget', 'blog.browse-search', 706, 1, NULL, NULL),
(710, 36, 'widget', 'blog.browse-menu-quick', 706, 2, NULL, NULL),
(711, 37, 'container', 'main', NULL, 1, '', NULL),
(712, 37, 'container', 'middle', 711, 3, '', NULL),
(713, 37, 'widget', 'core.content', 712, 1, '', NULL),
(715, 38, 'container', 'top', NULL, 1, NULL, NULL),
(716, 38, 'container', 'main', NULL, 2, NULL, NULL),
(717, 38, 'container', 'middle', 715, 1, NULL, NULL),
(718, 38, 'container', 'middle', 716, 2, NULL, NULL),
(719, 38, 'container', 'right', 716, 1, NULL, NULL),
(720, 38, 'widget', 'classified.browse-menu', 717, 1, NULL, NULL),
(721, 38, 'widget', 'core.content', 718, 1, NULL, NULL),
(722, 38, 'widget', 'classified.browse-search', 719, 1, NULL, NULL),
(723, 38, 'widget', 'classified.browse-menu-quick', 719, 2, NULL, NULL),
(724, 39, 'container', 'main', NULL, 1, '', NULL),
(725, 39, 'container', 'middle', 724, 3, '', NULL),
(726, 39, 'widget', 'core.content', 725, 1, '', NULL),
(727, 39, 'widget', 'core.comments', 725, 2, '', NULL),
(728, 40, 'container', 'top', NULL, 1, NULL, NULL),
(729, 40, 'container', 'main', NULL, 2, NULL, NULL),
(730, 40, 'container', 'middle', 728, 1, NULL, NULL),
(731, 40, 'container', 'middle', 729, 2, NULL, NULL),
(732, 40, 'widget', 'classified.browse-menu', 730, 1, NULL, NULL),
(733, 40, 'widget', 'core.content', 731, 1, NULL, NULL),
(734, 41, 'container', 'top', NULL, 1, NULL, NULL),
(735, 41, 'container', 'main', NULL, 2, NULL, NULL),
(736, 41, 'container', 'middle', 734, 1, NULL, NULL),
(737, 41, 'container', 'middle', 735, 2, NULL, NULL),
(738, 41, 'container', 'right', 735, 1, NULL, NULL),
(739, 41, 'widget', 'classified.browse-menu', 736, 1, NULL, NULL),
(740, 41, 'widget', 'core.content', 737, 1, NULL, NULL),
(741, 41, 'widget', 'classified.browse-search', 738, 1, NULL, NULL),
(742, 41, 'widget', 'classified.browse-menu-quick', 738, 2, NULL, NULL),
(743, 42, 'container', 'main', NULL, 1, '', NULL),
(744, 42, 'container', 'middle', 743, 2, '', NULL),
(745, 42, 'widget', 'event.profile-status', 744, 3, '', NULL),
(746, 42, 'widget', 'event.profile-photo', 744, 4, '', NULL),
(747, 42, 'widget', 'event.profile-info', 744, 5, '', NULL),
(748, 42, 'widget', 'event.profile-rsvp', 744, 6, '', NULL),
(749, 42, 'widget', 'core.container-tabs', 744, 7, '{"max":6}', NULL),
(750, 42, 'widget', 'activity.feed', 749, 8, '{"title":"What''s New"}', NULL),
(751, 42, 'widget', 'event.profile-members', 749, 9, '{"title":"Guests","titleCount":true}', NULL),
(752, 43, 'container', 'main', NULL, 1, '', NULL),
(753, 43, 'container', 'middle', 752, 3, '', NULL),
(754, 43, 'container', 'left', 752, 1, '', NULL),
(755, 43, 'widget', 'core.container-tabs', 753, 2, '{"max":"6"}', NULL),
(756, 43, 'widget', 'event.profile-status', 753, 1, '', NULL),
(757, 43, 'widget', 'event.profile-photo', 754, 1, '', NULL),
(758, 43, 'widget', 'event.profile-options', 754, 2, '', NULL),
(759, 43, 'widget', 'event.profile-info', 754, 3, '', NULL),
(760, 43, 'widget', 'event.profile-rsvp', 754, 4, '', NULL),
(761, 43, 'widget', 'activity.feed', 755, 1, '{"title":"Updates"}', NULL),
(762, 43, 'widget', 'event.profile-members', 755, 2, '{"title":"Guests","titleCount":true}', NULL),
(763, 43, 'widget', 'event.profile-photos', 755, 3, '{"title":"Photos","titleCount":true}', NULL),
(764, 43, 'widget', 'event.profile-discussions', 755, 4, '{"title":"Discussions","titleCount":true}', NULL),
(765, 43, 'widget', 'core.profile-links', 755, 5, '{"title":"Links","titleCount":true}', NULL),
(766, 4, 'widget', 'event.home-upcoming', 411, 16, '{"title":"Upcoming Events","titleCount":true}', NULL),
(768, 44, 'container', 'top', NULL, 1, NULL, NULL),
(769, 44, 'container', 'main', NULL, 2, NULL, NULL),
(770, 44, 'container', 'middle', 768, 1, NULL, NULL),
(771, 44, 'container', 'middle', 769, 2, NULL, NULL),
(772, 44, 'container', 'right', 769, 1, NULL, NULL),
(773, 44, 'widget', 'event.browse-menu', 770, 1, NULL, NULL),
(774, 44, 'widget', 'core.content', 771, 1, NULL, NULL),
(775, 44, 'widget', 'event.browse-search', 772, 1, NULL, NULL),
(776, 44, 'widget', 'event.browse-menu-quick', 772, 2, NULL, NULL),
(777, 45, 'container', 'top', NULL, 1, NULL, NULL),
(778, 45, 'container', 'main', NULL, 2, NULL, NULL),
(779, 45, 'container', 'middle', 777, 1, NULL, NULL),
(780, 45, 'container', 'middle', 778, 2, NULL, NULL),
(781, 45, 'widget', 'event.browse-menu', 779, 1, NULL, NULL),
(782, 45, 'widget', 'core.content', 780, 1, NULL, NULL),
(783, 46, 'container', 'top', NULL, 1, NULL, NULL),
(784, 46, 'container', 'main', NULL, 2, NULL, NULL),
(785, 46, 'container', 'middle', 783, 1, NULL, NULL),
(786, 46, 'container', 'middle', 784, 2, NULL, NULL),
(787, 46, 'container', 'right', 784, 1, NULL, NULL),
(788, 46, 'widget', 'event.browse-menu', 785, 1, NULL, NULL),
(789, 46, 'widget', 'core.content', 786, 1, NULL, NULL),
(790, 46, 'widget', 'event.browse-search', 787, 1, NULL, NULL),
(791, 46, 'widget', 'event.browse-menu-quick', 787, 2, NULL, NULL),
(793, 47, 'container', 'main', NULL, 1, NULL, NULL),
(794, 47, 'container', 'middle', 793, 1, NULL, NULL),
(795, 47, 'widget', 'core.content', 794, 1, NULL, NULL),
(796, 48, 'container', 'main', NULL, 1, NULL, NULL),
(797, 48, 'container', 'middle', 796, 1, NULL, NULL),
(798, 48, 'widget', 'core.content', 797, 1, NULL, NULL),
(799, 49, 'container', 'main', NULL, 1, NULL, NULL),
(800, 49, 'container', 'middle', 799, 1, NULL, NULL),
(801, 49, 'widget', 'core.content', 800, 1, NULL, NULL),
(803, 50, 'container', 'main', NULL, 1, '', NULL),
(804, 50, 'container', 'middle', 803, 3, '', NULL),
(805, 50, 'container', 'left', 803, 1, '', NULL),
(806, 50, 'widget', 'core.container-tabs', 804, 2, '{"max":"6"}', NULL),
(807, 50, 'widget', 'group.profile-status', 804, 1, '', NULL),
(808, 50, 'widget', 'group.profile-photo', 805, 1, '', NULL),
(809, 50, 'widget', 'group.profile-options', 805, 2, '', NULL),
(810, 50, 'widget', 'group.profile-info', 805, 3, '', NULL),
(811, 50, 'widget', 'activity.feed', 806, 1, '{"title":"Updates"}', NULL),
(812, 50, 'widget', 'group.profile-members', 806, 2, '{"title":"Members","titleCount":true}', NULL),
(813, 50, 'widget', 'group.profile-photos', 806, 3, '{"title":"Photos","titleCount":true}', NULL),
(814, 50, 'widget', 'group.profile-discussions', 806, 4, '{"title":"Discussions","titleCount":true}', NULL),
(815, 50, 'widget', 'core.profile-links', 806, 5, '{"title":"Links","titleCount":true}', NULL),
(816, 50, 'widget', 'group.profile-events', 806, 6, '{"title":"Events","titleCount":true}', NULL),
(817, 51, 'container', 'main', NULL, 1, '', NULL),
(818, 51, 'container', 'middle', 817, 2, '', NULL),
(819, 51, 'widget', 'group.profile-status', 818, 3, '', NULL),
(820, 51, 'widget', 'group.profile-photo', 818, 4, '', NULL),
(821, 51, 'widget', 'group.profile-info', 818, 5, '', NULL),
(822, 51, 'widget', 'core.container-tabs', 818, 6, '{"max":6}', NULL),
(823, 51, 'widget', 'activity.feed', 822, 7, '{"title":"What''s New"}', NULL),
(824, 51, 'widget', 'group.profile-members', 822, 8, '{"title":"Members","titleCount":true}', NULL),
(825, 52, 'container', 'top', NULL, 1, NULL, NULL),
(826, 52, 'container', 'main', NULL, 2, NULL, NULL),
(827, 52, 'container', 'middle', 825, 1, NULL, NULL),
(828, 52, 'container', 'middle', 826, 2, NULL, NULL),
(829, 52, 'container', 'right', 826, 1, NULL, NULL),
(830, 52, 'widget', 'group.browse-menu', 827, 1, NULL, NULL),
(831, 52, 'widget', 'core.content', 828, 1, NULL, NULL),
(832, 52, 'widget', 'group.browse-search', 829, 1, NULL, NULL),
(833, 52, 'widget', 'group.browse-menu-quick', 829, 2, NULL, NULL),
(834, 53, 'container', 'top', NULL, 1, NULL, NULL),
(835, 53, 'container', 'main', NULL, 2, NULL, NULL),
(836, 53, 'container', 'middle', 834, 1, NULL, NULL),
(837, 53, 'container', 'middle', 835, 2, NULL, NULL),
(838, 53, 'widget', 'group.browse-menu', 836, 1, NULL, NULL),
(839, 53, 'widget', 'core.content', 837, 1, NULL, NULL),
(840, 54, 'container', 'top', NULL, 1, NULL, NULL),
(841, 54, 'container', 'main', NULL, 2, NULL, NULL),
(842, 54, 'container', 'middle', 840, 1, NULL, NULL),
(843, 54, 'container', 'middle', 841, 2, NULL, NULL),
(844, 54, 'container', 'right', 841, 1, NULL, NULL),
(845, 54, 'widget', 'group.browse-menu', 842, 1, NULL, NULL),
(846, 54, 'widget', 'core.content', 843, 1, NULL, NULL),
(847, 54, 'widget', 'group.browse-search', 844, 1, NULL, NULL),
(848, 54, 'widget', 'group.browse-menu-quick', 844, 2, NULL, NULL),
(849, 55, 'container', 'main', NULL, 1, '', NULL),
(850, 55, 'widget', 'core.menu-logo', 849, 2, '', NULL),
(851, 55, 'widget', 'mobi.mobi-menu-main', 849, 3, '', NULL),
(852, 56, 'container', 'main', NULL, 1, '', NULL),
(853, 56, 'widget', 'mobi.mobi-footer', 852, 2, '', NULL),
(854, 57, 'container', 'main', NULL, 1, '', NULL),
(855, 57, 'container', 'middle', 854, 2, '', NULL),
(856, 57, 'widget', 'user.login-or-signup', 855, 3, '', NULL),
(857, 58, 'container', 'main', NULL, 1, '', NULL),
(858, 58, 'container', 'middle', 857, 2, '', NULL),
(859, 58, 'widget', 'activity.feed', 858, 3, '', NULL),
(860, 59, 'container', 'main', NULL, 1, '', NULL),
(861, 59, 'container', 'middle', 860, 2, '', NULL),
(862, 59, 'widget', 'user.profile-photo', 861, 3, '', NULL),
(863, 59, 'widget', 'user.profile-status', 861, 4, '', NULL),
(864, 59, 'widget', 'mobi.mobi-profile-options', 861, 5, '', NULL),
(865, 59, 'widget', 'core.container-tabs', 861, 6, '{"max":6}', NULL),
(866, 59, 'widget', 'activity.feed', 865, 7, '{"title":"What''s New"}', NULL),
(867, 59, 'widget', 'user.profile-fields', 865, 8, '{"title":"Info"}', NULL),
(868, 59, 'widget', 'user.profile-friends', 865, 9, '{"title":"Friends","titleCount":true}', NULL),
(871, 60, 'container', 'top', NULL, 1, NULL, NULL),
(872, 60, 'container', 'main', NULL, 2, NULL, NULL),
(873, 60, 'container', 'middle', 871, 1, NULL, NULL),
(874, 60, 'container', 'middle', 872, 2, NULL, NULL),
(875, 60, 'container', 'right', 872, 1, NULL, NULL),
(876, 60, 'widget', 'music.browse-menu', 873, 1, NULL, NULL),
(877, 60, 'widget', 'core.content', 874, 1, NULL, NULL),
(878, 60, 'widget', 'music.browse-search', 875, 1, NULL, NULL),
(879, 60, 'widget', 'music.browse-menu-quick', 875, 2, NULL, NULL),
(880, 61, 'container', 'main', NULL, 1, NULL, NULL),
(881, 61, 'container', 'middle', 880, 1, NULL, NULL),
(882, 61, 'widget', 'core.content', 881, 1, NULL, NULL),
(883, 61, 'widget', 'core.comments', 881, 2, NULL, NULL),
(884, 62, 'container', 'top', NULL, 1, NULL, NULL),
(885, 62, 'container', 'main', NULL, 2, NULL, NULL),
(886, 62, 'container', 'middle', 884, 1, NULL, NULL),
(887, 62, 'container', 'middle', 885, 2, NULL, NULL),
(888, 62, 'widget', 'music.browse-menu', 886, 1, NULL, NULL),
(889, 62, 'widget', 'core.content', 887, 1, NULL, NULL),
(890, 63, 'container', 'top', NULL, 1, NULL, NULL),
(891, 63, 'container', 'main', NULL, 2, NULL, NULL),
(892, 63, 'container', 'middle', 890, 1, NULL, NULL),
(893, 63, 'container', 'middle', 891, 2, NULL, NULL),
(894, 63, 'container', 'right', 891, 1, NULL, NULL),
(895, 63, 'widget', 'music.browse-menu', 892, 1, NULL, NULL),
(896, 63, 'widget', 'core.content', 893, 1, NULL, NULL),
(897, 63, 'widget', 'music.browse-search', 894, 1, NULL, NULL),
(898, 63, 'widget', 'music.browse-menu-quick', 894, 2, NULL, NULL),
(900, 64, 'container', 'top', NULL, 1, NULL, NULL),
(901, 64, 'container', 'main', NULL, 2, NULL, NULL),
(902, 64, 'container', 'middle', 900, 1, NULL, NULL),
(903, 64, 'container', 'middle', 901, 2, NULL, NULL),
(904, 64, 'container', 'right', 901, 1, NULL, NULL),
(905, 64, 'widget', 'poll.browse-menu', 902, 1, NULL, NULL),
(906, 64, 'widget', 'core.content', 903, 1, NULL, NULL),
(907, 64, 'widget', 'poll.browse-search', 904, 1, NULL, NULL),
(908, 64, 'widget', 'poll.browse-menu-quick', 904, 2, NULL, NULL),
(909, 65, 'container', 'main', NULL, 1, '', NULL),
(910, 65, 'container', 'middle', 909, 3, '', NULL),
(911, 65, 'widget', 'core.content', 910, 1, '', NULL),
(912, 65, 'widget', 'core.comments', 910, 2, '', NULL),
(913, 66, 'container', 'top', NULL, 1, NULL, NULL),
(914, 66, 'container', 'main', NULL, 2, NULL, NULL),
(915, 66, 'container', 'middle', 913, 1, NULL, NULL),
(916, 66, 'container', 'middle', 914, 2, NULL, NULL),
(917, 66, 'widget', 'poll.browse-menu', 915, 1, NULL, NULL),
(918, 66, 'widget', 'core.content', 916, 1, NULL, NULL),
(919, 67, 'container', 'top', NULL, 1, NULL, NULL),
(920, 67, 'container', 'main', NULL, 2, NULL, NULL),
(921, 67, 'container', 'middle', 919, 1, NULL, NULL),
(922, 67, 'container', 'middle', 920, 2, NULL, NULL),
(923, 67, 'container', 'right', 920, 1, NULL, NULL),
(924, 67, 'widget', 'poll.browse-menu', 921, 1, NULL, NULL),
(925, 67, 'widget', 'core.content', 922, 1, NULL, NULL),
(926, 67, 'widget', 'poll.browse-search', 923, 1, NULL, NULL),
(927, 67, 'widget', 'poll.browse-menu-quick', 923, 2, NULL, NULL),
(929, 68, 'container', 'main', NULL, 1, '', NULL),
(930, 68, 'container', 'right', 929, 1, '', NULL),
(931, 68, 'container', 'middle', 929, 3, '', NULL),
(932, 68, 'widget', 'core.content', 931, 1, '', NULL),
(933, 68, 'widget', 'core.comments', 931, 2, '', NULL),
(934, 68, 'widget', 'video.show-same-tags', 930, 1, '', NULL),
(935, 68, 'widget', 'video.show-also-liked', 930, 2, '', NULL),
(936, 68, 'widget', 'video.show-same-poster', 930, 3, '', NULL),
(937, 69, 'container', 'top', NULL, 1, NULL, NULL),
(938, 69, 'container', 'main', NULL, 2, NULL, NULL),
(939, 69, 'container', 'middle', 937, 1, NULL, NULL),
(940, 69, 'container', 'middle', 938, 2, NULL, NULL),
(941, 69, 'container', 'right', 938, 1, NULL, NULL),
(942, 69, 'widget', 'video.browse-menu', 939, 1, NULL, NULL),
(943, 69, 'widget', 'core.content', 940, 1, NULL, NULL),
(944, 69, 'widget', 'video.browse-search', 941, 1, NULL, NULL),
(945, 69, 'widget', 'video.browse-menu-quick', 941, 2, NULL, NULL),
(946, 70, 'container', 'top', NULL, 1, NULL, NULL),
(947, 70, 'container', 'main', NULL, 2, NULL, NULL),
(948, 70, 'container', 'middle', 946, 1, NULL, NULL),
(949, 70, 'container', 'middle', 947, 2, NULL, NULL),
(950, 70, 'widget', 'video.browse-menu', 948, 1, NULL, NULL),
(951, 70, 'widget', 'core.content', 949, 1, NULL, NULL),
(952, 71, 'container', 'top', NULL, 1, NULL, NULL),
(953, 71, 'container', 'main', NULL, 2, NULL, NULL),
(954, 71, 'container', 'middle', 952, 1, NULL, NULL),
(955, 71, 'container', 'middle', 953, 2, NULL, NULL),
(956, 71, 'container', 'right', 953, 1, NULL, NULL),
(957, 71, 'widget', 'video.browse-menu', 954, 1, NULL, NULL),
(958, 71, 'widget', 'core.content', 955, 1, NULL, NULL),
(959, 71, 'widget', 'video.browse-search', 956, 1, NULL, NULL),
(960, 71, 'widget', 'video.browse-menu-quick', 956, 2, NULL, NULL),
(961, 3, 'widget', 'rss', 312, 8, '{"timeout":"900","title":"","url":"http:\\/\\/feeds.reuters.com\\/reuters\\/topNews","strip":"1","nomobile":"0","name":"rss"}', NULL),
(978, 1, 'widget', 'core.html-block', 100, 2, '{"title":"","data":"<ul class=\\"mob_toplinkstoogle\\">\\r\\n                            <li><a href=\\"#\\"><img src=''\\/Rhetornews\\/public\\/user\\/24\\/0024_276f.png?c=1158''\\/><\\/a><\\/li>\\r\\n                            <li><a href=\\"#\\"><img src=''\\/Rhetornews\\/public\\/user\\/26\\/0026_b395.png?c=ccd5''\\/><\\/a><\\/li>\\r\\n                            <li><a href=\\"#\\"><img src=''\\/Rhetornews\\/public\\/user\\/28\\/0028_7f7b.png?c=ab7a''\\/><\\/a><\\/li>\\r\\n                            <li><a href=\\"#\\"><img src=''\\/Rhetornews\\/public\\/user\\/2a\\/002a_a506.png?c=8539''\\/><\\/a><\\/li>\\r\\n                        <\\/ul>","nomobile":"0","name":"core.html-block"}', NULL),
(979, 3, 'widget', 'core.menu-main', 985, 5, '["[]"]', NULL),
(980, 72, 'container', 'main', NULL, 2, '["[]"]', NULL),
(981, 72, 'container', 'middle', 980, 6, '["[]"]', NULL),
(983, 72, 'widget', 'rss', 981, 8, '{"timeout":"","title":"","url":"http:\\/\\/www.sciencedaily.com\\/rss\\/top.xml","strip":"1","nomobile":"0","name":"rss"}', NULL),
(984, 3, 'container', 'top', NULL, 1, '["[]"]', NULL),
(985, 3, 'container', 'middle', 984, 6, '["[]"]', NULL),
(987, 3, 'widget', 'core.ad-campaign', 985, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(988, 11, 'container', 'top', NULL, 1, '["[]"]', NULL),
(989, 11, 'container', 'middle', 988, 6, '["[]"]', NULL),
(990, 11, 'widget', 'core.ad-campaign', 989, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(991, 11, 'widget', 'core.menu-main', 989, 4, '["[]"]', NULL),
(992, 12, 'container', 'top', NULL, 1, '[]', NULL),
(993, 12, 'container', 'middle', 992, 6, '[]', NULL),
(994, 12, 'widget', 'core.ad-campaign', 993, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(995, 12, 'widget', 'core.menu-main', 993, 4, '[]', NULL),
(996, 4, 'container', 'top', NULL, 1, '["[]"]', NULL),
(997, 4, 'container', 'middle', 996, 6, '["[]"]', NULL),
(998, 4, 'widget', 'core.ad-campaign', 997, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(999, 4, 'widget', 'core.menu-main', 997, 4, '["[]"]', NULL),
(1000, 5, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1001, 5, 'container', 'middle', 1000, 6, '["[]"]', NULL),
(1002, 5, 'widget', 'core.ad-campaign', 1001, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1003, 5, 'widget', 'core.menu-main', 1001, 4, '["[]"]', NULL),
(1004, 5, 'widget', 'user.profile-photo', 511, 8, '["[]"]', NULL),
(1005, 5, 'widget', 'user.profile-friends', 511, 9, '{"title":"Friends","titleCount":true,"name":"user.profile-friends","itemCountPerPage":"9"}', NULL),
(1007, 5, 'widget', 'rss', 511, 10, '{"timeout":"","title":"Our Recent posts","url":"http:\\/\\/www.sciencedaily.com\\/rss\\/top.xml","strip":"1","nomobile":"0","name":"rss"}', NULL),
(1008, 73, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1009, 73, 'container', 'middle', 1008, 6, '["[]"]', NULL),
(1010, 73, 'widget', 'rss', 1009, 7, '{"timeout":"","title":"","url":"http:\\/\\/rss.cnn.com\\/rss\\/cnn_topstories.rss","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1011, 73, 'container', 'top', NULL, 1, '[]', NULL),
(1012, 73, 'container', 'middle', 1011, 6, '[]', NULL),
(1013, 73, 'widget', 'core.ad-campaign', 1012, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1014, 73, 'widget', 'core.menu-main', 1012, 4, '[]', NULL),
(1015, 74, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1016, 74, 'container', 'middle', 1015, 6, '["[]"]', NULL),
(1017, 74, 'widget', 'rss', 1016, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.reuters.com\\/reuters\\/topNews","strip":"1","nomobile":"0","name":"rss"}', NULL),
(1018, 74, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1019, 74, 'container', 'middle', 1018, 6, '["[]"]', NULL),
(1020, 74, 'widget', 'core.ad-campaign', 1019, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1021, 74, 'widget', 'core.menu-main', 1019, 5, '["[]"]', NULL),
(1024, 76, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1025, 76, 'container', 'middle', 1024, 6, '["[]"]', NULL),
(1026, 76, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1027, 76, 'container', 'middle', 1026, 6, '["[]"]', NULL),
(1028, 76, 'widget', 'core.ad-campaign', 1027, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1029, 76, 'widget', 'core.menu-main', 1027, 5, '["[]"]', NULL),
(1030, 76, 'widget', 'rss', 1025, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.foxnews.com\\/foxnews\\/latest","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1031, 72, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1032, 72, 'container', 'middle', 1031, 6, '["[]"]', NULL),
(1033, 72, 'widget', 'core.ad-campaign', 1032, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1034, 72, 'widget', 'core.menu-main', 1032, 5, '["[]"]', NULL),
(1035, 77, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1036, 77, 'container', 'middle', 1035, 6, '["[]"]', NULL),
(1037, 77, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1038, 77, 'container', 'middle', 1037, 6, '["[]"]', NULL),
(1039, 77, 'widget', 'core.ad-campaign', 1038, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1040, 77, 'widget', 'core.menu-main', 1038, 5, '["[]"]', NULL),
(1041, 77, 'widget', 'rss', 1036, 8, '{"timeout":"","title":"","url":"http:\\/\\/rss.cnn.com\\/rss\\/cnn_allpolitics.rss","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1042, 78, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1043, 78, 'container', 'middle', 1042, 6, '["[]"]', NULL),
(1044, 78, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1045, 78, 'container', 'middle', 1044, 6, '["[]"]', NULL),
(1046, 78, 'widget', 'core.ad-campaign', 1045, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1047, 78, 'widget', 'core.menu-main', 1045, 5, '["[]"]', NULL),
(1048, 78, 'widget', 'rss', 1043, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.reuters.com\\/Reuters\\/PoliticsNews","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1049, 79, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1050, 79, 'container', 'middle', 1049, 6, '["[]"]', NULL),
(1051, 79, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1052, 79, 'container', 'middle', 1051, 6, '["[]"]', NULL),
(1053, 79, 'widget', 'core.ad-campaign', 1052, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1054, 79, 'widget', 'core.menu-main', 1052, 5, '["[]"]', NULL),
(1055, 79, 'widget', 'rss', 1050, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.foxnews.com\\/foxnews\\/politics","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1056, 80, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1057, 80, 'container', 'middle', 1056, 6, '["[]"]', NULL),
(1058, 80, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1059, 80, 'container', 'middle', 1058, 6, '["[]"]', NULL),
(1060, 80, 'widget', 'core.ad-campaign', 1059, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1061, 80, 'widget', 'core.menu-main', 1059, 5, '["[]"]', NULL),
(1062, 80, 'widget', 'rss', 1057, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.foxnews.com\\/foxnews\\/latest","strip":"1","nomobile":"0","name":"rss"}', NULL),
(1063, 81, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1064, 81, 'container', 'middle', 1063, 6, '["[]"]', NULL),
(1065, 81, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1066, 81, 'container', 'middle', 1065, 6, '["[]"]', NULL),
(1067, 81, 'widget', 'core.ad-campaign', 1066, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1068, 81, 'widget', 'core.menu-main', 1066, 5, '["[]"]', NULL),
(1069, 81, 'widget', 'rss', 1064, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.reuters.com\\/Reuters\\/PoliticsNews","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1070, 82, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1071, 82, 'container', 'middle', 1070, 6, '["[]"]', NULL),
(1072, 82, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1073, 82, 'container', 'middle', 1072, 6, '["[]"]', NULL),
(1074, 82, 'widget', 'core.ad-campaign', 1073, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1075, 82, 'widget', 'core.menu-main', 1073, 5, '["[]"]', NULL),
(1076, 82, 'widget', 'rss', 1071, 8, '{"timeout":"","title":"","url":"http:\\/\\/rss.cnn.com\\/rss\\/cnn_allpolitics.rss","strip":"0","nomobile":"0","name":"rss"}', NULL),
(1077, 83, 'container', 'main', NULL, 2, '["[]"]', NULL),
(1078, 83, 'container', 'middle', 1077, 6, '["[]"]', NULL),
(1079, 83, 'container', 'top', NULL, 1, '["[]"]', NULL),
(1080, 83, 'container', 'middle', 1079, 6, '["[]"]', NULL),
(1081, 83, 'widget', 'core.ad-campaign', 1080, 3, '{"title":"","name":"core.ad-campaign","adcampaign_id":"1","nomobile":"0"}', NULL),
(1082, 83, 'widget', 'core.menu-main', 1080, 5, '["[]"]', NULL),
(1083, 83, 'widget', 'rss', 1078, 8, '{"timeout":"","title":"","url":"http:\\/\\/feeds.foxnews.com\\/foxnews\\/latest","strip":"1","nomobile":"0","name":"rss"}', NULL),
(1084, 3, 'widget', 'core.menu-generic', 985, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1085, 77, 'widget', 'core.menu-generic', 1038, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1086, 83, 'widget', 'core.menu-generic', 1080, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1087, 76, 'widget', 'core.menu-generic', 1027, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1088, 80, 'widget', 'core.menu-generic', 1059, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1089, 82, 'widget', 'core.menu-generic', 1073, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1090, 79, 'widget', 'core.menu-generic', 1052, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1091, 81, 'widget', 'core.menu-generic', 1066, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1092, 78, 'widget', 'core.menu-generic', 1045, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1093, 72, 'widget', 'core.menu-generic', 1032, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL),
(1094, 74, 'widget', 'core.menu-generic', 1019, 4, '{"title":"","name":"core.menu-generic","menu":"custom_32","ulClass":"","nomobile":"0"}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_geotags`
--

CREATE TABLE IF NOT EXISTS `engine4_core_geotags` (
  `geotag_id` int(11) unsigned NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`geotag_id`),
  KEY `latitude` (`latitude`,`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_jobs`
--

CREATE TABLE IF NOT EXISTS `engine4_core_jobs` (
  `job_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jobtype_id` int(10) unsigned NOT NULL,
  `state` enum('pending','active','sleeping','failed','cancelled','completed','timeout') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `is_complete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `progress` decimal(5,4) unsigned NOT NULL DEFAULT '0.0000',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `started_date` datetime DEFAULT NULL,
  `completion_date` datetime DEFAULT NULL,
  `priority` mediumint(9) NOT NULL DEFAULT '100',
  `data` text COLLATE utf8_unicode_ci,
  `messages` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`job_id`),
  KEY `jobtype_id` (`jobtype_id`),
  KEY `state` (`state`),
  KEY `is_complete` (`is_complete`,`priority`,`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_jobtypes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_jobtypes` (
  `jobtype_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `module` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `plugin` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `form` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `priority` mediumint(9) NOT NULL DEFAULT '100',
  `multi` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`jobtype_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `engine4_core_jobtypes`
--

INSERT INTO `engine4_core_jobtypes` (`jobtype_id`, `title`, `type`, `module`, `plugin`, `form`, `enabled`, `priority`, `multi`) VALUES
(1, 'Download File', 'file_download', 'core', 'Core_Plugin_Job_FileDownload', 'Core_Form_Admin_Job_FileDownload', 1, 100, 1),
(2, 'Upload File', 'file_upload', 'core', 'Core_Plugin_Job_FileUpload', 'Core_Form_Admin_Job_FileUpload', 1, 100, 1),
(3, 'Rebuild Activity Privacy', 'activity_maintenance_rebuild_privacy', 'activity', 'Activity_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(4, 'Rebuild Member Privacy', 'user_maintenance_rebuild_privacy', 'user', 'User_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(5, 'Rebuild Network Membership', 'network_maintenance_rebuild_membership', 'network', 'Network_Plugin_Job_Maintenance_RebuildMembership', NULL, 1, 50, 1),
(6, 'Storage Transfer', 'storage_transfer', 'core', 'Storage_Plugin_Job_Transfer', 'Core_Form_Admin_Job_Generic', 1, 100, 1),
(7, 'Storage Cleanup', 'storage_cleanup', 'core', 'Storage_Plugin_Job_Cleanup', 'Core_Form_Admin_Job_Generic', 1, 100, 1),
(8, 'Rebuild Album Privacy', 'album_maintenance_rebuild_privacy', 'album', 'Album_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(9, 'Rebuild Blog Privacy', 'blog_maintenance_rebuild_privacy', 'blog', 'Blog_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(10, 'Rebuild Classified Privacy', 'classified_maintenance_rebuild_privacy', 'classified', 'Classified_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(11, 'Rebuild Event Privacy', 'event_maintenance_rebuild_privacy', 'event', 'Event_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(12, 'Rebuild Group Privacy', 'group_maintenance_rebuild_privacy', 'group', 'Group_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(13, 'Rebuild Music Privacy', 'music_maintenance_rebuild_privacy', 'music', 'Music_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(14, 'Rebuild Poll Privacy', 'poll_maintenance_rebuild_privacy', 'poll', 'Poll_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1),
(15, 'Video Encode', 'video_encode', 'video', 'Video_Plugin_Job_Encode', NULL, 1, 75, 2),
(16, 'Rebuild Video Privacy', 'video_maintenance_rebuild_privacy', 'video', 'Video_Plugin_Job_Maintenance_RebuildPrivacy', NULL, 1, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_languages`
--

CREATE TABLE IF NOT EXISTS `engine4_core_languages` (
  `language_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fallback` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `order` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `engine4_core_languages`
--

INSERT INTO `engine4_core_languages` (`language_id`, `code`, `name`, `fallback`, `order`) VALUES
(1, 'en', 'English', 'en', 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_likes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_likes` (
  `like_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resource_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `resource_id` int(11) unsigned NOT NULL,
  `poster_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `poster_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`like_id`),
  KEY `resource_type` (`resource_type`,`resource_id`),
  KEY `poster_type` (`poster_type`,`poster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_links`
--

CREATE TABLE IF NOT EXISTS `engine4_core_links` (
  `link_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `parent_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  `owner_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `view_count` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`link_id`),
  KEY `owner` (`owner_type`,`owner_id`),
  KEY `parent` (`parent_type`,`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_listitems`
--

CREATE TABLE IF NOT EXISTS `engine4_core_listitems` (
  `listitem_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`listitem_id`),
  KEY `list_id` (`list_id`),
  KEY `child_id` (`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_lists`
--

CREATE TABLE IF NOT EXISTS `engine4_core_lists` (
  `list_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `owner_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `child_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `child_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`),
  KEY `owner_type` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_log`
--

CREATE TABLE IF NOT EXISTS `engine4_core_log` (
  `message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `plugin` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `priority` smallint(2) NOT NULL DEFAULT '6',
  `priorityName` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INFO',
  PRIMARY KEY (`message_id`),
  KEY `domain` (`domain`,`timestamp`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_mail`
--

CREATE TABLE IF NOT EXISTS `engine4_core_mail` (
  `mail_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('system','zend') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `priority` smallint(3) DEFAULT '100',
  `recipient_count` int(11) unsigned DEFAULT '0',
  `recipient_total` int(10) NOT NULL DEFAULT '0',
  `creation_time` datetime NOT NULL,
  PRIMARY KEY (`mail_id`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_mailrecipients`
--

CREATE TABLE IF NOT EXISTS `engine4_core_mailrecipients` (
  `recipient_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_mailtemplates`
--

CREATE TABLE IF NOT EXISTS `engine4_core_mailtemplates` (
  `mailtemplate_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `module` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vars` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`mailtemplate_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `engine4_core_mailtemplates`
--

INSERT INTO `engine4_core_mailtemplates` (`mailtemplate_id`, `type`, `module`, `vars`) VALUES
(1, 'header', 'core', ''),
(2, 'footer', 'core', ''),
(3, 'header_member', 'core', ''),
(4, 'footer_member', 'core', ''),
(5, 'core_contact', 'core', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_name],[sender_email],[sender_link],[sender_photo],[message]'),
(6, 'core_verification', 'core', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[object_link]'),
(7, 'core_verification_password', 'core', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[object_link],[password]'),
(8, 'core_welcome', 'core', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[object_link]'),
(9, 'core_welcome_password', 'core', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[object_link],[password]'),
(10, 'notify_admin_user_signup', 'core', '[host],[email],[date],[recipient_title],[object_title],[object_link]'),
(11, 'core_lostpassword', 'core', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[object_link]'),
(12, 'notify_commented', 'activity', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(13, 'notify_commented_commented', 'activity', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(14, 'notify_liked', 'activity', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(15, 'notify_liked_commented', 'activity', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(16, 'user_account_approved', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo]'),
(17, 'notify_friend_accepted', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(18, 'notify_friend_request', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(19, 'notify_friend_follow_request', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(20, 'notify_friend_follow_accepted', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(21, 'notify_friend_follow', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(22, 'notify_post_user', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(23, 'notify_tagged', 'user', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(24, 'notify_message_new', 'messages', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(25, 'invite', 'invite', '[host],[email],[sender_email],[sender_title],[sender_link],[sender_photo],[message],[object_link],[code]'),
(26, 'invite_code', 'invite', '[host],[email],[sender_email],[sender_title],[sender_link],[sender_photo],[message],[object_link],[code]'),
(27, 'payment_subscription_active', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(28, 'payment_subscription_cancelled', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(29, 'payment_subscription_expired', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(30, 'payment_subscription_overdue', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(31, 'payment_subscription_pending', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(32, 'payment_subscription_recurrence', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(33, 'payment_subscription_refunded', 'payment', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[subscription_title],[subscription_description],[object_link]'),
(34, 'notify_blog_subscribed_new', 'blog', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(35, 'notify_event_accepted', 'event', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(36, 'notify_event_approve', 'event', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(37, 'notify_event_discussion_response', 'event', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(38, 'notify_event_discussion_reply', 'event', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(39, 'notify_event_invite', 'event', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(40, 'notify_forum_topic_reply', 'forum', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(41, 'notify_forum_topic_response', 'forum', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(42, 'notify_forum_promote', 'forum', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(43, 'notify_group_accepted', 'group', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(44, 'notify_group_approve', 'group', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(45, 'notify_group_discussion_reply', 'group', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(46, 'notify_group_discussion_response', 'group', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(47, 'notify_group_invite', 'group', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(48, 'notify_group_promote', 'group', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(49, 'notify_video_processed', 'video', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]'),
(50, 'notify_video_processed_failed', 'video', '[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_menuitems`
--

CREATE TABLE IF NOT EXISTS `engine4_core_menuitems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `module` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `plugin` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `menu` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `submenu` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `custom` tinyint(1) NOT NULL DEFAULT '0',
  `order` smallint(6) NOT NULL DEFAULT '999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `LOOKUP` (`name`,`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=279 ;

--
-- Dumping data for table `engine4_core_menuitems`
--

INSERT INTO `engine4_core_menuitems` (`id`, `name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
(1, 'core_main_home', 'core', 'Home', 'User_Plugin_Menus', '', 'core_main', '', 1, 0, 1),
(2, 'core_sitemap_home', 'core', 'Home', '', '{"route":"default"}', 'core_sitemap', '', 1, 0, 1),
(3, 'core_footer_privacy', 'core', 'Privacy', '', '{"route":"default","module":"core","controller":"help","action":"privacy"}', 'core_footer', '', 1, 0, 1),
(4, 'core_footer_terms', 'core', 'Terms of Service', '', '{"route":"default","module":"core","controller":"help","action":"terms"}', 'core_footer', '', 1, 0, 2),
(5, 'core_footer_contact', 'core', 'Contact', '', '{"route":"default","module":"core","controller":"help","action":"contact"}', 'core_footer', '', 1, 0, 3),
(6, 'core_mini_admin', 'core', 'Admin', 'User_Plugin_Menus', '', 'core_mini', '', 0, 0, 6),
(7, 'core_mini_profile', 'user', 'My Profile', 'User_Plugin_Menus', '', 'core_mini', '', 0, 0, 5),
(8, 'core_mini_settings', 'user', 'Settings', 'User_Plugin_Menus', '', 'core_mini', '', 0, 0, 3),
(9, 'core_mini_auth', 'user', 'Auth', 'User_Plugin_Menus', '', 'core_mini', '', 1, 0, 2),
(10, 'core_mini_signup', 'user', 'Signup', 'User_Plugin_Menus', '', 'core_mini', '', 1, 0, 1),
(11, 'core_admin_main_home', 'core', 'Home', '', '{"route":"admin_default"}', 'core_admin_main', '', 1, 0, 1),
(12, 'core_admin_main_manage', 'core', 'Manage', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_manage', 1, 0, 2),
(13, 'core_admin_main_settings', 'core', 'Settings', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_settings', 1, 0, 3),
(14, 'core_admin_main_plugins', 'core', 'Plugins', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_plugins', 1, 0, 4),
(15, 'core_admin_main_layout', 'core', 'Layout', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_layout', 1, 0, 5),
(16, 'core_admin_main_ads', 'core', 'Ads', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_ads', 1, 0, 6),
(17, 'core_admin_main_stats', 'core', 'Stats', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_stats', 1, 0, 8),
(18, 'core_admin_main_manage_levels', 'core', 'Member Levels', '', '{"route":"admin_default","module":"authorization","controller":"level"}', 'core_admin_main_manage', '', 1, 0, 2),
(19, 'core_admin_main_manage_networks', 'network', 'Networks', '', '{"route":"admin_default","module":"network","controller":"manage"}', 'core_admin_main_manage', '', 1, 0, 3),
(20, 'core_admin_main_manage_announcements', 'announcement', 'Announcements', '', '{"route":"admin_default","module":"announcement","controller":"manage"}', 'core_admin_main_manage', '', 1, 0, 4),
(21, 'core_admin_message_mail', 'core', 'Email All Members', '', '{"route":"admin_default","module":"core","controller":"message","action":"mail"}', 'core_admin_main_manage', '', 1, 0, 5),
(22, 'core_admin_main_manage_reports', 'core', 'Abuse Reports', '', '{"route":"admin_default","module":"core","controller":"report"}', 'core_admin_main_manage', '', 1, 0, 6),
(23, 'core_admin_main_manage_packages', 'core', 'Packages & Plugins', '', '{"route":"admin_default","module":"core","controller":"packages"}', 'core_admin_main_manage', '', 1, 0, 7),
(24, 'core_admin_main_settings_general', 'core', 'General Settings', '', '{"route":"core_admin_settings","action":"general"}', 'core_admin_main_settings', '', 1, 0, 1),
(25, 'core_admin_main_settings_locale', 'core', 'Locale Settings', '', '{"route":"core_admin_settings","action":"locale"}', 'core_admin_main_settings', '', 1, 0, 1),
(26, 'core_admin_main_settings_fields', 'fields', 'Profile Questions', '', '{"route":"admin_default","module":"user","controller":"fields"}', 'core_admin_main_settings', '', 1, 0, 2),
(27, 'core_admin_main_wibiya', 'core', 'Wibiya Integration', '', '{"route":"admin_default", "action":"wibiya", "controller":"settings", "module":"core"}', 'core_admin_main_settings', '', 1, 0, 4),
(28, 'core_admin_main_settings_spam', 'core', 'Spam & Banning Tools', '', '{"route":"core_admin_settings","action":"spam"}', 'core_admin_main_settings', '', 1, 0, 5),
(29, 'core_admin_main_settings_mailtemplates', 'core', 'Mail Templates', '', '{"route":"admin_default","controller":"mail","action":"templates"}', 'core_admin_main_settings', '', 1, 0, 6),
(30, 'core_admin_main_settings_mailsettings', 'core', 'Mail Settings', '', '{"route":"admin_default","controller":"mail","action":"settings"}', 'core_admin_main_settings', '', 1, 0, 7),
(31, 'core_admin_main_settings_performance', 'core', 'Performance & Caching', '', '{"route":"core_admin_settings","action":"performance"}', 'core_admin_main_settings', '', 1, 0, 8),
(32, 'core_admin_main_settings_password', 'core', 'Admin Password', '', '{"route":"core_admin_settings","action":"password"}', 'core_admin_main_settings', '', 1, 0, 9),
(33, 'core_admin_main_settings_tasks', 'core', 'Task Scheduler', '', '{"route":"admin_default","controller":"tasks"}', 'core_admin_main_settings', '', 1, 0, 10),
(34, 'core_admin_main_layout_content', 'core', 'Layout Editor', '', '{"route":"admin_default","controller":"content"}', 'core_admin_main_layout', '', 1, 0, 1),
(35, 'core_admin_main_layout_themes', 'core', 'Theme Editor', '', '{"route":"admin_default","controller":"themes"}', 'core_admin_main_layout', '', 1, 0, 2),
(36, 'core_admin_main_layout_files', 'core', 'File & Media Manager', '', '{"route":"admin_default","controller":"files"}', 'core_admin_main_layout', '', 1, 0, 3),
(37, 'core_admin_main_layout_language', 'core', 'Language Manager', '', '{"route":"admin_default","controller":"language"}', 'core_admin_main_layout', '', 1, 0, 4),
(38, 'core_admin_main_layout_menus', 'core', 'Menu Editor', '', '{"route":"admin_default","controller":"menus"}', 'core_admin_main_layout', '', 1, 0, 5),
(39, 'core_admin_main_ads_manage', 'core', 'Manage Ad Campaigns', '', '{"route":"admin_default","controller":"ads"}', 'core_admin_main_ads', '', 1, 0, 1),
(40, 'core_admin_main_ads_create', 'core', 'Create New Campaign', '', '{"route":"admin_default","controller":"ads","action":"create"}', 'core_admin_main_ads', '', 1, 0, 2),
(41, 'core_admin_main_ads_affiliate', 'core', 'SE Affiliate Program', '', '{"route":"admin_default","controller":"settings","action":"affiliate"}', 'core_admin_main_ads', '', 1, 0, 3),
(42, 'core_admin_main_ads_viglink', 'core', 'VigLink', '', '{"route":"admin_default","controller":"settings","action":"viglink"}', 'core_admin_main_ads', '', 1, 0, 4),
(43, 'core_admin_main_stats_statistics', 'core', 'Site-wide Statistics', '', '{"route":"admin_default","controller":"stats"}', 'core_admin_main_stats', '', 1, 0, 1),
(44, 'core_admin_main_stats_url', 'core', 'Referring URLs', '', '{"route":"admin_default","controller":"stats","action":"referrers"}', 'core_admin_main_stats', '', 1, 0, 2),
(45, 'core_admin_main_stats_resources', 'core', 'Server Information', '', '{"route":"admin_default","controller":"system"}', 'core_admin_main_stats', '', 1, 0, 3),
(46, 'core_admin_main_stats_logs', 'core', 'Log Browser', '', '{"route":"admin_default","controller":"log","action":"index"}', 'core_admin_main_stats', '', 1, 0, 3),
(47, 'core_admin_banning_general', 'core', 'Spam & Banning Tools', '', '{"route":"core_admin_settings","action":"spam"}', 'core_admin_banning', '', 1, 0, 1),
(48, 'adcampaign_admin_main_edit', 'core', 'Edit Settings', '', '{"route":"admin_default","module":"core","controller":"ads","action":"edit"}', 'adcampaign_admin_main', '', 1, 0, 1),
(49, 'adcampaign_admin_main_manageads', 'core', 'Manage Advertisements', '', '{"route":"admin_default","module":"core","controller":"ads","action":"manageads"}', 'adcampaign_admin_main', '', 1, 0, 2),
(50, 'core_admin_main_settings_activity', 'activity', 'Activity Feed Settings', '', '{"route":"admin_default","module":"activity","controller":"settings","action":"index"}', 'core_admin_main_settings', '', 1, 0, 4),
(51, 'core_admin_main_settings_notifications', 'activity', 'Default Email Notifications', '', '{"route":"admin_default","module":"activity","controller":"settings","action":"notifications"}', 'core_admin_main_settings', '', 1, 0, 11),
(52, 'authorization_admin_main_manage', 'authorization', 'View Member Levels', '', '{"route":"admin_default","module":"authorization","controller":"level"}', 'authorization_admin_main', '', 1, 0, 1),
(53, 'authorization_admin_main_level', 'authorization', 'Member Level Settings', '', '{"route":"admin_default","module":"authorization","controller":"level","action":"edit"}', 'authorization_admin_main', '', 1, 0, 3),
(54, 'authorization_admin_level_main', 'authorization', 'Level Info', '', '{"route":"admin_default","module":"authorization","controller":"level","action":"edit"}', 'authorization_admin_level', '', 1, 0, 1),
(55, 'core_main_user', 'user', 'Members', '', '{"route":"user_general","action":"browse"}', 'core_main', '', 1, 0, 3),
(56, 'core_sitemap_user', 'user', 'Members', '', '{"route":"user_general","action":"browse"}', 'core_sitemap', '', 1, 0, 2),
(57, 'user_home_updates', 'user', 'View Recent Updates', '', '{"route":"recent_activity","icon":"application/modules/User/externals/images/links/updates.png"}', 'user_home', '', 1, 0, 1),
(58, 'user_home_view', 'user', 'View My Profile', 'User_Plugin_Menus', '{"route":"user_profile_self","icon":"application/modules/User/externals/images/links/profile.png"}', 'user_home', '', 1, 0, 2),
(59, 'user_home_edit', 'user', 'Edit My Profile', 'User_Plugin_Menus', '{"route":"user_extended","module":"user","controller":"edit","action":"profile","icon":"application/modules/User/externals/images/links/edit.png"}', 'user_home', '', 1, 0, 3),
(60, 'user_home_friends', 'user', 'Browse Members', '', '{"route":"user_general","controller":"index","action":"browse","icon":"application/modules/User/externals/images/links/search.png"}', 'user_home', '', 1, 0, 4),
(61, 'user_profile_edit', 'user', 'Edit Profile', 'User_Plugin_Menus', '', 'user_profile', '', 1, 0, 1),
(62, 'user_profile_friend', 'user', 'Friends', 'User_Plugin_Menus', '', 'user_profile', '', 1, 0, 3),
(63, 'user_profile_block', 'user', 'Block', 'User_Plugin_Menus', '', 'user_profile', '', 1, 0, 4),
(64, 'user_profile_report', 'user', 'Report User', 'User_Plugin_Menus', '', 'user_profile', '', 1, 0, 5),
(65, 'user_profile_admin', 'user', 'Admin Settings', 'User_Plugin_Menus', '', 'user_profile', '', 1, 0, 9),
(66, 'user_edit_profile', 'user', 'Personal Info', '', '{"route":"user_extended","module":"user","controller":"edit","action":"profile"}', 'user_edit', '', 1, 0, 1),
(67, 'user_edit_photo', 'user', 'Edit My Photo', '', '{"route":"user_extended","module":"user","controller":"edit","action":"photo"}', 'user_edit', '', 1, 0, 2),
(68, 'user_edit_style', 'user', 'Profile Style', 'User_Plugin_Menus', '{"route":"user_extended","module":"user","controller":"edit","action":"style"}', 'user_edit', '', 1, 0, 3),
(69, 'user_settings_general', 'user', 'General', '', '{"route":"user_extended","module":"user","controller":"settings","action":"general"}', 'user_settings', '', 1, 0, 1),
(70, 'user_settings_privacy', 'user', 'Privacy', '', '{"route":"user_extended","module":"user","controller":"settings","action":"privacy"}', 'user_settings', '', 1, 0, 2),
(71, 'user_settings_notifications', 'user', 'Notifications', '', '{"route":"user_extended","module":"user","controller":"settings","action":"notifications"}', 'user_settings', '', 1, 0, 3),
(72, 'user_settings_password', 'user', 'Change Password', '', '{"route":"user_extended", "module":"user", "controller":"settings", "action":"password"}', 'user_settings', '', 1, 0, 5),
(73, 'user_settings_delete', 'user', 'Delete Account', 'User_Plugin_Menus::canDelete', '{"route":"user_extended", "module":"user", "controller":"settings", "action":"delete"}', 'user_settings', '', 1, 0, 6),
(74, 'core_admin_main_manage_members', 'user', 'Members', '', '{"route":"admin_default","module":"user","controller":"manage"}', 'core_admin_main_manage', '', 1, 0, 1),
(75, 'core_admin_main_signup', 'user', 'Signup Process', '', '{"route":"admin_default", "controller":"signup", "module":"user"}', 'core_admin_main_settings', '', 1, 0, 3),
(76, 'core_admin_main_facebook', 'user', 'Facebook Integration', '', '{"route":"admin_default", "action":"facebook", "controller":"settings", "module":"user"}', 'core_admin_main_settings', '', 1, 0, 4),
(77, 'core_admin_main_twitter', 'user', 'Twitter Integration', '', '{"route":"admin_default", "action":"twitter", "controller":"settings", "module":"user"}', 'core_admin_main_settings', '', 1, 0, 4),
(78, 'core_admin_main_janrain', 'user', 'Janrain Integration', '', '{"route":"admin_default", "action":"janrain", "controller":"settings", "module":"user"}', 'core_admin_main_settings', '', 1, 0, 4),
(79, 'core_admin_main_settings_friends', 'user', 'Friendship Settings', '', '{"route":"admin_default","module":"user","controller":"settings","action":"friends"}', 'core_admin_main_settings', '', 1, 0, 6),
(80, 'user_admin_banning_logins', 'user', 'Login History', '', '{"route":"admin_default","module":"user","controller":"logins","action":"index"}', 'core_admin_banning', '', 1, 0, 2),
(81, 'authorization_admin_level_user', 'user', 'Members', '', '{"route":"admin_default","module":"user","controller":"settings","action":"level"}', 'authorization_admin_level', '', 1, 0, 2),
(82, 'core_mini_messages', 'messages', 'Messages', 'Messages_Plugin_Menus', '', 'core_mini', '', 0, 0, 4),
(83, 'user_profile_message', 'messages', 'Send Message', 'Messages_Plugin_Menus', '', 'user_profile', '', 1, 0, 2),
(84, 'authorization_admin_level_messages', 'messages', 'Messages', '', '{"route":"admin_default","module":"messages","controller":"settings","action":"level"}', 'authorization_admin_level', '', 1, 0, 3),
(85, 'messages_main_inbox', 'messages', 'Inbox', '', '{"route":"messages_general","action":"inbox"}', 'messages_main', '', 1, 0, 1),
(86, 'messages_main_outbox', 'messages', 'Sent Messages', '', '{"route":"messages_general","action":"outbox"}', 'messages_main', '', 1, 0, 2),
(87, 'messages_main_compose', 'messages', 'Compose Message', '', '{"route":"messages_general","action":"compose"}', 'messages_main', '', 1, 0, 3),
(88, 'user_settings_network', 'network', 'Networks', '', '{"route":"user_extended", "module":"user", "controller":"settings", "action":"network"}', 'user_settings', '', 1, 0, 3),
(89, 'core_main_invite', 'invite', 'Invite', 'Invite_Plugin_Menus::canInvite', '{"route":"default","module":"invite"}', 'core_main', '', 1, 0, 2),
(90, 'user_home_invite', 'invite', 'Invite Your Friends', 'Invite_Plugin_Menus::canInvite', '{"route":"default","module":"invite","icon":"application/modules/Invite/externals/images/invite.png"}', 'user_home', '', 1, 0, 5),
(91, 'core_admin_main_settings_storage', 'core', 'Storage System', '', '{"route":"admin_default","module":"storage","controller":"services","action":"index"}', 'core_admin_main_settings', '', 1, 0, 11),
(92, 'user_settings_payment', 'user', 'Subscription', 'Payment_Plugin_Menus', '{"route":"default", "module":"payment", "controller":"settings", "action":"index"}', 'user_settings', '', 1, 0, 4),
(93, 'core_admin_main_payment', 'payment', 'Billing', '', '{"uri":"javascript:void(0);this.blur();"}', 'core_admin_main', 'core_admin_main_payment', 1, 0, 7),
(94, 'core_admin_main_payment_transactions', 'payment', 'Transactions', '', '{"route":"admin_default","module":"payment","controller":"index","action":"index"}', 'core_admin_main_payment', '', 1, 0, 1),
(95, 'core_admin_main_payment_settings', 'payment', 'Settings', '', '{"route":"admin_default","module":"payment","controller":"settings","action":"index"}', 'core_admin_main_payment', '', 1, 0, 2),
(96, 'core_admin_main_payment_gateways', 'payment', 'Gateways', '', '{"route":"admin_default","module":"payment","controller":"gateway","action":"index"}', 'core_admin_main_payment', '', 1, 0, 3),
(97, 'core_admin_main_payment_packages', 'payment', 'Plans', '', '{"route":"admin_default","module":"payment","controller":"package","action":"index"}', 'core_admin_main_payment', '', 1, 0, 4),
(98, 'core_admin_main_payment_subscriptions', 'payment', 'Subscriptions', '', '{"route":"admin_default","module":"payment","controller":"subscription","action":"index"}', 'core_admin_main_payment', '', 1, 0, 5),
(99, 'core_main_album', 'album', 'Albums', '', '{"route":"album_general","action":"browse"}', 'core_main', '', 1, 0, 4),
(100, 'core_sitemap_album', 'album', 'Albums', '', '{"route":"album_general","action":"browse"}', 'core_sitemap', '', 1, 0, 3),
(101, 'album_main_browse', 'album', 'Browse Albums', 'Album_Plugin_Menus::canViewAlbums', '{"route":"album_general","action":"browse"}', 'album_main', '', 1, 0, 1),
(102, 'album_main_manage', 'album', 'My Albums', 'Album_Plugin_Menus::canCreateAlbums', '{"route":"album_general","action":"manage"}', 'album_main', '', 1, 0, 2),
(103, 'album_main_upload', 'album', 'Add New Photos', 'Album_Plugin_Menus::canCreateAlbums', '{"route":"album_general","action":"upload"}', 'album_main', '', 1, 0, 3),
(104, 'album_quick_upload', 'album', 'Add New Photos', 'Album_Plugin_Menus::canCreateAlbums', '{"route":"album_general","action":"upload","class":"buttonlink icon_photos_new"}', 'album_quick', '', 1, 0, 1),
(105, 'core_admin_main_plugins_album', 'album', 'Photo Albums', '', '{"route":"admin_default","module":"album","controller":"manage","action":"index"}', 'core_admin_main_plugins', '', 1, 0, 999),
(106, 'album_admin_main_manage', 'album', 'View Albums', '', '{"route":"admin_default","module":"album","controller":"manage"}', 'album_admin_main', '', 1, 0, 1),
(107, 'album_admin_main_settings', 'album', 'Global Settings', '', '{"route":"admin_default","module":"album","controller":"settings"}', 'album_admin_main', '', 1, 0, 2),
(108, 'album_admin_main_level', 'album', 'Member Level Settings', '', '{"route":"admin_default","module":"album","controller":"level"}', 'album_admin_main', '', 1, 0, 3),
(109, 'album_admin_main_categories', 'album', 'Categories', '', '{"route":"admin_default","module":"album","controller":"settings", "action":"categories"}', 'album_admin_main', '', 1, 0, 4),
(110, 'authorization_admin_level_album', 'album', 'Photo Albums', '', '{"route":"admin_default","module":"album","controller":"level","action":"index"}', 'authorization_admin_level', '', 1, 0, 999),
(111, 'mobi_browse_album', 'album', 'Albums', '', '{"route":"album_general","action":"browse"}', 'mobi_browse', '', 1, 0, 2),
(112, 'core_main_blog', 'blog', 'Blogs', '', '{"route":"blog_general"}', 'core_main', '', 1, 0, 5),
(113, 'core_sitemap_blog', 'blog', 'Blogs', '', '{"route":"blog_general"}', 'core_sitemap', '', 1, 0, 4),
(114, 'blog_main_browse', 'blog', 'Browse Entries', 'Blog_Plugin_Menus::canViewBlogs', '{"route":"blog_general"}', 'blog_main', '', 1, 0, 1),
(115, 'blog_main_manage', 'blog', 'My Entries', 'Blog_Plugin_Menus::canCreateBlogs', '{"route":"blog_general","action":"manage"}', 'blog_main', '', 1, 0, 2),
(116, 'blog_main_create', 'blog', 'Write New Entry', 'Blog_Plugin_Menus::canCreateBlogs', '{"route":"blog_general","action":"create"}', 'blog_main', '', 1, 0, 3),
(117, 'blog_quick_create', 'blog', 'Write New Entry', 'Blog_Plugin_Menus::canCreateBlogs', '{"route":"blog_general","action":"create","class":"buttonlink icon_blog_new"}', 'blog_quick', '', 1, 0, 1),
(118, 'blog_quick_style', 'blog', 'Edit Blog Style', 'Blog_Plugin_Menus', '{"route":"blog_general","action":"style","class":"smoothbox buttonlink icon_blog_style"}', 'blog_quick', '', 1, 0, 2),
(119, 'blog_gutter_list', 'blog', 'View All Entries', 'Blog_Plugin_Menus', '{"route":"blog_view","class":"buttonlink icon_blog_viewall"}', 'blog_gutter', '', 1, 0, 1),
(120, 'blog_gutter_create', 'blog', 'Write New Entry', 'Blog_Plugin_Menus', '{"route":"blog_general","action":"create","class":"buttonlink icon_blog_new"}', 'blog_gutter', '', 1, 0, 2),
(121, 'blog_gutter_style', 'blog', 'Edit Blog Style', 'Blog_Plugin_Menus', '{"route":"blog_general","action":"style","class":"smoothbox buttonlink icon_blog_style"}', 'blog_gutter', '', 1, 0, 3),
(122, 'blog_gutter_edit', 'blog', 'Edit This Entry', 'Blog_Plugin_Menus', '{"route":"blog_specific","action":"edit","class":"buttonlink icon_blog_edit"}', 'blog_gutter', '', 1, 0, 4),
(123, 'blog_gutter_delete', 'blog', 'Delete This Entry', 'Blog_Plugin_Menus', '{"route":"blog_specific","action":"delete","class":"buttonlink smoothbox icon_blog_delete"}', 'blog_gutter', '', 1, 0, 5),
(124, 'blog_gutter_share', 'blog', 'Share', 'Blog_Plugin_Menus', '{"route":"default","module":"activity","controller":"index","action":"share","class":"buttonlink smoothbox icon_comments"}', 'blog_gutter', '', 1, 0, 6),
(125, 'blog_gutter_report', 'blog', 'Report', 'Blog_Plugin_Menus', '{"route":"default","module":"core","controller":"report","action":"create","class":"buttonlink smoothbox icon_report"}', 'blog_gutter', '', 1, 0, 7),
(126, 'blog_gutter_subscribe', 'blog', 'Subscribe', 'Blog_Plugin_Menus', '{"route":"default","module":"blog","controller":"subscription","action":"add","class":"buttonlink smoothbox icon_blog_subscribe"}', 'blog_gutter', '', 1, 0, 8),
(127, 'core_admin_main_plugins_blog', 'blog', 'Blogs', '', '{"route":"admin_default","module":"blog","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(128, 'blog_admin_main_manage', 'blog', 'View Blogs', '', '{"route":"admin_default","module":"blog","controller":"manage"}', 'blog_admin_main', '', 1, 0, 1),
(129, 'blog_admin_main_settings', 'blog', 'Global Settings', '', '{"route":"admin_default","module":"blog","controller":"settings"}', 'blog_admin_main', '', 1, 0, 2),
(130, 'blog_admin_main_level', 'blog', 'Member Level Settings', '', '{"route":"admin_default","module":"blog","controller":"level"}', 'blog_admin_main', '', 1, 0, 3),
(131, 'blog_admin_main_categories', 'blog', 'Categories', '', '{"route":"admin_default","module":"blog","controller":"settings", "action":"categories"}', 'blog_admin_main', '', 1, 0, 4),
(132, 'authorization_admin_level_blog', 'blog', 'Blogs', '', '{"route":"admin_default","module":"blog","controller":"level","action":"index"}', 'authorization_admin_level', '', 1, 0, 999),
(133, 'mobi_browse_blog', 'blog', 'Blogs', '', '{"route":"blog_general"}', 'mobi_browse', '', 1, 0, 3),
(134, 'core_main_chat', 'chat', 'Chat', '', '{"route":"default","module":"chat"}', 'core_main', '', 1, 0, 7),
(135, 'core_sitemap_chat', 'chat', 'Chat', '', '{"route":"default","module":"chat"}', 'core_sitemap', '', 1, 0, 5),
(136, 'core_admin_main_plugins_chat', 'chat', 'Chat', '', '{"route":"admin_default","module":"chat","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(137, 'chat_admin_main_manage', 'chat', 'Manage Chat Rooms', '', '{"route":"admin_default","module":"chat","controller":"manage"}', 'chat_admin_main', '', 1, 0, 1),
(138, 'chat_admin_main_settings', 'chat', 'Global Settings', '', '{"route":"admin_default","module":"chat","controller":"settings"}', 'chat_admin_main', '', 1, 0, 2),
(139, 'chat_admin_main_level', 'chat', 'Member Level Settings', '', '{"route":"admin_default","module":"chat","controller":"settings","action":"level"}', 'chat_admin_main', '', 1, 0, 3),
(140, 'authorization_admin_level_chat', 'chat', 'Chat', '', '{"route":"admin_default","module":"chat","controller":"settings","action":"level"}', 'authorization_admin_level', '', 1, 0, 999),
(141, 'core_main_classified', 'classified', 'Classifieds', '', '{"route":"classified_general"}', 'core_main', '', 1, 0, 6),
(142, 'core_sitemap_classified', 'classified', 'Classifieds', '', '{"route":"classified_general"}', 'core_sitemap', '', 1, 0, 4),
(143, 'classified_main_browse', 'classified', 'Browse Listings', 'Classified_Plugin_Menus::canViewClassifieds', '{"route":"classified_general"}', 'classified_main', '', 1, 0, 1),
(144, 'classified_main_manage', 'classified', 'My Listings', 'Classified_Plugin_Menus::canCreateClassifieds', '{"route":"classified_general","action":"manage"}', 'classified_main', '', 1, 0, 2),
(145, 'classified_main_create', 'classified', 'Post a New Listing', 'Classified_Plugin_Menus::canCreateClassifieds', '{"route":"classified_general","action":"create"}', 'classified_main', '', 1, 0, 3),
(146, 'classified_quick_create', 'classified', 'Post a New Listing', 'Classified_Plugin_Menus::canCreateClassifieds', '{"route":"classified_general","action":"create","class":"buttonlink icon_classified_new"}', 'classified_quick', '', 1, 0, 1),
(147, 'core_admin_main_plugins_classified', 'classified', 'Classifieds', '', '{"route":"admin_default","module":"classified","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(148, 'classified_admin_main_manage', 'classified', 'View Classifieds', '', '{"route":"admin_default","module":"classified","controller":"manage"}', 'classified_admin_main', '', 1, 0, 1),
(149, 'classified_admin_main_settings', 'classified', 'Global Settings', '', '{"route":"admin_default","module":"classified","controller":"settings"}', 'classified_admin_main', '', 1, 0, 2),
(150, 'classified_admin_main_level', 'classified', 'Member Level Settings', '', '{"route":"admin_default","module":"classified","controller":"level"}', 'classified_admin_main', '', 1, 0, 3),
(151, 'classified_admin_main_fields', 'classified', 'Classified Questions', '', '{"route":"admin_default","module":"classified","controller":"fields"}', 'classified_admin_main', '', 1, 0, 4),
(152, 'classified_admin_main_categories', 'classified', 'Categories', '', '{"route":"admin_default","module":"classified","controller":"settings","action":"categories"}', 'classified_admin_main', '', 1, 0, 5),
(153, 'authorization_admin_level_classified', 'classified', 'Classifieds', '', '{"route":"admin_default","module":"classified","controller":"level","action":"index"}', 'authorization_admin_level', '', 1, 0, 999),
(154, 'mobi_browse_classified', 'classified', 'Classifieds', '', '{"route":"classified_general"}', 'mobi_browse', '', 1, 0, 4),
(155, 'core_main_event', 'event', 'Events', '', '{"route":"event_general"}', 'core_main', '', 1, 0, 11),
(156, 'core_sitemap_event', 'event', 'Events', '', '{"route":"event_general"}', 'core_sitemap', '', 1, 0, 6),
(157, 'event_main_upcoming', 'event', 'Upcoming Events', '', '{"route":"event_upcoming"}', 'event_main', '', 1, 0, 1),
(158, 'event_main_past', 'event', 'Past Events', '', '{"route":"event_past"}', 'event_main', '', 1, 0, 2),
(159, 'event_main_manage', 'event', 'My Events', 'Event_Plugin_Menus', '{"route":"event_general","action":"manage"}', 'event_main', '', 1, 0, 3),
(160, 'event_main_create', 'event', 'Create New Event', 'Event_Plugin_Menus', '{"route":"event_general","action":"create"}', 'event_main', '', 1, 0, 4),
(161, 'event_quick_create', 'event', 'Create New Event', 'Event_Plugin_Menus::canCreateEvents', '{"route":"event_general","action":"create","class":"buttonlink icon_event_new"}', 'event_quick', '', 1, 0, 1),
(162, 'event_profile_edit', 'event', 'Edit Profile', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 1),
(163, 'event_profile_style', 'event', 'Edit Styles', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 2),
(164, 'event_profile_member', 'event', 'Member', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 3),
(165, 'event_profile_report', 'event', 'Report Event', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 4),
(166, 'event_profile_share', 'event', 'Share', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 5),
(167, 'event_profile_invite', 'event', 'Invite', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 6),
(168, 'event_profile_message', 'event', 'Message Members', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 7),
(169, 'event_profile_delete', 'event', 'Delete Event', 'Event_Plugin_Menus', '', 'event_profile', '', 1, 0, 8),
(170, 'core_admin_main_plugins_event', 'event', 'Events', '', '{"route":"admin_default","module":"event","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(171, 'event_admin_main_manage', 'event', 'Manage Events', '', '{"route":"admin_default","module":"event","controller":"manage"}', 'event_admin_main', '', 1, 0, 1),
(172, 'event_admin_main_settings', 'event', 'Global Settings', '', '{"route":"admin_default","module":"event","controller":"settings"}', 'event_admin_main', '', 1, 0, 2),
(173, 'event_admin_main_level', 'event', 'Member Level Settings', '', '{"route":"admin_default","module":"event","controller":"settings","action":"level"}', 'event_admin_main', '', 1, 0, 3),
(174, 'event_admin_main_categories', 'event', 'Categories', '', '{"route":"admin_default","module":"event","controller":"settings","action":"categories"}', 'event_admin_main', '', 1, 0, 4),
(175, 'authorization_admin_level_event', 'event', 'Events', '', '{"route":"admin_default","module":"event","controller":"level","action":"index"}', 'authorization_admin_level', '', 1, 0, 999),
(176, 'mobi_browse_event', 'event', 'Events', '', '{"route":"event_general"}', 'mobi_browse', '', 1, 0, 7),
(177, 'core_main_forum', 'forum', 'Forum', '', '{"route":"forum_general"}', 'core_main', '', 1, 0, 8),
(178, 'core_sitemap_forum', 'forum', 'Forum', '', '{"route":"forum_general"}', 'core_sitemap', '', 1, 0, 5),
(179, 'core_admin_main_plugins_forum', 'forum', 'Forums', '', '{"route":"admin_default","module":"forum","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(180, 'forum_admin_main_manage', 'forum', 'Manage Forums', '', '{"route":"admin_default","module":"forum","controller":"manage"}', 'forum_admin_main', '', 1, 0, 1),
(181, 'forum_admin_main_settings', 'forum', 'Global Settings', '', '{"route":"admin_default","module":"forum","controller":"settings"}', 'forum_admin_main', '', 1, 0, 2),
(182, 'forum_admin_main_level', 'forum', 'Member Level Settings', '', '{"route":"admin_default","module":"forum","controller":"level"}', 'forum_admin_main', '', 1, 0, 3),
(183, 'authorization_admin_level_forum', 'forum', 'Forums', '', '{"route":"admin_default","module":"forum","controller":"level","action":"index"}', 'authorization_admin_level', '', 1, 0, 999),
(184, 'mobi_browse_forum', 'forum', 'Forum', '', '{"route":"forum_general"}', 'mobi_browse', '', 1, 0, 5),
(185, 'core_main_group', 'group', 'Groups', '', '{"route":"group_general"}', 'core_main', '', 1, 0, 9),
(186, 'core_sitemap_group', 'group', 'Groups', '', '{"route":"group_general"}', 'core_sitemap', '', 1, 0, 6),
(187, 'group_main_browse', 'group', 'Browse Groups', '', '{"route":"group_general","action":"browse"}', 'group_main', '', 1, 0, 1),
(188, 'group_main_manage', 'group', 'My Groups', 'Group_Plugin_Menus', '{"route":"group_general","action":"manage"}', 'group_main', '', 1, 0, 2),
(189, 'group_main_create', 'group', 'Create New Group', 'Group_Plugin_Menus', '{"route":"group_general","action":"create"}', 'group_main', '', 1, 0, 3),
(190, 'group_quick_create', 'group', 'Create New Group', 'Group_Plugin_Menus::canCreateGroups', '{"route":"group_general","action":"create","class":"buttonlink icon_group_new"}', 'group_quick', '', 1, 0, 1),
(191, 'group_profile_edit', 'group', 'Edit Profile', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 1),
(192, 'group_profile_style', 'group', 'Edit Styles', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 2),
(193, 'group_profile_member', 'group', 'Member', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 3),
(194, 'group_profile_report', 'group', 'Report Group', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 4),
(195, 'group_profile_share', 'group', 'Share', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 5),
(196, 'group_profile_invite', 'group', 'Invite', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 6),
(197, 'group_profile_message', 'group', 'Message Members', 'Group_Plugin_Menus', '', 'group_profile', '', 1, 0, 7),
(198, 'core_admin_main_plugins_group', 'group', 'Groups', '', '{"route":"admin_default","module":"group","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(199, 'group_admin_main_manage', 'group', 'Manage Groups', '', '{"route":"admin_default","module":"group","controller":"manage"}', 'group_admin_main', '', 1, 0, 1),
(200, 'group_admin_main_settings', 'group', 'Global Settings', '', '{"route":"admin_default","module":"group","controller":"settings"}', 'group_admin_main', '', 1, 0, 2),
(201, 'group_admin_main_level', 'group', 'Member Level Settings', '', '{"route":"admin_default","module":"group","controller":"settings","action":"level"}', 'group_admin_main', '', 1, 0, 3),
(202, 'group_admin_main_categories', 'group', 'Categories', '', '{"route":"admin_default","module":"group","controller":"settings","action":"categories"}', 'group_admin_main', '', 1, 0, 4),
(203, 'authorization_admin_level_group', 'group', 'Groups', '', '{"route":"admin_default","module":"group","controller":"settings","action":"level"}', 'authorization_admin_level', '', 1, 0, 999),
(204, 'mobi_browse_group', 'group', 'Groups', '', '{"route":"group_general"}', 'mobi_browse', '', 1, 0, 8),
(205, 'core_footer_mobile', 'mobi', 'Mobile Site', 'Mobi_Plugin_Menus', '', 'core_footer', '', 1, 0, 4),
(206, 'mobi_footer_mobile', 'mobi', 'Mobile Site', 'Mobi_Plugin_Menus', '', 'mobi_footer', '', 1, 0, 1),
(207, 'mobi_footer_auth', 'mobi', 'Auth', 'Mobi_Plugin_Menus', '', 'mobi_footer', '', 1, 0, 2),
(208, 'mobi_footer_signup', 'mobi', 'Sign Up', 'Mobi_Plugin_Menus', '', 'mobi_footer', '', 1, 0, 3),
(209, 'mobi_main_home', 'mobi', 'Home', 'Mobi_Plugin_Menus', '', 'mobi_main', '', 1, 0, 1),
(210, 'mobi_main_profile', 'mobi', 'Profile', 'Mobi_Plugin_Menus', '', 'mobi_main', '', 1, 0, 2),
(211, 'mobi_main_messages', 'mobi', 'Inbox', 'Mobi_Plugin_Menus', '', 'mobi_main', '', 1, 0, 3),
(212, 'mobi_main_browse', 'mobi', 'Browse', 'Mobi_Plugin_Menus', '', 'mobi_main', '', 1, 0, 4),
(213, 'mobi_profile_message', 'mobi', 'Send Message', 'Mobi_Plugin_Menus', '', 'mobi_profile', '', 1, 0, 1),
(214, 'mobi_profile_friend', 'mobi', 'Friends', 'Mobi_Plugin_Menus', '', 'mobi_profile', '', 1, 0, 2),
(215, 'mobi_profile_edit', 'mobi', 'Edit Profile', 'Mobi_Plugin_Menus', '', 'mobi_profile', '', 1, 0, 3),
(216, 'mobi_profile_report', 'mobi', 'Report User', 'Mobi_Plugin_Menus', '', 'mobi_profile', '', 1, 0, 4),
(217, 'mobi_profile_block', 'mobi', 'Block', 'Mobi_Plugin_Menus', '', 'mobi_profile', '', 1, 0, 5),
(218, 'mobi_profile_admin', 'mobi', 'Admin', 'Mobi_Plugin_Menus', '', 'mobi_profile', '', 1, 0, 6),
(219, 'mobi_browse_members', 'user', 'Members', '', '{"route":"user_general","action":"browse"}', 'mobi_browse', '', 1, 0, 1),
(220, 'core_main_music', 'music', 'Music', '', '{"route":"music_general","action":"browse"}', 'core_main', '', 1, 0, 22),
(221, 'core_sitemap_music', 'music', 'Music', '', '{"route":"music_general","action":"browse"}', 'core_sitemap', '', 1, 0, 100),
(222, 'music_main_browse', 'music', 'Browse Music', 'Music_Plugin_Menus::canViewPlaylists', '{"route":"music_general","action":"browse"}', 'music_main', '', 1, 0, 1),
(223, 'music_main_manage', 'music', 'My Music', 'Music_Plugin_Menus::canCreatePlaylists', '{"route":"music_general","action":"manage"}', 'music_main', '', 1, 0, 2),
(224, 'music_main_create', 'music', 'Upload Music', 'Music_Plugin_Menus::canCreatePlaylists', '{"route":"music_general","action":"create"}', 'music_main', '', 1, 0, 3),
(225, 'music_quick_create', 'music', 'Upload Music', 'Music_Plugin_Menus::canCreatePlaylists', '{"route":"music_general","action":"create","class":"buttonlink icon_music_new"}', 'music_quick', '', 1, 0, 1),
(226, 'core_admin_main_plugins_music', 'music', 'Music', '', '{"route":"admin_default","module":"music","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(227, 'music_admin_main_manage', 'music', 'Manage Music', '', '{"route":"admin_default","module":"music","controller":"manage"}', 'music_admin_main', '', 1, 0, 1),
(228, 'music_admin_main_settings', 'music', 'Global Settings', '', '{"route":"admin_default","module":"music","controller":"settings"}', 'music_admin_main', '', 1, 0, 2),
(229, 'music_admin_main_level', 'music', 'Member Level Settings', '', '{"route":"admin_default","module":"music","controller":"level"}', 'music_admin_main', '', 1, 0, 3),
(230, 'authorization_admin_level_music', 'music', 'Music', '', '{"route":"admin_default","module":"music","controller":"level","action":"index"}', 'authorization_admin_level', '', 1, 0, 999),
(231, 'mobi_browse_music', 'music', 'Music', '', '{"route":"music_general","action":"browse"}', 'mobi_browse', '', 1, 0, 10),
(232, 'core_main_poll', 'poll', 'Polls', '', '{"route":"poll_general","action":"browse"}', 'core_main', '', 1, 0, 10),
(233, 'core_sitemap_poll', 'poll', 'Polls', '', '{"route":"poll_general","action":"browse"}', 'core_sitemap', '', 1, 0, 5),
(234, 'poll_main_browse', 'poll', 'Browse Polls', 'Poll_Plugin_Menus::canViewPolls', '{"route":"poll_general","action":"browse"}', 'poll_main', '', 1, 0, 1),
(235, 'poll_main_manage', 'poll', 'My Polls', 'Poll_Plugin_Menus::canCreatePolls', '{"route":"poll_general","action":"manage"}', 'poll_main', '', 1, 0, 2),
(236, 'poll_main_create', 'poll', 'Create New Poll', 'Poll_Plugin_Menus::canCreatePolls', '{"route":"poll_general","action":"create"}', 'poll_main', '', 1, 0, 3),
(237, 'poll_quick_create', 'poll', 'Create New Poll', 'Poll_Plugin_Menus::canCreatePolls', '{"route":"poll_general","action":"create","class":"buttonlink icon_poll_new"}', 'poll_quick', '', 1, 0, 1),
(238, 'core_admin_main_plugins_poll', 'poll', 'Polls', '', '{"route":"admin_default","module":"poll","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(239, 'poll_admin_main_manage', 'poll', 'Manage Polls', '', '{"route":"admin_default","module":"poll","controller":"manage"}', 'poll_admin_main', '', 1, 0, 1),
(240, 'poll_admin_main_settings', 'poll', 'Global Settings', '', '{"route":"admin_default","module":"poll","controller":"settings"}', 'poll_admin_main', '', 1, 0, 2),
(241, 'poll_admin_main_level', 'poll', 'Member Level Settings', '', '{"route":"admin_default","module":"poll","controller":"settings","action":"level"}', 'poll_admin_main', '', 1, 0, 3),
(242, 'authorization_admin_level_poll', 'poll', 'Polls', '', '{"route":"admin_default","module":"poll","controller":"settings","action":"level"}', 'authorization_admin_level', '', 1, 0, 999),
(243, 'mobi_browse_poll', 'poll', 'Polls', '', '{"route":"poll_general","action":"browse"}', 'mobi_browse', '', 1, 0, 6),
(244, 'core_main_video', 'video', 'Videos', '', '{"route":"video_general"}', 'core_main', '', 1, 0, 12),
(245, 'core_sitemap_video', 'video', 'Videos', '', '{"route":"video_general"}', 'core_sitemap', '', 1, 0, 7),
(246, 'core_admin_main_plugins_video', 'video', 'Videos', '', '{"route":"admin_default","module":"video","controller":"manage"}', 'core_admin_main_plugins', '', 1, 0, 999),
(247, 'video_main_browse', 'video', 'Browse Videos', '', '{"route":"video_general"}', 'video_main', '', 1, 0, 1),
(248, 'video_main_manage', 'video', 'My Videos', 'Video_Plugin_Menus', '{"route":"video_general","action":"manage"}', 'video_main', '', 1, 0, 2),
(249, 'video_main_create', 'video', 'Post New Video', 'Video_Plugin_Menus', '{"route":"video_general","action":"create"}', 'video_main', '', 1, 0, 3),
(250, 'video_quick_create', 'video', 'Post New Video', 'Video_Plugin_Menus::canCreateVideos', '{"route":"video_general","action":"create","class":"buttonlink icon_video_new"}', 'video_quick', '', 1, 0, 1),
(251, 'video_admin_main_manage', 'video', 'Manage Videos', '', '{"route":"admin_default","module":"video","controller":"manage"}', 'video_admin_main', '', 1, 0, 1),
(252, 'video_admin_main_utility', 'video', 'Video Utilities', '', '{"route":"admin_default","module":"video","controller":"settings","action":"utility"}', 'video_admin_main', '', 1, 0, 2),
(253, 'video_admin_main_settings', 'video', 'Global Settings', '', '{"route":"admin_default","module":"video","controller":"settings"}', 'video_admin_main', '', 1, 0, 3),
(254, 'video_admin_main_level', 'video', 'Member Level Settings', '', '{"route":"admin_default","module":"video","controller":"settings","action":"level"}', 'video_admin_main', '', 1, 0, 4),
(255, 'video_admin_main_categories', 'video', 'Categories', '', '{"route":"admin_default","module":"video","controller":"settings","action":"categories"}', 'video_admin_main', '', 1, 0, 5),
(256, 'authorization_admin_level_video', 'video', 'Videos', '', '{"route":"admin_default","module":"video","controller":"settings","action":"level"}', 'authorization_admin_level', '', 1, 0, 999),
(257, 'mobi_browse_video', 'video', 'Videos', '', '{"route":"video_general"}', 'mobi_browse', '', 1, 0, 9),
(269, 'custom_269', 'core', 'WORLD', '', '{"uri":"pages\\/world","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 1),
(270, 'custom_270', 'core', 'U.S.', '', '{"uri":"pages\\/us","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 2),
(271, 'custom_271', 'core', 'GENERAL', '', '{"uri":"pages\\/general","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 3),
(272, 'custom_272', 'core', 'BUSINESS', '', '{"uri":"pages\\/business","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 4),
(273, 'custom_273', 'core', 'TECHNOLOGY', '', '{"uri":"pages\\/technology","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 5),
(274, 'custom_274', 'core', 'POLITICS', '', '{"uri":"pages\\/politics","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 6),
(275, 'custom_275', 'core', 'LIFESTYLE', '', '{"uri":"pages\\/lifestyle","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 7),
(276, 'custom_276', 'core', 'SPORTS', '', '{"uri":"pages\\/sports","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 8),
(277, 'custom_277', 'core', 'PODCAST', '', '{"uri":"pages\\/podcast","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 9),
(278, 'custom_278', 'core', 'ENTERTAINMENT', '', '{"uri":"pages\\/entertainment","icon":"","target":"","enabled":"1"}', 'custom_32', '', 1, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_menus`
--

CREATE TABLE IF NOT EXISTS `engine4_core_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` enum('standard','hidden','custom') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'standard',
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `order` smallint(3) NOT NULL DEFAULT '999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `order` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `engine4_core_menus`
--

INSERT INTO `engine4_core_menus` (`id`, `name`, `type`, `title`, `order`) VALUES
(1, 'core_main', 'standard', 'Main Navigation Menu', 1),
(2, 'core_mini', 'standard', 'Mini Navigation Menu', 2),
(3, 'core_footer', 'standard', 'Footer Menu', 3),
(4, 'core_sitemap', 'standard', 'Sitemap', 4),
(5, 'user_home', 'standard', 'Member Home Quick Links Menu', 999),
(6, 'user_profile', 'standard', 'Member Profile Options Menu', 999),
(7, 'user_edit', 'standard', 'Member Edit Profile Navigation Menu', 999),
(8, 'user_browse', 'standard', 'Member Browse Navigation Menu', 999),
(9, 'user_settings', 'standard', 'Member Settings Navigation Menu', 999),
(10, 'messages_main', 'standard', 'Messages Main Navigation Menu', 999),
(11, 'album_main', 'standard', 'Album Main Navigation Menu', 999),
(12, 'album_quick', 'standard', 'Album Quick Navigation Menu', 999),
(13, 'blog_main', 'standard', 'Blog Main Navigation Menu', 999),
(14, 'blog_quick', 'standard', 'Blog Quick Navigation Menu', 999),
(15, 'blog_gutter', 'standard', 'Blog Gutter Navigation Menu', 999),
(16, 'classified_main', 'standard', 'Classified Main Navigation Menu', 999),
(17, 'classified_quick', 'standard', 'Classified Quick Navigation Menu', 999),
(18, 'event_main', 'standard', 'Event Main Navigation Menu', 999),
(19, 'event_profile', 'standard', 'Event Profile Options Menu', 999),
(20, 'group_main', 'standard', 'Group Main Navigation Menu', 999),
(21, 'group_profile', 'standard', 'Group Profile Options Menu', 999),
(22, 'mobi_footer', 'standard', 'Mobile Footer Menu', 999),
(23, 'mobi_main', 'standard', 'Mobile Main Menu', 999),
(24, 'mobi_profile', 'standard', 'Mobile Profile Options Menu', 999),
(25, 'mobi_browse', 'standard', 'Mobile Browse Page Menu', 999),
(26, 'music_main', 'standard', 'Music Main Navigation Menu', 999),
(27, 'music_quick', 'standard', 'Music Quick Navigation Menu', 999),
(28, 'poll_main', 'standard', 'Poll Main Navigation Menu', 999),
(29, 'poll_quick', 'standard', 'Poll Quick Navigation Menu', 999),
(30, 'video_main', 'standard', 'Video Main Navigation Menu', 999),
(31, 'custom_31', 'custom', 'hello', 999),
(32, 'custom_32', 'custom', 'my menu', 999);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_migrations`
--

CREATE TABLE IF NOT EXISTS `engine4_core_migrations` (
  `package` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `current` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_modules`
--

CREATE TABLE IF NOT EXISTS `engine4_core_modules` (
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `version` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('core','standard','extra') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'extra',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_core_modules`
--

INSERT INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES
('activity', 'Activity', 'Activity', '4.8.9', 1, 'core'),
('album', 'Albums', 'Albums', '4.8.9', 1, 'extra'),
('announcement', 'Announcements', 'Announcements', '4.8.0', 1, 'standard'),
('authorization', 'Authorization', 'Authorization', '4.7.0', 1, 'core'),
('blog', 'Blogs', 'Blogs', '4.8.7', 1, 'extra'),
('chat', 'Chat', 'Chat', '4.8.8', 1, 'extra'),
('classified', 'Classifieds', 'Classifieds', '4.8.9', 1, 'extra'),
('core', 'Core', 'Core', '4.8.9', 1, 'core'),
('event', 'Events', 'Events', '4.8.9', 1, 'extra'),
('fields', 'Fields', 'Fields', '4.8.8', 1, 'core'),
('forum', 'Forum', 'Forum', '4.8.7', 1, 'extra'),
('group', 'Groups', 'Groups', '4.8.9', 1, 'extra'),
('invite', 'Invite', 'Invite', '4.8.7', 1, 'standard'),
('messages', 'Messages', 'Messages', '4.8.7', 1, 'standard'),
('mobi', 'Mobi', 'Mobile', '4.8.9', 1, 'extra'),
('music', 'Music', 'Music', '4.8.9', 1, 'extra'),
('network', 'Networks', 'Networks', '4.8.6', 1, 'standard'),
('payment', 'Payment', 'Payment', '4.8.9', 1, 'standard'),
('poll', 'Polls', 'Polls', '4.8.0', 1, 'extra'),
('storage', 'Storage', 'Storage', '4.8.9', 1, 'core'),
('user', 'Members', 'Members', '4.8.9', 1, 'core'),
('video', 'Videos', 'Videos', '4.8.9', 1, 'extra');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_nodes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_nodes` (
  `node_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `signature` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varbinary(16) NOT NULL,
  `first_seen` datetime NOT NULL,
  `last_seen` datetime NOT NULL,
  PRIMARY KEY (`node_id`),
  UNIQUE KEY `signature` (`signature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_pages`
--

CREATE TABLE IF NOT EXISTS `engine4_core_pages` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `displayname` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `custom` tinyint(1) NOT NULL DEFAULT '1',
  `fragment` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `levels` text COLLATE utf8_unicode_ci,
  `provides` text COLLATE utf8_unicode_ci,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=84 ;

--
-- Dumping data for table `engine4_core_pages`
--

INSERT INTO `engine4_core_pages` (`page_id`, `name`, `displayname`, `url`, `title`, `description`, `keywords`, `custom`, `fragment`, `layout`, `levels`, `provides`, `view_count`, `search`) VALUES
(1, 'header', 'Site Header', NULL, '', '', '', 0, 1, '', NULL, 'header-footer', 0, 0),
(2, 'footer', 'Site Footer', NULL, '', '', '', 0, 1, '', NULL, 'header-footer', 0, 0),
(3, 'core_index_index', 'Landing Page', NULL, 'Landing Page', 'This is your site''s landing page.', '', 0, 0, '', NULL, 'no-viewer;no-subject', 0, 0),
(4, 'user_index_home', 'Member Home Page', NULL, 'Member Home Page', 'This is the home page for members.', '', 0, 0, '', NULL, 'viewer;no-subject', 0, 0),
(5, 'user_profile_index', 'Member Profile', NULL, 'Member Profile', 'This is a member''s profile.', '', 0, 0, '', NULL, 'subject=user', 0, 0),
(6, 'core_help_contact', 'Contact Page', NULL, 'Contact Us', 'This is the contact page', '', 0, 0, '', NULL, 'no-viewer;no-subject', 0, 0),
(7, 'core_help_privacy', 'Privacy Page', NULL, 'Privacy Policy', 'This is the privacy policy page', '', 0, 0, '', NULL, 'no-viewer;no-subject', 0, 0),
(8, 'core_help_terms', 'Terms of Service Page', NULL, 'Terms of Service', 'This is the terms of service page', '', 0, 0, '', NULL, 'no-viewer;no-subject', 0, 0),
(9, 'core_error_requireuser', 'Sign-in Required Page', NULL, 'Sign-in Required', '', '', 0, 0, '', NULL, NULL, 0, 0),
(10, 'core_search_index', 'Search Page', NULL, 'Searc', '', '', 0, 0, '', NULL, NULL, 0, 0),
(11, 'user_auth_login', 'Sign-in Page', NULL, 'Sign-in', 'This is the site sign-in page.', '', 0, 0, '', NULL, NULL, 0, 0),
(12, 'user_signup_index', 'Sign-up Page', NULL, 'Sign-up', 'This is the site sign-up page.', '', 0, 0, '', NULL, NULL, 0, 0),
(13, 'user_auth_forgot', 'Forgot Password Page', NULL, 'Forgot Password', 'This is the site forgot password page.', '', 0, 0, '', NULL, NULL, 0, 0),
(14, 'user_settings_general', 'User General Settings Page', NULL, 'General', 'This page is the user general settings page.', '', 0, 0, '', NULL, NULL, 0, 0),
(15, 'user_settings_privacy', 'User Privacy Settings Page', NULL, 'Privacy', 'This page is the user privacy settings page.', '', 0, 0, '', NULL, NULL, 0, 0),
(16, 'user_settings_network', 'User Networks Settings Page', NULL, 'Networks', 'This page is the user networks settings page.', '', 0, 0, '', NULL, NULL, 0, 0),
(17, 'user_settings_notifications', 'User Notifications Settings Page', NULL, 'Notifications', 'This page is the user notification settings page.', '', 0, 0, '', NULL, NULL, 0, 0),
(18, 'user_settings_password', 'User Change Password Settings Page', NULL, 'Change Password', 'This page is the change password page.', '', 0, 0, '', NULL, NULL, 0, 0),
(19, 'user_settings_delete', 'User Delete Account Settings Page', NULL, 'Delete Account', 'This page is the delete accout page.', '', 0, 0, '', NULL, NULL, 0, 0),
(20, 'user_index_browse', 'Member Browse Page', NULL, 'Member Browse', 'This page show member lists.', '', 0, 0, '', NULL, NULL, 0, 0),
(21, 'invite_index_index', 'Invite Page', NULL, 'Invite', '', '', 0, 0, '', NULL, NULL, 0, 0),
(22, 'messages_messages_compose', 'Messages Compose Page', NULL, 'Compose', '', '', 0, 0, '', NULL, NULL, 0, 0),
(23, 'messages_messages_inbox', 'Messages Inbox Page', NULL, 'Inbox', '', '', 0, 0, '', NULL, NULL, 0, 0),
(24, 'messages_messages_outbox', 'Messages Outbox Page', NULL, 'Inbox', '', '', 0, 0, '', NULL, NULL, 0, 0),
(25, 'messages_messages_search', 'Messages Search Page', NULL, 'Search', '', '', 0, 0, '', NULL, NULL, 0, 0),
(26, 'messages_messages_view', 'Messages View Page', NULL, 'My Message', '', '', 0, 0, '', NULL, NULL, 0, 0),
(27, 'album_photo_view', 'Album Photo View Page', NULL, 'Album Photo View', 'This page displays an album''s photo.', '', 0, 0, '', NULL, 'subject=album_photo', 0, 0),
(28, 'album_album_view', 'Album View Page', NULL, 'Album View', 'This page displays an album''s photos.', '', 0, 0, '', NULL, 'subject=album', 0, 0),
(29, 'album_index_browse', 'Album Browse Page', NULL, 'Album Browse', 'This page lists album entries.', '', 0, 0, '', NULL, NULL, 0, 0),
(30, 'album_index_upload', 'Album Create Page', NULL, 'Add New Photos', 'This page is the album create page.', '', 0, 0, '', NULL, NULL, 0, 0),
(31, 'album_index_manage', 'Album Manage Page', NULL, 'My Albums', 'This page lists album a user''s albums.', '', 0, 0, '', NULL, NULL, 0, 0),
(32, 'blog_index_list', 'Blog List Page', NULL, 'Blog List', 'This page lists a member''s blog entries.', '', 0, 0, '', NULL, 'subject=user', 0, 0),
(33, 'blog_index_view', 'Blog View Page', NULL, 'Blog View', 'This page displays a blog entry.', '', 0, 0, '', NULL, 'subject=blog', 0, 0),
(34, 'blog_index_index', 'Blog Browse Page', NULL, 'Blog Browse', 'This page lists blog entries.', '', 0, 0, '', NULL, NULL, 0, 0),
(35, 'blog_index_create', 'Blog Create Page', NULL, 'Write New Entry', 'This page is the blog create page.', '', 0, 0, '', NULL, NULL, 0, 0),
(36, 'blog_index_manage', 'Blog Manage Page', NULL, 'My Entries', 'This page lists a user''s blog entries.', '', 0, 0, '', NULL, NULL, 0, 0),
(37, 'chat_index_index', 'Chat Main Page', NULL, 'Chat', 'This is the chat room.', '', 0, 0, '', NULL, NULL, 0, 0),
(38, 'classified_index_index', 'Classified Browse Page', NULL, 'Classified Browse', 'This page lists classifieds.', '', 0, 0, '', NULL, NULL, 0, 0),
(39, 'classified_index_view', 'Classified View Page', NULL, 'View Classified', 'This is the view page for a classified.', '', 0, 0, '', NULL, 'subject=classified', 0, 0),
(40, 'classified_index_create', 'Classified Create Page', NULL, 'Post a New Listing', 'This page is the classified create page.', '', 0, 0, '', NULL, NULL, 0, 0),
(41, 'classified_index_manage', 'Classified Manage Page', NULL, 'My Listings', 'This page lists a user''s classifieds.', '', 0, 0, '', NULL, NULL, 0, 0),
(42, 'mobi_event_profile', 'Mobile Event Profile', NULL, 'Mobile Event Profile', 'This is the mobile verison of an event profile.', '', 0, 0, '', NULL, NULL, 0, 0),
(43, 'event_profile_index', 'Event Profile', NULL, 'Event Profile', 'This is the profile for an event.', '', 0, 0, '', NULL, 'subject=event', 0, 0),
(44, 'event_index_browse', 'Event Browse Page', NULL, 'Event Browse', 'This page lists events.', '', 0, 0, '', NULL, NULL, 0, 0),
(45, 'event_index_create', 'Event Create Page', NULL, 'Event Create', 'This page allows users to create events.', '', 0, 0, '', NULL, NULL, 0, 0),
(46, 'event_index_manage', 'Event Manage Page', NULL, 'My Events', 'This page lists a user''s events.', '', 0, 0, '', NULL, NULL, 0, 0),
(47, 'forum_index_index', 'Forum Main Page', NULL, 'Forum Main', 'This is the main forum page.', '', 0, 0, '', NULL, NULL, 0, 0),
(48, 'forum_forum_view', 'Forum View Page', NULL, 'Forum View', 'This is the view forum page.', '', 0, 0, '', NULL, NULL, 0, 0),
(49, 'forum_forum_topic-create', 'Forum Topic Create Page', NULL, 'Post Topic', 'This is the forum topic create page.', '', 0, 0, '', NULL, NULL, 0, 0),
(50, 'group_profile_index', 'Group Profile', NULL, 'Group Profile', 'This is the profile for an group.', '', 0, 0, '', NULL, 'subject=group', 0, 0),
(51, 'mobi_group_profile', 'Mobile Group Profile', NULL, 'Mobile Group Profile', 'This is the mobile verison of a group profile.', '', 0, 0, '', NULL, NULL, 0, 0),
(52, 'group_index_browse', 'Group Browse Page', NULL, 'Group Browse', 'This page lists groups.', '', 0, 0, '', NULL, NULL, 0, 0),
(53, 'group_index_create', 'Group Create Page', NULL, 'Group Create', 'This page allows users to create groups.', '', 0, 0, '', NULL, NULL, 0, 0),
(54, 'group_index_manage', 'Group Manage Page', NULL, 'My Groups', 'This page lists a user''s groups.', '', 0, 0, '', NULL, NULL, 0, 0),
(55, 'header_mobi', 'Mobile Site Header', NULL, 'Mobile Site Header', 'This is the mobile site header.', '', 0, 1, '', NULL, NULL, 0, 0),
(56, 'footer_mobi', 'Mobile Site Footer', NULL, 'Mobile Site Footer', 'This is the mobile site footer.', '', 0, 1, '', NULL, NULL, 0, 0),
(57, 'mobi_index_index', 'Mobile Home Page', NULL, 'Mobile Home Page', 'This is the mobile homepage.', '', 0, 0, 'default', NULL, NULL, 0, 0),
(58, 'mobi_index_userhome', 'Mobile Member Home Page', NULL, 'Mobile Member Home Page', 'This is the mobile member homepage.', '', 0, 0, '', NULL, NULL, 0, 0),
(59, 'mobi_index_profile', 'Mobile Member Profile', NULL, 'Mobile Member Profile', 'This is the mobile verison of a member profile.', '', 0, 0, '', NULL, NULL, 0, 0),
(60, 'music_index_browse', 'Music Browse Page', NULL, 'Music Browse', 'This page lists music.', '', 0, 0, '', NULL, NULL, 0, 0),
(61, 'music_playlist_view', 'Music Playlist View Page', NULL, 'Music View', 'This page displays a playlist.', '', 0, 0, '', NULL, NULL, 0, 0),
(62, 'music_index_create', 'Music Create Page', NULL, 'Upload Music', 'This page is the music create page.', '', 0, 0, '', NULL, NULL, 0, 0),
(63, 'music_index_manage', 'Music Manage Page', NULL, 'My Music', 'This page is the music manage page.', '', 0, 0, '', NULL, NULL, 0, 0),
(64, 'poll_index_browse', 'Poll Browse Page', NULL, 'Poll Browse', 'This page lists polls.', '', 0, 0, '', NULL, NULL, 0, 0),
(65, 'poll_poll_view', 'Poll View Page', NULL, 'View Poll', 'This is the view page for a poll.', '', 0, 0, '', NULL, 'subject=poll', 0, 0),
(66, 'poll_index_create', 'Poll Create Page', NULL, 'Create New Pll', 'This page is the poll create page.', '', 0, 0, '', NULL, NULL, 0, 0),
(67, 'poll_index_manage', 'Poll Manage Page', NULL, 'My Poll', 'This page lists a user''s polls.', '', 0, 0, '', NULL, NULL, 0, 0),
(68, 'video_index_view', 'Video View Page', NULL, 'View Video', 'This is the view page for a video.', '', 0, 0, '', NULL, 'subject=video', 0, 0),
(69, 'video_index_browse', 'Video Browse Page', NULL, 'Video Browse', 'This page lists videos.', '', 0, 0, '', NULL, NULL, 0, 0),
(70, 'video_index_create', 'Video Create Page', NULL, 'Video Create', 'This page allows video to be added.', '', 0, 0, '', NULL, NULL, 0, 0),
(71, 'video_index_manage', 'Video Manage Page', NULL, 'My Videos', 'This page lists a user''s videos.', '', 0, 0, '', NULL, NULL, 0, 0),
(72, NULL, 'U.S.', 'us', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 1),
(73, NULL, 'topstories', 'topstories', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(74, NULL, 'WORLD', 'world', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(76, NULL, 'GENERAL', 'general', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(77, NULL, 'BUSINESS', 'business', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(78, NULL, 'TECHNOLOGY', 'technology', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(79, NULL, 'POLITICS', 'politics', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(80, NULL, 'LIFESTYLE', 'lifestyle', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(81, NULL, 'SPORTS', 'sports', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(82, NULL, 'PODCAST', 'podcast', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0),
(83, NULL, 'ENTERTAINMENT', 'entertainment', '', '', '', 1, 0, '', '["1","2","3","4","5"]', 'no-subject', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_processes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_processes` (
  `pid` int(10) unsigned NOT NULL,
  `parent_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `system_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `started` int(10) unsigned NOT NULL,
  `timeout` mediumint(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_referrers`
--

CREATE TABLE IF NOT EXISTS `engine4_core_referrers` (
  `host` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `query` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) unsigned NOT NULL,
  PRIMARY KEY (`host`,`path`,`query`),
  KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_reports`
--

CREATE TABLE IF NOT EXISTS `engine4_core_reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `subject_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `subject_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`report_id`),
  KEY `category` (`category`),
  KEY `user_id` (`user_id`),
  KEY `read` (`read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_routes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_routes` (
  `name` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`),
  KEY `order` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_search`
--

CREATE TABLE IF NOT EXISTS `engine4_core_search` (
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id` int(11) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hidden` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`type`,`id`),
  FULLTEXT KEY `LOOKUP` (`title`,`description`,`keywords`,`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_core_search`
--

INSERT INTO `engine4_core_search` (`type`, `id`, `title`, `description`, `keywords`, `hidden`) VALUES
('user', 2, 'mehul patel', '', '', ''),
('video', 1, 'Philips Presents: The Longest Night', 'Páll Pálsson has been a man of the sea for 36 years. With a life of fishing, comes a life of little sleep. Research shows that nearly half of Icelandic fishermen suffer from sleep-related disorders such as chronic fatigue and apnea. <br />  <br />  Exec', '', ''),
('user', 3, 'pushpak zala', '', '', ''),
('album', 1, 'Wall Photos', '', '', ''),
('user', 4, 'mayank patel', '', '', ''),
('user', 1, 'vijay patel', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_serviceproviders`
--

CREATE TABLE IF NOT EXISTS `engine4_core_serviceproviders` (
  `serviceprovider_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `class` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`serviceprovider_id`),
  UNIQUE KEY `type` (`type`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `engine4_core_serviceproviders`
--

INSERT INTO `engine4_core_serviceproviders` (`serviceprovider_id`, `title`, `type`, `name`, `class`, `enabled`) VALUES
(1, 'MySQL', 'database', 'mysql', 'Engine_ServiceLocator_Plugin_Database_Mysql', 1),
(2, 'PDO MySQL', 'database', 'mysql_pdo', 'Engine_ServiceLocator_Plugin_Database_MysqlPdo', 1),
(3, 'MySQLi', 'database', 'mysqli', 'Engine_ServiceLocator_Plugin_Database_Mysqli', 1),
(4, 'File', 'cache', 'file', 'Engine_ServiceLocator_Plugin_Cache_File', 1),
(5, 'APC', 'cache', 'apc', 'Engine_ServiceLocator_Plugin_Cache_Apc', 1),
(6, 'Memcache', 'cache', 'memcached', 'Engine_ServiceLocator_Plugin_Cache_Memcached', 1),
(7, 'Simple', 'captcha', 'image', 'Engine_ServiceLocator_Plugin_Captcha_Image', 1),
(8, 'ReCaptcha', 'captcha', 'recaptcha', 'Engine_ServiceLocator_Plugin_Captcha_Recaptcha', 1),
(9, 'SMTP', 'mail', 'smtp', 'Engine_ServiceLocator_Plugin_Mail_Smtp', 1),
(10, 'Sendmail', 'mail', 'sendmail', 'Engine_ServiceLocator_Plugin_Mail_Sendmail', 1),
(11, 'GD', 'image', 'gd', 'Engine_ServiceLocator_Plugin_Image_Gd', 1),
(12, 'Imagick', 'image', 'imagick', 'Engine_ServiceLocator_Plugin_Image_Imagick', 1),
(13, 'Akismet', 'akismet', 'standard', 'Engine_ServiceLocator_Plugin_Akismet', 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_services`
--

CREATE TABLE IF NOT EXISTS `engine4_core_services` (
  `service_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `profile` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`service_id`),
  UNIQUE KEY `type` (`type`,`profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_servicetypes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_servicetypes` (
  `servicetype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `interface` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`servicetype_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `engine4_core_servicetypes`
--

INSERT INTO `engine4_core_servicetypes` (`servicetype_id`, `title`, `type`, `interface`, `enabled`) VALUES
(1, 'Database', 'database', 'Zend_Db_Adapter_Abstract', 1),
(2, 'Cache', 'cache', 'Zend_Cache_Backend', 1),
(3, 'Captcha', 'captcha', 'Zend_Captcha_Adapter', 1),
(4, 'Mail Transport', 'mail', 'Zend_Mail_Transport_Abstract', 1),
(5, 'Image', 'image', 'Engine_Image_Adapter_Abstract', 1),
(6, 'Akismet', 'akismet', 'Zend_Service_Akismet', 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_session`
--

CREATE TABLE IF NOT EXISTS `engine4_core_session` (
  `id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_core_session`
--

INSERT INTO `engine4_core_session` (`id`, `modified`, `lifetime`, `data`, `user_id`) VALUES
('0h0kcp9a9vci5a6ogsk2cj2q17', 1454913656, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('0hfs9hofb3bau962emdk16eiv0', 1454921999, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('0ivc7e86vroulj4m52aof02442', 1454914091, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('2tk7vicfds9gmkdaiai0uf5s70', 1454914676, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('3bnf2650jbu16k27s4hbo58hd4', 1454912781, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('3u8t4vseji29n3ab5bte6gtnf3', 1454913541, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('5ot1gi1vh9ailqk97teg0p3o87', 1454910942, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('5oucqd15clsd24o2qvpp54sr66', 1454910509, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('61n2ciotl2bg89ul7fsn899824', 1454923152, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}redirectURL|s:17:"/Rhetornews/login";Zend_Auth|a:1:{s:7:"storage";i:1;}login_id|s:2:"42";ActivityFormToken|a:1:{s:5:"token";s:32:"d807c4804b0e601c57c511c69ce9762d";}twitter_lock|i:1;twitter_uid|b:0;', 1),
('72o43tqj6n3sanuq19go2qprr0', 1454915146, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('a6mpcjisvhs7qmgdsn2bse4k34', 1454913102, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('aoa7jvpqe7ifr5qqv61e57dj80', 1454922676, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('av2h35jh20k7vmbdc27qq3ef56', 1454913368, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('b0orvnd0tartv73utnkmmff9v6', 1454913026, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('bnj0shu1gopo0agrqb8hki1ss2', 1454922606, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('bse5kqmu0no8egnbrpuo0saa01', 1454910129, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('dp7hmmba1ouuv3t8hiamba9v36', 1454914459, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('e2r78v6a4t66nk06ma84hidju6', 1454914546, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('ermm6dbjr4lf2djl3iabc87ru2', 1454914221, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('g6fufqef3trrmaijsu9schlco4', 1454913745, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('hmknqaeefb015vv7cj3pluq8u2', 1454912571, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('hsp2smmq4o71lrq6d4am816ag3', 1454910588, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('jhjcqlgiscg49gejoopk7p0k75', 1454914800, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('k3bli95bl1dulners56g3qlr14', 1454911088, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('kfql2p6jcvd2mlp4r0q01h2q73', 1454914995, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('kr7rs5gt2uev85311lum6j6at0', 1454913972, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('l1lrurb2t7tha5v8ui6bacs9a1', 1454913851, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('l77lbbnshngc9gsc5js6pstb57', 1454905570, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('ll2eeptdcq7bg8lvt720mqv4r3', 1454914611, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('mo1ukiioft3l607gmc1bdh7rj2', 1454910721, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('q8q5850rgr65gjd3lstjtfms94', 1454913199, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('qetu2b5s8e4nrjegkcg5fhgom4', 1454923000, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('qj51tktpau514u0c0gk7dls144', 1454910437, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('smrtb198n8h0a6amousa4lla76', 1454923105, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('t70gmqpqovsiq3nqrrcghj4oh0', 1454910789, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('to88n7qp1cba8eo7nnt3n9mik5', 1454912506, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('ucvhlb451umo0m0oicta83qri3', 1454912903, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('ugi4fpvtques26q50bequb9890', 1454910649, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('uuf0uo4cp4js3ojipa94caggs3', 1454923069, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL),
('vd0ivmitdla9l9bcsd93pmrq71', 1454912658, 86400, 'mobile|a:1:{s:6:"mobile";b:0;}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_settings`
--

CREATE TABLE IF NOT EXISTS `engine4_core_settings` (
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_core_settings`
--

INSERT INTO `engine4_core_settings` (`name`, `value`) VALUES
('activity.content', 'everyone'),
('activity.disallowed', 'N'),
('activity.filter', '1'),
('activity.length', '15'),
('activity.liveupdate', '120000'),
('activity.publish', '1'),
('activity.userdelete', '1'),
('activity.userlength', '5'),
('chat.general.delay', '5000'),
('chat.im.privacy', 'friends'),
('classified.currency', '$'),
('core.admin.mode', 'none'),
('core.admin.password', ''),
('core.admin.reauthenticate', '0'),
('core.admin.timeout', '600'),
('core.doctype', 'XHTML1_STRICT'),
('core.facebook.enable', 'none'),
('core.facebook.key', ''),
('core.facebook.secret', ''),
('core.general.browse', '1'),
('core.general.commenthtml', ''),
('core.general.notificationupdate', '120000'),
('core.general.portal', '1'),
('core.general.profile', '1'),
('core.general.quota', '0'),
('core.general.search', '1'),
('core.general.site.title', 'Rhetornews'),
('core.license.email', 'email@domain.com'),
('core.license.key', '4336-4336-6666-1296'),
('core.license.statistics', '1'),
('core.locale.locale', 'auto'),
('core.locale.timezone', 'US/Pacific'),
('core.log.adapter', 'file'),
('core.mail.count', '25'),
('core.mail.enabled', '1'),
('core.mail.from', 'email@domain.com'),
('core.mail.name', 'Site Admin'),
('core.mail.queueing', '1'),
('core.secret', 'cdc97a0b643c71c73663775ac8c9df18dfd84cc3'),
('core.site.counter', '28'),
('core.site.creation', '2016-01-01 09:41:16'),
('core.site.title', 'Social Network'),
('core.spam.censor', ''),
('core.spam.comment', '0'),
('core.spam.contact', '0'),
('core.spam.email.antispam.login', '1'),
('core.spam.email.antispam.signup', '1'),
('core.spam.invite', '0'),
('core.spam.ipbans', ''),
('core.spam.login', '0'),
('core.spam.signup', '0'),
('core.tasks.count', '1'),
('core.tasks.interval', '60'),
('core.tasks.jobs', '3'),
('core.tasks.key', '761d8b99'),
('core.tasks.last', '1454923066'),
('core.tasks.mode', 'curl'),
('core.tasks.pid', ''),
('core.tasks.processes', '2'),
('core.tasks.time', '120'),
('core.tasks.timeout', '900'),
('core.thumbnails.icon.height', '48'),
('core.thumbnails.icon.mode', 'crop'),
('core.thumbnails.icon.width', '48'),
('core.thumbnails.main.height', '720'),
('core.thumbnails.main.mode', 'resize'),
('core.thumbnails.main.width', '720'),
('core.thumbnails.normal.height', '160'),
('core.thumbnails.normal.mode', 'resize'),
('core.thumbnails.normal.width', '140'),
('core.thumbnails.profile.height', '400'),
('core.thumbnails.profile.mode', 'resize'),
('core.thumbnails.profile.width', '200'),
('core.translate.adapter', 'csv'),
('core.twitter.enable', 'none'),
('core.twitter.key', ''),
('core.twitter.secret', ''),
('event.bbcode', '1'),
('event.html', '1'),
('forum.bbcode', '1'),
('forum.forum.pagelength', '25'),
('forum.html', '1'),
('forum.public', '1'),
('forum.topic.pagelength', '25'),
('group.bbcode', '1'),
('group.html', '1'),
('invite.allowCustomMessage', '1'),
('invite.fromEmail', ''),
('invite.fromName', ''),
('invite.max', '10'),
('invite.message', 'You are being invited to join our social network.'),
('invite.subject', 'Join Us'),
('music.playlistsperpage', '10'),
('payment.benefit', 'all'),
('payment.currency', 'USD'),
('payment.secret', 'c27a78310680a4e38cd11ae014688913'),
('poll.canchangevote', '1'),
('poll.maxoptions', '15'),
('poll.showpiechart', '0'),
('storage.service.mirrored.counter', '0'),
('storage.service.mirrored.index', '0'),
('storage.service.roundrobin.counter', '0'),
('user.friends.direction', '1'),
('user.friends.eligible', '2'),
('user.friends.lists', '1'),
('user.friends.verification', '1'),
('user.signup.approve', '1'),
('user.signup.checkemail', '1'),
('user.signup.inviteonly', '0'),
('user.signup.random', '0'),
('user.signup.terms', '1'),
('user.signup.username', '1'),
('user.signup.verifyemail', '0'),
('user.support.links', '1'),
('video.embeds', '1'),
('video.ffmpeg.path', ''),
('video.html5', ''),
('video.jobs', '2'),
('video.page', '10'),
('video.youtube.apikey', 'AIzaSyDXVh9Q0OhOZhNKdD1tD05BRCN2LwFKMVg');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_statistics`
--

CREATE TABLE IF NOT EXISTS `engine4_core_statistics` (
  `type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `date` datetime NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_core_statistics`
--

INSERT INTO `engine4_core_statistics` (`type`, `date`, `value`) VALUES
('core.comments', '2016-01-01 12:00:00', 1),
('core.comments', '2016-01-04 14:00:00', 1),
('core.comments', '2016-01-05 06:00:00', 1),
('core.emails', '2016-01-01 10:00:00', 3),
('core.emails', '2016-01-07 07:00:00', 1),
('core.emails', '2016-01-25 06:00:00', 1),
('core.views', '2016-01-01 09:00:00', 12),
('core.views', '2016-01-01 10:00:00', 67),
('core.views', '2016-01-01 11:00:00', 6),
('core.views', '2016-01-01 12:00:00', 7),
('core.views', '2016-01-04 04:00:00', 4),
('core.views', '2016-01-04 11:00:00', 5),
('core.views', '2016-01-04 12:00:00', 4),
('core.views', '2016-01-04 13:00:00', 4),
('core.views', '2016-01-04 14:00:00', 4),
('core.views', '2016-01-05 04:00:00', 5),
('core.views', '2016-01-05 05:00:00', 9),
('core.views', '2016-01-05 06:00:00', 19),
('core.views', '2016-01-05 07:00:00', 22),
('core.views', '2016-01-07 07:00:00', 17),
('core.views', '2016-01-12 05:00:00', 85),
('core.views', '2016-01-12 06:00:00', 1),
('core.views', '2016-01-13 05:00:00', 11),
('core.views', '2016-01-13 06:00:00', 13),
('core.views', '2016-01-13 10:00:00', 10),
('core.views', '2016-01-18 04:00:00', 19),
('core.views', '2016-01-18 05:00:00', 9),
('core.views', '2016-01-18 06:00:00', 10),
('core.views', '2016-01-18 07:00:00', 3),
('core.views', '2016-01-18 08:00:00', 11),
('core.views', '2016-01-19 14:00:00', 4),
('core.views', '2016-01-19 15:00:00', 14),
('core.views', '2016-01-20 04:00:00', 6),
('core.views', '2016-01-20 05:00:00', 31),
('core.views', '2016-01-20 06:00:00', 7),
('core.views', '2016-01-20 07:00:00', 3),
('core.views', '2016-01-20 08:00:00', 9),
('core.views', '2016-01-20 09:00:00', 32),
('core.views', '2016-01-20 10:00:00', 18),
('core.views', '2016-01-20 11:00:00', 27),
('core.views', '2016-01-20 12:00:00', 13),
('core.views', '2016-01-20 13:00:00', 10),
('core.views', '2016-01-20 14:00:00', 1),
('core.views', '2016-01-21 03:00:00', 1),
('core.views', '2016-01-21 04:00:00', 4),
('core.views', '2016-01-21 05:00:00', 13),
('core.views', '2016-01-21 06:00:00', 19),
('core.views', '2016-01-21 07:00:00', 9),
('core.views', '2016-01-21 08:00:00', 4),
('core.views', '2016-01-21 09:00:00', 24),
('core.views', '2016-01-21 10:00:00', 37),
('core.views', '2016-01-21 11:00:00', 23),
('core.views', '2016-01-21 12:00:00', 24),
('core.views', '2016-01-21 13:00:00', 16),
('core.views', '2016-01-21 14:00:00', 3),
('core.views', '2016-01-22 04:00:00', 5),
('core.views', '2016-01-22 05:00:00', 1),
('core.views', '2016-01-22 13:00:00', 2),
('core.views', '2016-01-22 14:00:00', 8),
('core.views', '2016-01-23 04:00:00', 5),
('core.views', '2016-01-23 05:00:00', 14),
('core.views', '2016-01-23 06:00:00', 12),
('core.views', '2016-01-23 07:00:00', 6),
('core.views', '2016-01-23 08:00:00', 3),
('core.views', '2016-01-23 10:00:00', 12),
('core.views', '2016-01-23 11:00:00', 3),
('core.views', '2016-01-23 13:00:00', 1),
('core.views', '2016-01-23 14:00:00', 26),
('core.views', '2016-01-25 04:00:00', 11),
('core.views', '2016-01-25 05:00:00', 9),
('core.views', '2016-01-25 06:00:00', 44),
('core.views', '2016-01-25 08:00:00', 16),
('core.views', '2016-01-25 09:00:00', 26),
('core.views', '2016-01-25 10:00:00', 36),
('core.views', '2016-01-25 11:00:00', 7),
('core.views', '2016-01-25 12:00:00', 4),
('core.views', '2016-01-27 03:00:00', 14),
('core.views', '2016-01-27 04:00:00', 17),
('core.views', '2016-01-27 10:00:00', 1),
('core.views', '2016-01-28 03:00:00', 16),
('core.views', '2016-01-28 04:00:00', 16),
('core.views', '2016-01-28 05:00:00', 7),
('core.views', '2016-01-28 07:00:00', 6),
('core.views', '2016-01-28 08:00:00', 18),
('core.views', '2016-01-31 05:00:00', 10),
('core.views', '2016-01-31 06:00:00', 14),
('core.views', '2016-01-31 07:00:00', 11),
('core.views', '2016-01-31 08:00:00', 5),
('core.views', '2016-01-31 09:00:00', 22),
('core.views', '2016-01-31 10:00:00', 21),
('core.views', '2016-01-31 11:00:00', 6),
('core.views', '2016-01-31 12:00:00', 22),
('core.views', '2016-01-31 13:00:00', 4),
('core.views', '2016-02-01 04:00:00', 11),
('core.views', '2016-02-01 05:00:00', 3),
('core.views', '2016-02-01 09:00:00', 9),
('core.views', '2016-02-02 06:00:00', 7),
('core.views', '2016-02-02 07:00:00', 35),
('core.views', '2016-02-02 08:00:00', 7),
('core.views', '2016-02-02 09:00:00', 12),
('core.views', '2016-02-02 10:00:00', 33),
('core.views', '2016-02-02 11:00:00', 23),
('core.views', '2016-02-02 12:00:00', 7),
('core.views', '2016-02-02 13:00:00', 5),
('core.views', '2016-02-03 04:00:00', 8),
('core.views', '2016-02-03 13:00:00', 1),
('core.views', '2016-02-03 14:00:00', 3),
('core.views', '2016-02-04 04:00:00', 1),
('core.views', '2016-02-05 04:00:00', 4),
('core.views', '2016-02-05 05:00:00', 3),
('core.views', '2016-02-05 06:00:00', 21),
('core.views', '2016-02-05 07:00:00', 4),
('core.views', '2016-02-05 08:00:00', 6),
('core.views', '2016-02-05 09:00:00', 128),
('core.views', '2016-02-05 10:00:00', 35),
('core.views', '2016-02-05 11:00:00', 15),
('core.views', '2016-02-05 12:00:00', 19),
('core.views', '2016-02-06 08:00:00', 1),
('core.views', '2016-02-06 09:00:00', 3),
('core.views', '2016-02-06 10:00:00', 14),
('core.views', '2016-02-06 11:00:00', 3),
('core.views', '2016-02-06 12:00:00', 10),
('core.views', '2016-02-08 04:00:00', 1),
('core.views', '2016-02-08 05:00:00', 9),
('core.views', '2016-02-08 06:00:00', 26),
('core.views', '2016-02-08 07:00:00', 14),
('core.views', '2016-02-08 08:00:00', 1),
('core.views', '2016-02-08 09:00:00', 10),
('user.creations', '2016-01-01 10:00:00', 1),
('user.creations', '2016-01-07 07:00:00', 1),
('user.creations', '2016-01-25 06:00:00', 1),
('user.friendships', '2016-01-01 10:00:00', 1),
('user.logins', '2016-01-01 10:00:00', 2),
('user.logins', '2016-01-04 04:00:00', 1),
('user.logins', '2016-01-04 11:00:00', 1),
('user.logins', '2016-01-04 12:00:00', 1),
('user.logins', '2016-01-05 04:00:00', 1),
('user.logins', '2016-01-12 05:00:00', 1),
('user.logins', '2016-01-13 06:00:00', 1),
('user.logins', '2016-01-18 04:00:00', 1),
('user.logins', '2016-01-19 15:00:00', 1),
('user.logins', '2016-01-20 06:00:00', 1),
('user.logins', '2016-01-21 04:00:00', 1),
('user.logins', '2016-01-21 05:00:00', 2),
('user.logins', '2016-01-21 13:00:00', 2),
('user.logins', '2016-01-22 04:00:00', 1),
('user.logins', '2016-01-22 14:00:00', 1),
('user.logins', '2016-01-23 05:00:00', 1),
('user.logins', '2016-01-25 05:00:00', 1),
('user.logins', '2016-01-25 06:00:00', 1),
('user.logins', '2016-01-25 09:00:00', 1),
('user.logins', '2016-01-25 10:00:00', 1),
('user.logins', '2016-01-27 03:00:00', 1),
('user.logins', '2016-01-28 03:00:00', 1),
('user.logins', '2016-01-28 04:00:00', 1),
('user.logins', '2016-01-31 05:00:00', 2),
('user.logins', '2016-02-01 04:00:00', 3),
('user.logins', '2016-02-02 06:00:00', 2),
('user.logins', '2016-02-03 04:00:00', 1),
('user.logins', '2016-02-03 14:00:00', 1),
('user.logins', '2016-02-05 04:00:00', 1),
('user.logins', '2016-02-08 05:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_status`
--

CREATE TABLE IF NOT EXISTS `engine4_core_status` (
  `status_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resource_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `resource_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `engine4_core_status`
--

INSERT INTO `engine4_core_status` (`status_id`, `resource_type`, `resource_id`, `body`, `creation_date`) VALUES
(1, 'user', 1, 'wscdw', '2016-01-01 12:20:53'),
(2, 'user', 1, '<a href="https://www.youtube.com/watch?v=OU8ulREBIiw" target="_blank" rel="nofollow">https://www.youtube.com/watch?v=OU8ulREBIiw</a>', '2016-01-05 05:01:01');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_styles`
--

CREATE TABLE IF NOT EXISTS `engine4_core_styles` (
  `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id` int(11) unsigned NOT NULL,
  `style` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`type`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_tagmaps`
--

CREATE TABLE IF NOT EXISTS `engine4_core_tagmaps` (
  `tagmap_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resource_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `resource_id` int(11) unsigned NOT NULL,
  `tagger_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tagger_id` int(11) unsigned NOT NULL,
  `tag_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`tagmap_id`),
  KEY `resource_type` (`resource_type`,`resource_id`),
  KEY `tagger_type` (`tagger_type`,`tagger_id`),
  KEY `tag_type` (`tag_type`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_tags`
--

CREATE TABLE IF NOT EXISTS `engine4_core_tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `text` (`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_tasks`
--

CREATE TABLE IF NOT EXISTS `engine4_core_tasks` (
  `task_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `plugin` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `timeout` int(11) unsigned NOT NULL DEFAULT '60',
  `processes` smallint(3) unsigned NOT NULL DEFAULT '1',
  `semaphore` smallint(3) NOT NULL DEFAULT '0',
  `started_last` int(11) NOT NULL DEFAULT '0',
  `started_count` int(11) unsigned NOT NULL DEFAULT '0',
  `completed_last` int(11) NOT NULL DEFAULT '0',
  `completed_count` int(11) unsigned NOT NULL DEFAULT '0',
  `failure_last` int(11) NOT NULL DEFAULT '0',
  `failure_count` int(11) unsigned NOT NULL DEFAULT '0',
  `success_last` int(11) NOT NULL DEFAULT '0',
  `success_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_id`),
  UNIQUE KEY `plugin` (`plugin`),
  KEY `module` (`module`),
  KEY `started_last` (`started_last`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `engine4_core_tasks`
--

INSERT INTO `engine4_core_tasks` (`task_id`, `title`, `module`, `plugin`, `timeout`, `processes`, `semaphore`, `started_last`, `started_count`, `completed_last`, `completed_count`, `failure_last`, `failure_count`, `success_last`, `success_count`) VALUES
(1, 'Job Queue', 'core', 'Core_Plugin_Task_Jobs', 5, 1, 0, 1454923067, 1723, 1454923067, 1723, 0, 0, 1454923067, 1723),
(2, 'Background Mailer', 'core', 'Core_Plugin_Task_Mail', 15, 1, 0, 1454923068, 1723, 1454923068, 1723, 0, 0, 1454923068, 1723),
(3, 'Cache Prefetch', 'core', 'Core_Plugin_Task_Prefetch', 300, 1, 0, 1454922999, 632, 1454923000, 632, 0, 0, 1454923000, 632),
(4, 'Statistics', 'core', 'Core_Plugin_Task_Statistics', 43200, 1, 0, 1454910505, 22, 1454910508, 22, 0, 0, 1454910508, 22),
(5, 'Log Rotation', 'core', 'Core_Plugin_Task_LogRotation', 7200, 1, 0, 1454922676, 60, 1454922676, 60, 0, 0, 1454922676, 60),
(6, 'Member Data Maintenance', 'user', 'User_Plugin_Task_Cleanup', 60, 1, 0, 1454923068, 996, 1454923069, 996, 0, 0, 1454923069, 996),
(7, 'Payment Maintenance', 'user', 'Payment_Plugin_Task_Cleanup', 43200, 1, 0, 1454910649, 22, 1454910649, 22, 0, 0, 1454910649, 22),
(8, 'Chat Data Maintenance', 'chat', 'Chat_Plugin_Task_Cleanup', 60, 1, 0, 1454923069, 964, 1454923069, 964, 0, 0, 1454923069, 964),
(9, 'Music Cleanup', 'music', 'Music_Plugin_Task_Cleanup', 43200, 1, 0, 1454910721, 22, 1454910721, 22, 0, 0, 1454910721, 22);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_core_themes`
--

CREATE TABLE IF NOT EXISTS `engine4_core_themes` (
  `theme_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`theme_id`),
  UNIQUE KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `engine4_core_themes`
--

INSERT INTO `engine4_core_themes` (`theme_id`, `name`, `title`, `description`, `active`) VALUES
(1, 'default', 'Default', '', 0),
(2, 'midnight', 'Midnight', '', 0),
(3, 'clean', 'Clean', '', 0),
(4, 'modern', 'Modern', '', 1),
(5, 'bamboo', 'Bamboo', '', 0),
(6, 'digita', 'Digita', '', 0),
(7, 'grid-blue', 'Grid Blue', '', 0),
(8, 'grid-brown', 'Grid Brown', '', 0),
(9, 'grid-dark', 'Grid Dark', '', 0),
(10, 'grid-gray', 'Grid Gray', '', 0),
(11, 'grid-green', 'Grid Green', '', 0),
(12, 'grid-pink', 'Grid Pink', '', 0),
(13, 'grid-purple', 'Grid Purple', '', 0),
(14, 'grid-red', 'Grid Red', '', 0),
(15, 'kandy-cappuccino', 'Kandy Cappuccino', '', 0),
(16, 'kandy-limeorange', 'Kandy Limeorange', '', 0),
(17, 'kandy-mangoberry', 'Kandy Mangoberry', '', 0),
(18, 'kandy-watermelon', 'Kandy Watermelon', '', 0),
(19, 'musicbox-blue', 'Musicbox Blue', '', 0),
(20, 'musicbox-brown', 'Musicbox Brown', '', 0),
(21, 'musicbox-gray', 'Musicbox Gray', '', 0),
(22, 'musicbox-green', 'Musicbox Green', '', 0),
(23, 'musicbox-pink', 'Musicbox Pink', '', 0),
(24, 'musicbox-purple', 'Musicbox Purple', '', 0),
(25, 'musicbox-red', 'Musicbox Red', '', 0),
(26, 'musicbox-yellow', 'Musicbox Yellow', '', 0),
(27, 'quantum-beige', 'Quantum Beige', '', 0),
(28, 'quantum-blue', 'Quantum Blue', '', 0),
(29, 'quantum-gray', 'Quantum Gray', '', 0),
(30, 'quantum-green', 'Quantum Green', '', 0),
(31, 'quantum-orange', 'Quantum Orange', '', 0),
(32, 'quantum-pink', 'Quantum Pink', '', 0),
(33, 'quantum-purple', 'Quantum Purple', '', 0),
(34, 'quantum-red', 'Quantum Red', '', 0),
(35, 'slipstream', 'Slipstream', '', 0),
(36, 'snowbot', 'Snowbot', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_albums`
--

CREATE TABLE IF NOT EXISTS `engine4_event_albums` (
  `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `collectible_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`album_id`),
  KEY `event_id` (`event_id`),
  KEY `search` (`search`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_event_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `engine4_event_categories`
--

INSERT INTO `engine4_event_categories` (`category_id`, `title`) VALUES
(1, 'Arts'),
(2, 'Business'),
(3, 'Conferences'),
(4, 'Festivals'),
(5, 'Food'),
(6, 'Fundraisers'),
(7, 'Galleries'),
(8, 'Health'),
(9, 'Just For Fun'),
(10, 'Kids'),
(11, 'Learning'),
(12, 'Literary'),
(13, 'Movies'),
(14, 'Museums'),
(15, 'Neighborhood'),
(16, 'Networking'),
(17, 'Nightlife'),
(18, 'On Campus'),
(19, 'Organizations'),
(20, 'Outdoors'),
(21, 'Pets'),
(22, 'Politics'),
(23, 'Sales'),
(24, 'Science'),
(25, 'Spirituality'),
(26, 'Sports'),
(27, 'Technology'),
(28, 'Theatre'),
(29, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_events`
--

CREATE TABLE IF NOT EXISTS `engine4_event_events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `parent_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `host` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `member_count` int(11) unsigned NOT NULL DEFAULT '0',
  `approval` tinyint(1) NOT NULL DEFAULT '0',
  `invite` tinyint(1) NOT NULL DEFAULT '0',
  `photo_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`),
  KEY `user_id` (`user_id`),
  KEY `parent_type` (`parent_type`,`parent_id`),
  KEY `starttime` (`starttime`),
  KEY `search` (`search`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_membership`
--

CREATE TABLE IF NOT EXISTS `engine4_event_membership` (
  `resource_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `resource_approved` tinyint(1) NOT NULL DEFAULT '0',
  `user_approved` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci,
  `rsvp` tinyint(3) NOT NULL DEFAULT '3',
  `title` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`resource_id`,`user_id`),
  KEY `REVERSE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_photos`
--

CREATE TABLE IF NOT EXISTS `engine4_event_photos` (
  `photo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`photo_id`),
  KEY `album_id` (`album_id`),
  KEY `event_id` (`event_id`),
  KEY `collection_id` (`collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_posts`
--

CREATE TABLE IF NOT EXISTS `engine4_event_posts` (
  `post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `topic_id` (`topic_id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_topics`
--

CREATE TABLE IF NOT EXISTS `engine4_event_topics` (
  `topic_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `post_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastpost_id` int(11) unsigned NOT NULL,
  `lastposter_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`topic_id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_event_topicwatches`
--

CREATE TABLE IF NOT EXISTS `engine4_event_topicwatches` (
  `resource_id` int(10) unsigned NOT NULL,
  `topic_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `watch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`resource_id`,`topic_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '0',
  `forum_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  KEY `order` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `engine4_forum_categories`
--

INSERT INTO `engine4_forum_categories` (`category_id`, `title`, `description`, `creation_date`, `modified_date`, `order`, `forum_count`) VALUES
(1, 'General', '', '2016-01-01 10:13:56', '2016-01-01 10:13:56', 1, 3),
(2, 'Off-Topic', '', '2016-01-01 10:13:56', '2016-01-01 10:13:56', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_forums`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_forums` (
  `forum_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '999',
  `file_id` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `topic_count` int(11) unsigned NOT NULL DEFAULT '0',
  `post_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastpost_id` int(11) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`forum_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `engine4_forum_forums`
--

INSERT INTO `engine4_forum_forums` (`forum_id`, `category_id`, `title`, `description`, `creation_date`, `modified_date`, `order`, `file_id`, `view_count`, `topic_count`, `post_count`, `lastpost_id`, `lastposter_id`) VALUES
(1, 1, 'News and Announcements', '', '2010-02-01 14:59:01', '2010-02-01 14:59:01', 1, 0, 0, 0, 0, 0, 0),
(2, 1, 'Support', '', '2010-02-01 15:09:01', '2010-02-01 17:59:01', 2, 0, 0, 0, 0, 0, 0),
(3, 1, 'Suggestions', '', '2010-02-01 15:09:01', '2010-02-01 17:59:01', 3, 0, 0, 0, 0, 0, 0),
(4, 2, 'Off-Topic Discussions', '', '2010-02-01 15:09:01', '2010-02-01 17:59:01', 1, 0, 0, 0, 0, 0, 0),
(5, 2, 'Introduce Yourself', '', '2010-02-01 15:09:01', '2010-02-01 17:59:01', 2, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_listitems`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_listitems` (
  `listitem_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`listitem_id`),
  KEY `list_id` (`list_id`),
  KEY `child_id` (`child_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `engine4_forum_listitems`
--

INSERT INTO `engine4_forum_listitems` (`listitem_id`, `list_id`, `child_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_lists`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_lists` (
  `list_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) unsigned NOT NULL,
  `child_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `engine4_forum_lists`
--

INSERT INTO `engine4_forum_lists` (`list_id`, `owner_id`, `child_count`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_membership`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_membership` (
  `resource_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `resource_approved` tinyint(1) NOT NULL DEFAULT '0',
  `moderator` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`resource_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_posts`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_posts` (
  `post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) unsigned NOT NULL,
  `forum_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `file_id` int(11) unsigned NOT NULL DEFAULT '0',
  `edit_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_signatures`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_signatures` (
  `signature_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `post_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`signature_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_topics`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_topics` (
  `topic_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `sticky` tinyint(4) NOT NULL DEFAULT '0',
  `closed` tinyint(4) NOT NULL DEFAULT '0',
  `post_count` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastpost_id` int(11) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic_id`),
  KEY `forum_id` (`forum_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_topicviews`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_topicviews` (
  `user_id` int(11) unsigned NOT NULL,
  `topic_id` int(11) unsigned NOT NULL,
  `last_view_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_forum_topicwatches`
--

CREATE TABLE IF NOT EXISTS `engine4_forum_topicwatches` (
  `resource_id` int(10) unsigned NOT NULL,
  `topic_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `watch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`resource_id`,`topic_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_albums`
--

CREATE TABLE IF NOT EXISTS `engine4_group_albums` (
  `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `collectible_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`album_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_group_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `engine4_group_categories`
--

INSERT INTO `engine4_group_categories` (`category_id`, `title`) VALUES
(1, 'Animals'),
(2, 'Business & Finance'),
(3, 'Computers & Internet'),
(4, 'Cultures & Community'),
(5, 'Dating & Relationships'),
(6, 'Entertainment & Arts'),
(7, 'Family & Home'),
(8, 'Games'),
(9, 'Government & Politics'),
(10, 'Health & Wellness'),
(11, 'Hobbies & Crafts'),
(12, 'Music'),
(13, 'Recreation & Sports'),
(14, 'Regional'),
(15, 'Religion & Beliefs'),
(16, 'Schools & Education'),
(17, 'Science');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_groups`
--

CREATE TABLE IF NOT EXISTS `engine4_group_groups` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `invite` tinyint(1) NOT NULL DEFAULT '1',
  `approval` tinyint(1) NOT NULL DEFAULT '0',
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `member_count` smallint(6) unsigned NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `search` (`search`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_listitems`
--

CREATE TABLE IF NOT EXISTS `engine4_group_listitems` (
  `listitem_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`listitem_id`),
  KEY `list_id` (`list_id`),
  KEY `child_id` (`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_lists`
--

CREATE TABLE IF NOT EXISTS `engine4_group_lists` (
  `list_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `owner_id` int(11) unsigned NOT NULL,
  `child_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_membership`
--

CREATE TABLE IF NOT EXISTS `engine4_group_membership` (
  `resource_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `resource_approved` tinyint(1) NOT NULL DEFAULT '0',
  `user_approved` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci,
  `title` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`resource_id`,`user_id`),
  KEY `REVERSE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_photos`
--

CREATE TABLE IF NOT EXISTS `engine4_group_photos` (
  `photo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`photo_id`),
  KEY `album_id` (`album_id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_posts`
--

CREATE TABLE IF NOT EXISTS `engine4_group_posts` (
  `post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `topic_id` (`topic_id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_topics`
--

CREATE TABLE IF NOT EXISTS `engine4_group_topics` (
  `topic_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `post_count` int(11) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastpost_id` int(11) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic_id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_group_topicwatches`
--

CREATE TABLE IF NOT EXISTS `engine4_group_topicwatches` (
  `resource_id` int(10) unsigned NOT NULL,
  `topic_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `watch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`resource_id`,`topic_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_invites`
--

CREATE TABLE IF NOT EXISTS `engine4_invites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `send_request` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `new_user_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `user_id` (`user_id`),
  KEY `recipient` (`recipient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_messages_conversations`
--

CREATE TABLE IF NOT EXISTS `engine4_messages_conversations` (
  `conversation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) unsigned NOT NULL,
  `recipients` int(11) unsigned NOT NULL,
  `modified` datetime NOT NULL,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `resource_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
  `resource_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`conversation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_messages_messages`
--

CREATE TABLE IF NOT EXISTS `engine4_messages_messages` (
  `message_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `attachment_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
  `attachment_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`),
  UNIQUE KEY `CONVERSATIONS` (`conversation_id`,`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_messages_recipients`
--

CREATE TABLE IF NOT EXISTS `engine4_messages_recipients` (
  `user_id` int(11) unsigned NOT NULL,
  `conversation_id` int(11) unsigned NOT NULL,
  `inbox_message_id` int(11) unsigned DEFAULT NULL,
  `inbox_updated` datetime DEFAULT NULL,
  `inbox_read` tinyint(1) DEFAULT NULL,
  `inbox_deleted` tinyint(1) DEFAULT NULL,
  `outbox_message_id` int(11) unsigned DEFAULT NULL,
  `outbox_updated` datetime DEFAULT NULL,
  `outbox_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`conversation_id`),
  KEY `INBOX_UPDATED` (`user_id`,`conversation_id`,`inbox_updated`),
  KEY `OUTBOX_UPDATED` (`user_id`,`conversation_id`,`outbox_updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_music_playlists`
--

CREATE TABLE IF NOT EXISTS `engine4_music_playlists` (
  `playlist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(63) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `owner_type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `profile` tinyint(1) NOT NULL DEFAULT '0',
  `special` enum('wall','message') COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `play_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`playlist_id`),
  KEY `creation_date` (`creation_date`),
  KEY `play_count` (`play_count`),
  KEY `owner_id` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_music_playlist_songs`
--

CREATE TABLE IF NOT EXISTS `engine4_music_playlist_songs` (
  `song_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `play_count` int(11) unsigned NOT NULL DEFAULT '0',
  `order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`song_id`),
  KEY `playlist_id` (`playlist_id`,`file_id`),
  KEY `play_count` (`play_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_network_membership`
--

CREATE TABLE IF NOT EXISTS `engine4_network_membership` (
  `resource_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `resource_approved` tinyint(1) NOT NULL DEFAULT '0',
  `user_approved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`resource_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_network_networks`
--

CREATE TABLE IF NOT EXISTS `engine4_network_networks` (
  `network_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pattern` text COLLATE utf8_unicode_ci,
  `member_count` int(11) unsigned NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `assignment` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`network_id`),
  KEY `assignment` (`assignment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `engine4_network_networks`
--

INSERT INTO `engine4_network_networks` (`network_id`, `title`, `description`, `field_id`, `pattern`, `member_count`, `hide`, `assignment`) VALUES
(1, 'North America', '', 0, NULL, 0, 0, 0),
(2, 'South America', '', 0, NULL, 0, 0, 0),
(3, 'Europe', '', 0, NULL, 0, 0, 0),
(4, 'Asia', '', 0, NULL, 0, 0, 0),
(5, 'Africa', '', 0, NULL, 0, 0, 0),
(6, 'Australia', '', 0, NULL, 0, 0, 0),
(7, 'Antarctica', '', 0, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_payment_gateways`
--

CREATE TABLE IF NOT EXISTS `engine4_payment_gateways` (
  `gateway_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `plugin` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `config` mediumblob,
  `test_mode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gateway_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `engine4_payment_gateways`
--

INSERT INTO `engine4_payment_gateways` (`gateway_id`, `title`, `description`, `enabled`, `plugin`, `config`, `test_mode`) VALUES
(1, '2Checkout', NULL, 0, 'Payment_Plugin_Gateway_2Checkout', NULL, 0),
(2, 'PayPal', NULL, 0, 'Payment_Plugin_Gateway_PayPal', NULL, 0),
(3, 'Testing', NULL, 0, 'Payment_Plugin_Gateway_Testing', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_payment_orders`
--

CREATE TABLE IF NOT EXISTS `engine4_payment_orders` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `gateway_id` int(10) unsigned NOT NULL,
  `gateway_order_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gateway_transaction_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `state` enum('pending','cancelled','failed','incomplete','complete') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'pending',
  `creation_date` datetime NOT NULL,
  `source_type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `gateway_id` (`gateway_id`,`gateway_order_id`),
  KEY `state` (`state`),
  KEY `source_type` (`source_type`,`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_payment_packages`
--

CREATE TABLE IF NOT EXISTS `engine4_payment_packages` (
  `package_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(10) unsigned NOT NULL,
  `downgrade_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `price` decimal(16,2) unsigned NOT NULL,
  `recurrence` int(11) unsigned NOT NULL,
  `recurrence_type` enum('day','week','month','year','forever') COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) unsigned NOT NULL,
  `duration_type` enum('day','week','month','year','forever') COLLATE utf8_unicode_ci NOT NULL,
  `trial_duration` int(11) unsigned NOT NULL DEFAULT '0',
  `trial_duration_type` enum('day','week','month','year','forever') COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `signup` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `after_signup` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_payment_products`
--

CREATE TABLE IF NOT EXISTS `engine4_payment_products` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `extension_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `extension_id` int(10) unsigned DEFAULT NULL,
  `sku` bigint(20) unsigned NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(16,2) unsigned NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `sku` (`sku`),
  KEY `extension_type` (`extension_type`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_payment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `engine4_payment_subscriptions` (
  `subscription_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `package_id` int(11) unsigned NOT NULL,
  `status` enum('initial','trial','pending','active','cancelled','expired','overdue','refunded') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'initial',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `gateway_id` int(10) unsigned DEFAULT NULL,
  `gateway_profile_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `gateway_id` (`gateway_id`,`gateway_profile_id`),
  KEY `user_id` (`user_id`),
  KEY `package_id` (`package_id`),
  KEY `status` (`status`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_payment_transactions`
--

CREATE TABLE IF NOT EXISTS `engine4_payment_transactions` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `gateway_id` int(10) unsigned NOT NULL,
  `timestamp` datetime NOT NULL,
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gateway_transaction_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gateway_parent_transaction_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gateway_order_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `amount` decimal(16,2) NOT NULL,
  `currency` char(3) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`transaction_id`),
  KEY `user_id` (`user_id`),
  KEY `gateway_id` (`gateway_id`),
  KEY `type` (`type`),
  KEY `state` (`state`),
  KEY `gateway_transaction_id` (`gateway_transaction_id`),
  KEY `gateway_parent_transaction_id` (`gateway_parent_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_poll_options`
--

CREATE TABLE IF NOT EXISTS `engine4_poll_options` (
  `poll_option_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) unsigned NOT NULL,
  `poll_option` text COLLATE utf8_unicode_ci NOT NULL,
  `votes` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`poll_option_id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_poll_polls`
--

CREATE TABLE IF NOT EXISTS `engine4_poll_polls` (
  `poll_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `is_closed` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `vote_count` int(11) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`poll_id`),
  KEY `user_id` (`user_id`),
  KEY `is_closed` (`is_closed`),
  KEY `creation_date` (`creation_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_poll_votes`
--

CREATE TABLE IF NOT EXISTS `engine4_poll_votes` (
  `poll_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `poll_option_id` int(11) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`poll_id`,`user_id`),
  KEY `poll_option_id` (`poll_option_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_storage_chunks`
--

CREATE TABLE IF NOT EXISTS `engine4_storage_chunks` (
  `chunk_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(11) unsigned NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`chunk_id`),
  KEY `file_id` (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_storage_files`
--

CREATE TABLE IF NOT EXISTS `engine4_storage_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_file_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `parent_type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `service_id` int(10) unsigned NOT NULL DEFAULT '1',
  `storage_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mime_major` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `mime_minor` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `hash` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `parent_file_id` (`parent_file_id`,`type`),
  KEY `PARENT` (`parent_type`,`parent_id`),
  KEY `user_id` (`user_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `engine4_storage_files`
--

INSERT INTO `engine4_storage_files` (`file_id`, `parent_file_id`, `type`, `parent_type`, `parent_id`, `user_id`, `creation_date`, `modified_date`, `service_id`, `storage_path`, `extension`, `name`, `mime_major`, `mime_minor`, `size`, `hash`) VALUES
(1, NULL, NULL, 'user', 1, 1, '2016-01-01 09:54:38', '2016-01-01 09:54:38', 1, 'public/user/01/0001_9492.jpg', 'jpg', 'index_m.jpg', 'image', 'jpeg', 8502, '68d194924c545fe54c9869773791f5de'),
(2, 1, 'thumb.profile', 'user', 1, 1, '2016-01-01 09:54:38', '2016-01-01 09:54:38', 1, 'public/user/02/0002_5971.jpg', 'jpg', 'index_p.jpg', 'image', 'jpeg', 7565, '594c5971dc15a96e6d163034f55dc7d1'),
(3, 1, 'thumb.normal', 'user', 1, 1, '2016-01-01 09:54:38', '2016-01-01 09:54:38', 1, 'public/user/03/0003_f8df.jpg', 'jpg', 'index_in.jpg', 'image', 'jpeg', 4760, '1abbf8df9cba1c29d70eedbd3c23c17c'),
(4, 1, 'thumb.icon', 'user', 1, 1, '2016-01-01 09:54:38', '2016-01-01 09:54:38', 1, 'public/user/04/0004_6dd9.jpg', 'jpg', 'index_is.jpg', 'image', 'jpeg', 1507, '06426dd966e8f4377602418fb5c0d8be'),
(5, NULL, NULL, 'user', 2, NULL, '2016-01-01 10:25:10', '2016-01-01 10:25:10', 1, 'public/user/05/0005_b020.jpg', 'jpg', 'm_Hydrangeas.jpg', 'image', 'jpeg', 50446, '20a2b02090bc18d636d7916e759bc9f5'),
(6, 5, 'thumb.profile', 'user', 2, NULL, '2016-01-01 10:25:10', '2016-01-01 10:25:10', 1, 'public/user/06/0006_2498.jpg', 'jpg', 'p_Hydrangeas.jpg', 'image', 'jpeg', 7140, 'ab552498f54afc3d2f4faa2621185968'),
(7, 5, 'thumb.normal', 'user', 2, NULL, '2016-01-01 10:25:10', '2016-01-01 10:25:10', 1, 'public/user/07/0007_278a.jpg', 'jpg', 'n_Hydrangeas.jpg', 'image', 'jpeg', 1266, '7645278aed8867fa650f5b7777c784e9'),
(8, 5, 'thumb.icon', 'user', 2, NULL, '2016-01-01 10:25:10', '2016-01-01 10:25:10', 1, 'public/user/08/0008_45a5.jpg', 'jpg', 's_Hydrangeas.jpg', 'image', 'jpeg', 1384, 'c15745a5fa733846545038d16bd20a15'),
(9, NULL, NULL, 'video', 1, 1, '2016-01-01 12:29:14', '2016-01-01 12:29:14', 1, 'public/video/09/0009_ed52.jpg', 'jpg', 'link_thumb_c0d88b0f08f4c91d4c86bf2c510be55a.jpg', 'image', 'jpeg', 2853, 'dcf9ed5244843a0dcd1d86812d9f5ea4'),
(10, NULL, NULL, 'user', 3, NULL, '2016-01-07 07:22:43', '2016-01-07 07:22:43', 1, 'public/user/0a/000a_559d.jpg', 'jpg', 'm_Jellyfish.jpg', 'image', 'jpeg', 33409, '4eb2559ddd93571a7421a9c29558aec5'),
(11, 10, 'thumb.profile', 'user', 3, NULL, '2016-01-07 07:22:44', '2016-01-07 07:22:44', 1, 'public/user/0b/000b_db16.jpg', 'jpg', 'p_Jellyfish.jpg', 'image', 'jpeg', 4872, '4271db16ef36561d033e94c0ca7dbc3d'),
(12, 10, 'thumb.normal', 'user', 3, NULL, '2016-01-07 07:22:44', '2016-01-07 07:22:44', 1, 'public/user/0c/000c_e22b.jpg', 'jpg', 'n_Jellyfish.jpg', 'image', 'jpeg', 1117, 'a13ee22b58cd6a563928de3f9d91b698'),
(13, 10, 'thumb.icon', 'user', 3, NULL, '2016-01-07 07:22:44', '2016-01-07 07:22:44', 1, 'public/user/0d/000d_1a90.jpg', 'jpg', 's_Jellyfish.jpg', 'image', 'jpeg', 1213, '742f1a905275021386ef6bb66f5094f4'),
(14, NULL, NULL, 'album_photo', 1, 3, '2016-01-07 07:26:22', '2016-01-07 07:26:22', 1, 'public/album_photo/0e/000e_28ab.jpg', 'jpg', '57d40443c07c8f645722786f4185699b_m.jpg', 'image', 'jpeg', 62595, 'e9e228ab5d0e0de64759f5849b98abcd'),
(15, 14, 'thumb.normal', 'album_photo', 1, 3, '2016-01-07 07:26:22', '2016-01-07 07:26:22', 1, 'public/album_photo/0f/000f_e0cf.jpg', 'jpg', '57d40443c07c8f645722786f4185699b_in.jpg', 'image', 'jpeg', 5034, 'a0f3e0cf4441e239fe6d56c0a53dfbab'),
(16, NULL, NULL, 'user', 1, 1, '2016-01-20 10:30:39', '2016-01-20 10:30:39', 1, 'public/user/10/0010_23fb.png', 'png', 'm_php2477.tmp.png', 'image', 'png', 80677, '7bbf23fb82190fd77b7de541bb18e317'),
(17, 16, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:30:39', '2016-01-20 10:30:39', 1, 'public/user/11/0011_d42f.png', 'png', 't_php2477.tmp.png', 'image', 'png', 3909, '04f4d42f37e0c4bb9d4466f6e320ac80'),
(18, NULL, NULL, 'user', 1, 1, '2016-01-20 10:50:03', '2016-01-20 10:50:03', 1, 'public/user/12/0012_a506.png', 'png', 'm_phpE631.tmp.png', 'image', 'png', 312, '8539a506666431b2c8d467d1280809b2'),
(19, 18, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:50:03', '2016-01-20 10:50:03', 1, 'public/user/13/0013_a506.png', 'png', 't_phpE631.tmp.png', 'image', 'png', 312, '8539a506666431b2c8d467d1280809b2'),
(20, NULL, NULL, 'user', 1, 1, '2016-01-20 10:50:47', '2016-01-20 10:50:47', 1, 'public/user/14/0014_70fd.png', 'png', 'm_php95B3.tmp.png', 'image', 'png', 227, '87ce70fd3d327b9e6900ca589a072e4f'),
(21, 20, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:50:47', '2016-01-20 10:50:47', 1, 'public/user/15/0015_70fd.png', 'png', 't_php95B3.tmp.png', 'image', 'png', 227, '87ce70fd3d327b9e6900ca589a072e4f'),
(22, NULL, NULL, 'user', 1, 1, '2016-01-20 10:50:53', '2016-01-20 10:50:53', 1, 'public/user/16/0016_7074.png', 'png', 'm_phpAD0A.tmp.png', 'image', 'png', 430, '61867074a341d37c41830ddfb976fabc'),
(23, 22, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:50:53', '2016-01-20 10:50:53', 1, 'public/user/17/0017_7074.png', 'png', 't_phpAD0A.tmp.png', 'image', 'png', 430, '61867074a341d37c41830ddfb976fabc'),
(24, NULL, NULL, 'user', 1, 1, '2016-01-20 10:51:24', '2016-01-20 10:51:24', 1, 'public/user/18/0018_276f.png', 'png', 'm_php2392.tmp.png', 'image', 'png', 202, '1158276f8e3d4104fcf9752a8eb258ac'),
(25, 24, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:51:24', '2016-01-20 10:51:24', 1, 'public/user/19/0019_276f.png', 'png', 't_php2392.tmp.png', 'image', 'png', 202, '1158276f8e3d4104fcf9752a8eb258ac'),
(26, NULL, NULL, 'user', 1, 1, '2016-01-20 10:51:36', '2016-01-20 10:51:36', 1, 'public/user/1a/001a_b395.png', 'png', 'm_php54A1.tmp.png', 'image', 'png', 282, 'ccd5b395f5bcb93b53c472a7365c0c79'),
(27, 26, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:51:36', '2016-01-20 10:51:36', 1, 'public/user/1b/001b_b395.png', 'png', 't_php54A1.tmp.png', 'image', 'png', 282, 'ccd5b395f5bcb93b53c472a7365c0c79'),
(28, NULL, NULL, 'user', 1, 1, '2016-01-20 10:51:51', '2016-01-20 10:51:51', 1, 'public/user/1c/001c_7f7b.png', 'png', 'm_php8C93.tmp.png', 'image', 'png', 305, 'ab7a7f7b02b692fe5ebce67086bcb729'),
(29, 28, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:51:51', '2016-01-20 10:51:51', 1, 'public/user/1d/001d_7f7b.png', 'png', 't_php8C93.tmp.png', 'image', 'png', 305, 'ab7a7f7b02b692fe5ebce67086bcb729'),
(31, 30, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:52:01', '2016-01-20 10:52:01', 1, 'public/user/1f/001f_a506.png', 'png', 't_phpB394.tmp.png', 'image', 'png', 312, '8539a506666431b2c8d467d1280809b2'),
(33, 32, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:57:54', '2016-01-20 10:57:54', 1, 'public/user/21/0021_276f.png', 'png', 't_php161E.tmp.png', 'image', 'png', 202, '1158276f8e3d4104fcf9752a8eb258ac'),
(35, 34, 'thumb.normal', 'user', 1, 1, '2016-01-20 10:58:29', '2016-01-20 10:58:29', 1, 'public/user/23/0023_b395.png', 'png', 't_phpA286.tmp.png', 'image', 'png', 282, 'ccd5b395f5bcb93b53c472a7365c0c79'),
(36, NULL, NULL, 'user', 1, 1, '2016-01-20 11:03:11', '2016-01-20 11:03:11', 1, 'public/user/24/0024_276f.png', 'png', 'm_phpEFAC.tmp.png', 'image', 'png', 202, '1158276f8e3d4104fcf9752a8eb258ac'),
(37, 36, 'thumb.normal', 'user', 1, 1, '2016-01-20 11:03:11', '2016-01-20 11:03:11', 1, 'public/user/25/0025_276f.png', 'png', 't_phpEFAC.tmp.png', 'image', 'png', 202, '1158276f8e3d4104fcf9752a8eb258ac'),
(38, NULL, NULL, 'user', 1, 1, '2016-01-20 11:04:45', '2016-01-20 11:04:45', 1, 'public/user/26/0026_b395.png', 'png', 'm_php5BE7.tmp.png', 'image', 'png', 282, 'ccd5b395f5bcb93b53c472a7365c0c79'),
(39, 38, 'thumb.normal', 'user', 1, 1, '2016-01-20 11:04:45', '2016-01-20 11:04:45', 1, 'public/user/27/0027_b395.png', 'png', 't_php5BE7.tmp.png', 'image', 'png', 282, 'ccd5b395f5bcb93b53c472a7365c0c79'),
(40, NULL, NULL, 'user', 1, 1, '2016-01-20 11:05:37', '2016-01-20 11:05:37', 1, 'public/user/28/0028_7f7b.png', 'png', 'm_php27AF.tmp.png', 'image', 'png', 305, 'ab7a7f7b02b692fe5ebce67086bcb729'),
(41, 40, 'thumb.normal', 'user', 1, 1, '2016-01-20 11:05:37', '2016-01-20 11:05:37', 1, 'public/user/29/0029_7f7b.png', 'png', 't_php27AF.tmp.png', 'image', 'png', 305, 'ab7a7f7b02b692fe5ebce67086bcb729'),
(42, NULL, NULL, 'user', 1, 1, '2016-01-20 11:06:27', '2016-01-20 11:06:27', 1, 'public/user/2a/002a_a506.png', 'png', 'm_phpEA82.tmp.png', 'image', 'png', 312, '8539a506666431b2c8d467d1280809b2'),
(43, 42, 'thumb.normal', 'user', 1, 1, '2016-01-20 11:06:27', '2016-01-20 11:06:27', 1, 'public/user/2b/002b_a506.png', 'png', 't_phpEA82.tmp.png', 'image', 'png', 312, '8539a506666431b2c8d467d1280809b2');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_storage_mirrors`
--

CREATE TABLE IF NOT EXISTS `engine4_storage_mirrors` (
  `file_id` bigint(20) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`file_id`,`service_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_storage_services`
--

CREATE TABLE IF NOT EXISTS `engine4_storage_services` (
  `service_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `servicetype_id` int(10) unsigned NOT NULL,
  `config` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `engine4_storage_services`
--

INSERT INTO `engine4_storage_services` (`service_id`, `servicetype_id`, `config`, `enabled`, `default`) VALUES
(1, 1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_storage_servicetypes`
--

CREATE TABLE IF NOT EXISTS `engine4_storage_servicetypes` (
  `servicetype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `plugin` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `form` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`servicetype_id`),
  UNIQUE KEY `plugin` (`plugin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `engine4_storage_servicetypes`
--

INSERT INTO `engine4_storage_servicetypes` (`servicetype_id`, `title`, `plugin`, `form`, `enabled`) VALUES
(1, 'Local Storage', 'Storage_Service_Local', 'Storage_Form_Admin_Service_Local', 1),
(2, 'Database Storage', 'Storage_Service_Db', 'Storage_Form_Admin_Service_Db', 0),
(3, 'Amazon S3', 'Storage_Service_S3', 'Storage_Form_Admin_Service_S3', 1),
(4, 'Virtual File System', 'Storage_Service_Vfs', 'Storage_Form_Admin_Service_Vfs', 1),
(5, 'Round-Robin', 'Storage_Service_RoundRobin', 'Storage_Form_Admin_Service_RoundRobin', 0),
(6, 'Mirrored', 'Storage_Service_Mirrored', 'Storage_Form_Admin_Service_Mirrored', 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_users`
--

CREATE TABLE IF NOT EXISTS `engine4_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayname` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photo_id` int(11) unsigned NOT NULL DEFAULT '0',
  `status` text COLLATE utf8_unicode_ci,
  `status_date` datetime DEFAULT NULL,
  `password` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `salt` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'auto',
  `language` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'en_US',
  `timezone` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'America/Los_Angeles',
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `show_profileviewers` tinyint(1) NOT NULL DEFAULT '1',
  `level_id` int(11) unsigned NOT NULL,
  `invites_used` int(11) unsigned NOT NULL DEFAULT '0',
  `extra_invites` int(11) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `creation_ip` varbinary(16) NOT NULL,
  `modified_date` datetime NOT NULL,
  `lastlogin_date` datetime DEFAULT NULL,
  `lastlogin_ip` varbinary(16) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `member_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL` (`email`),
  UNIQUE KEY `USERNAME` (`username`),
  KEY `MEMBER_COUNT` (`member_count`),
  KEY `CREATION_DATE` (`creation_date`),
  KEY `search` (`search`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `engine4_users`
--

INSERT INTO `engine4_users` (`user_id`, `email`, `username`, `displayname`, `photo_id`, `status`, `status_date`, `password`, `salt`, `locale`, `language`, `timezone`, `search`, `show_profileviewers`, `level_id`, `invites_used`, `extra_invites`, `enabled`, `verified`, `approved`, `creation_date`, `creation_ip`, `modified_date`, `lastlogin_date`, `lastlogin_ip`, `update_date`, `member_count`, `view_count`) VALUES
(1, 'vijay@amutechnologies.com', 'admin', 'vijay patel', 1, '<a href="https://www.youtube.com/watch?v=OU8ulREBIiw" target="_blank" rel="nofollow">https://www.youtube.com/watch?v=OU8ulREBIiw</a>', '2016-01-05 05:01:01', 'dd915acf5341e233fe0e2b565cfa3b13', '3345639', 'auto', 'en_US', 'Asia/Dili', 1, 1, 1, 0, 0, 1, 1, 1, '2016-01-01 09:44:00', '2057899569', '2016-01-05 05:01:02', '2016-02-08 05:47:40', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', NULL, 1, 18),
(2, 'mehul@amutechnologies.com', 'mehul', 'mehul patel', 5, NULL, NULL, '40ab836c3d79f1a4900499b6edeed06a', '9577501', 'English', 'English', 'Asia/Calcutta', 1, 1, 4, 0, 0, 1, 1, 1, '2016-01-01 10:25:13', 'z�1', '2016-01-01 10:26:49', '2016-02-02 06:53:58', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', NULL, 1, 9),
(3, 'pushapk@amutechnologies.com', 'pushpak', 'pushpak zala', 10, NULL, NULL, '14458b94a5ff51bfe0e8f44763091dad', '3586456', 'English', 'English', 'Asia/Calcutta', 1, 1, 4, 0, 0, 1, 1, 1, '2016-01-07 07:23:31', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-07 07:23:34', '2016-01-07 07:23:34', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', NULL, 0, 3),
(4, 'mayank@amutechnologies.com', 'mayank', 'mayank patel', 0, NULL, NULL, 'bac3256ebc219f3fe7817c8c180d0a60', '9989288', 'English', 'English', 'Asia/Calcutta', 1, 1, 4, 0, 0, 1, 1, 1, '2016-01-25 06:16:19', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-25 06:16:20', '2016-01-25 06:16:21', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', NULL, 0, 11);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_block`
--

CREATE TABLE IF NOT EXISTS `engine4_user_block` (
  `user_id` int(11) unsigned NOT NULL,
  `blocked_user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`blocked_user_id`),
  KEY `REVERSE` (`blocked_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_facebook`
--

CREATE TABLE IF NOT EXISTS `engine4_user_facebook` (
  `user_id` int(11) unsigned NOT NULL,
  `facebook_uid` bigint(20) unsigned NOT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `facebook_uid` (`facebook_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_fields_maps`
--

CREATE TABLE IF NOT EXISTS `engine4_user_fields_maps` (
  `field_id` int(11) unsigned NOT NULL,
  `option_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  `order` smallint(6) NOT NULL,
  PRIMARY KEY (`field_id`,`option_id`,`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_user_fields_maps`
--

INSERT INTO `engine4_user_fields_maps` (`field_id`, `option_id`, `child_id`, `order`) VALUES
(0, 0, 1, 1),
(1, 1, 2, 2),
(1, 1, 3, 3),
(1, 1, 4, 4),
(1, 1, 5, 5),
(1, 1, 6, 6),
(1, 1, 7, 7),
(1, 1, 8, 8),
(1, 1, 9, 9),
(1, 1, 10, 10),
(1, 1, 11, 11),
(1, 1, 12, 12),
(1, 1, 13, 13);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_fields_meta`
--

CREATE TABLE IF NOT EXISTS `engine4_user_fields_meta` (
  `field_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `label` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `display` tinyint(1) unsigned NOT NULL,
  `publish` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `order` smallint(3) unsigned NOT NULL DEFAULT '999',
  `config` text COLLATE utf8_unicode_ci,
  `validators` text COLLATE utf8_unicode_ci,
  `filters` text COLLATE utf8_unicode_ci,
  `style` text COLLATE utf8_unicode_ci,
  `error` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `engine4_user_fields_meta`
--

INSERT INTO `engine4_user_fields_meta` (`field_id`, `type`, `label`, `description`, `alias`, `required`, `display`, `publish`, `search`, `show`, `order`, `config`, `validators`, `filters`, `style`, `error`) VALUES
(1, 'profile_type', 'Profile Type', '', 'profile_type', 1, 0, 0, 2, 1, 999, '', NULL, NULL, NULL, NULL),
(2, 'heading', 'Personal Information', '', '', 0, 1, 0, 0, 1, 999, '', NULL, NULL, NULL, NULL),
(3, 'first_name', 'First Name', '', 'first_name', 1, 1, 0, 2, 1, 999, '', '[["StringLength",false,[1,32]]]', NULL, NULL, NULL),
(4, 'last_name', 'Last Name', '', 'last_name', 1, 1, 0, 2, 1, 999, '', '[["StringLength",false,[1,32]]]', NULL, NULL, NULL),
(5, 'gender', 'Gender', '', 'gender', 0, 1, 0, 1, 1, 999, '', NULL, NULL, NULL, NULL),
(6, 'birthdate', 'Birthday', '', 'birthdate', 0, 1, 0, 1, 1, 999, '', NULL, NULL, NULL, NULL),
(7, 'heading', 'Contact Information', '', '', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, NULL, NULL),
(8, 'website', 'Website', '', 'website', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, '', ''),
(9, 'twitter', 'Twitter', '', 'twitter', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, '', ''),
(10, 'facebook', 'Facebook', '', 'facebook', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, '', ''),
(11, 'aim', 'AIM', '', 'aim', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, '', ''),
(12, 'heading', 'Personal Details', '', '', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, NULL, NULL),
(13, 'about_me', 'About Me', '', 'about_me', 0, 1, 0, 0, 0, 999, '[]', NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_fields_options`
--

CREATE TABLE IF NOT EXISTS `engine4_user_fields_options` (
  `option_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(11) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '999',
  PRIMARY KEY (`option_id`),
  KEY `field_id` (`field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `engine4_user_fields_options`
--

INSERT INTO `engine4_user_fields_options` (`option_id`, `field_id`, `label`, `order`) VALUES
(1, 1, 'Regular Member', 1),
(2, 5, 'Male', 1),
(3, 5, 'Female', 2);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_fields_search`
--

CREATE TABLE IF NOT EXISTS `engine4_user_fields_search` (
  `item_id` int(11) unsigned NOT NULL,
  `profile_type` smallint(11) unsigned DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` smallint(6) unsigned DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `profile_type` (`profile_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `gender` (`gender`),
  KEY `birthdate` (`birthdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_user_fields_search`
--

INSERT INTO `engine4_user_fields_search` (`item_id`, `profile_type`, `first_name`, `last_name`, `gender`, `birthdate`) VALUES
(1, 1, 'vijay', 'patel', 2, '1990-05-05'),
(2, 1, 'mehul', 'patel', 2, '1990-07-03'),
(3, 1, 'pushpak', 'zala', 2, '1991-01-01'),
(4, 1, 'mayank', 'patel', 2, '1983-03-03');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_fields_values`
--

CREATE TABLE IF NOT EXISTS `engine4_user_fields_values` (
  `item_id` int(11) unsigned NOT NULL,
  `field_id` int(11) unsigned NOT NULL,
  `index` smallint(3) unsigned NOT NULL DEFAULT '0',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `privacy` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`item_id`,`field_id`,`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_user_fields_values`
--

INSERT INTO `engine4_user_fields_values` (`item_id`, `field_id`, `index`, `value`, `privacy`) VALUES
(1, 1, 0, '1', NULL),
(1, 3, 0, 'vijay', 'everyone'),
(1, 4, 0, 'patel', 'everyone'),
(1, 5, 0, '2', 'everyone'),
(1, 6, 0, '1990-5-5', 'everyone'),
(1, 8, 0, '', 'everyone'),
(1, 9, 0, '', 'everyone'),
(1, 10, 0, '', 'everyone'),
(1, 11, 0, '', 'everyone'),
(1, 13, 0, 'hello amu...', 'everyone'),
(2, 1, 0, '1', NULL),
(2, 3, 0, 'mehul', NULL),
(2, 4, 0, 'patel', NULL),
(2, 5, 0, '2', NULL),
(2, 6, 0, '1990-7-3', NULL),
(2, 8, 0, '', NULL),
(2, 9, 0, '', NULL),
(2, 10, 0, '', NULL),
(2, 11, 0, '', NULL),
(2, 13, 0, '', NULL),
(3, 1, 0, '1', NULL),
(3, 3, 0, 'pushpak', NULL),
(3, 4, 0, 'zala', NULL),
(3, 5, 0, '2', NULL),
(3, 6, 0, '1991-1-1', NULL),
(3, 8, 0, '', NULL),
(3, 9, 0, '', NULL),
(3, 10, 0, '', NULL),
(3, 11, 0, '', NULL),
(3, 13, 0, 'helllo', NULL),
(4, 1, 0, '1', NULL),
(4, 3, 0, 'mayank', NULL),
(4, 4, 0, 'patel', NULL),
(4, 5, 0, '2', NULL),
(4, 6, 0, '1983-3-3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_forgot`
--

CREATE TABLE IF NOT EXISTS `engine4_user_forgot` (
  `user_id` int(11) unsigned NOT NULL,
  `code` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_janrain`
--

CREATE TABLE IF NOT EXISTS `engine4_user_janrain` (
  `user_id` int(11) unsigned NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_listitems`
--

CREATE TABLE IF NOT EXISTS `engine4_user_listitems` (
  `listitem_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_id` int(11) unsigned NOT NULL,
  `child_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`listitem_id`),
  KEY `list_id` (`list_id`),
  KEY `child_id` (`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_lists`
--

CREATE TABLE IF NOT EXISTS `engine4_user_lists` (
  `list_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `owner_id` int(11) unsigned NOT NULL,
  `child_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_logins`
--

CREATE TABLE IF NOT EXISTS `engine4_user_logins` (
  `login_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varbinary(16) NOT NULL,
  `timestamp` datetime NOT NULL,
  `state` enum('success','no-member','bad-password','disabled','unpaid','third-party','v3-migration','unknown') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `source` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`login_id`),
  KEY `user_id` (`user_id`),
  KEY `email` (`email`),
  KEY `ip` (`ip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Dumping data for table `engine4_user_logins`
--

INSERT INTO `engine4_user_logins` (`login_id`, `user_id`, `email`, `ip`, `timestamp`, `state`, `source`, `active`) VALUES
(1, 1, 'vijay@amutechnologies.com', 'z�1', '2016-01-01 10:07:01', 'success', NULL, 0),
(2, 1, 'vijay@amutechnologies.com', 'z�1', '2016-01-01 10:21:05', 'bad-password', NULL, 0),
(3, 1, 'vijay@amutechnologies.com', 'z�1', '2016-01-01 10:21:13', 'success', NULL, 0),
(4, 1, 'vijay@amutechnologies.com', 'z�1', '2016-01-04 04:49:11', 'success', NULL, 1),
(5, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-04 11:44:43', 'success', NULL, 0),
(6, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-04 12:13:50', 'success', NULL, 1),
(7, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-05 04:58:39', 'success', NULL, 1),
(8, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-12 05:16:04', 'success', NULL, 1),
(9, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-13 06:18:13', 'success', NULL, 1),
(10, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-18 04:51:25', 'success', NULL, 1),
(11, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-19 15:07:38', 'success', NULL, 1),
(12, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-20 06:13:08', 'success', NULL, 1),
(13, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-21 04:41:26', 'success', NULL, 0),
(14, 2, 'mehul@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-21 05:19:08', 'success', NULL, 0),
(15, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-21 05:20:09', 'success', NULL, 0),
(16, 2, 'mehul@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-21 13:53:45', 'success', NULL, 0),
(17, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-21 13:54:42', 'success', NULL, 1),
(18, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-22 04:54:21', 'success', NULL, 1),
(19, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-22 14:05:42', 'success', NULL, 0),
(20, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-23 05:00:59', 'success', NULL, 1),
(21, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-25 05:48:04', 'success', NULL, 0),
(22, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-25 06:45:57', 'success', NULL, 0),
(23, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-25 09:43:54', 'success', NULL, 1),
(24, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-25 10:35:03', 'success', NULL, 0),
(25, NULL, 'viajy@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-27 03:45:21', 'no-member', NULL, 0),
(26, NULL, 'viajy@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-27 03:45:40', 'no-member', NULL, 0),
(27, NULL, 'viajy@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-27 03:46:22', 'no-member', NULL, 0),
(28, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-27 03:47:27', 'success', NULL, 1),
(29, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-28 03:57:35', 'success', NULL, 1),
(30, 2, 'mehul@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-28 04:56:59', 'success', NULL, 1),
(31, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-31 05:16:24', 'success', NULL, 1),
(32, NULL, 'vujay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-31 05:19:50', 'no-member', NULL, 0),
(33, 2, 'mehul@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-01-31 05:20:12', 'success', NULL, 1),
(34, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-01 04:37:21', 'success', NULL, 0),
(35, 2, 'mehul@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-01 04:38:47', 'success', NULL, 1),
(36, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-01 04:46:21', 'success', NULL, 1),
(37, 2, 'mehul@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-02 06:53:57', 'success', NULL, 0),
(38, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-02 06:57:28', 'success', NULL, 1),
(39, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-03 04:36:14', 'success', NULL, 1),
(40, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-03 14:07:55', 'success', NULL, 1),
(41, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-05 04:35:25', 'success', NULL, 1),
(42, 1, 'vijay@amutechnologies.com', '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '2016-02-08 05:47:40', 'success', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_membership`
--

CREATE TABLE IF NOT EXISTS `engine4_user_membership` (
  `resource_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `resource_approved` tinyint(1) NOT NULL DEFAULT '0',
  `user_approved` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`resource_id`,`user_id`),
  KEY `REVERSE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_user_membership`
--

INSERT INTO `engine4_user_membership` (`resource_id`, `user_id`, `active`, `resource_approved`, `user_approved`, `message`, `description`) VALUES
(1, 2, 1, 1, 1, NULL, NULL),
(2, 1, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_online`
--

CREATE TABLE IF NOT EXISTS `engine4_user_online` (
  `ip` varbinary(16) NOT NULL,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `active` datetime NOT NULL,
  PRIMARY KEY (`ip`,`user_id`),
  KEY `LOOKUP` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `engine4_user_online`
--

INSERT INTO `engine4_user_online` (`ip`, `user_id`, `active`) VALUES
('\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 0, '2016-02-08 09:18:23'),
('\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 1, '2016-02-08 09:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_settings`
--

CREATE TABLE IF NOT EXISTS `engine4_user_settings` (
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_signup`
--

CREATE TABLE IF NOT EXISTS `engine4_user_signup` (
  `signup_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '999',
  `enable` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`signup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `engine4_user_signup`
--

INSERT INTO `engine4_user_signup` (`signup_id`, `class`, `order`, `enable`) VALUES
(1, 'User_Plugin_Signup_Account', 2, 1),
(2, 'User_Plugin_Signup_Fields', 3, 1),
(3, 'User_Plugin_Signup_Photo', 4, 1),
(4, 'User_Plugin_Signup_Invite', 5, 0),
(5, 'Payment_Plugin_Signup_Subscription', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_twitter`
--

CREATE TABLE IF NOT EXISTS `engine4_user_twitter` (
  `user_id` int(10) unsigned NOT NULL,
  `twitter_uid` bigint(20) unsigned NOT NULL,
  `twitter_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `twitter_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `twitter_uid` (`twitter_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_user_verify`
--

CREATE TABLE IF NOT EXISTS `engine4_user_verify` (
  `user_id` int(11) unsigned NOT NULL,
  `code` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_video_categories`
--

CREATE TABLE IF NOT EXISTS `engine4_video_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `category_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `engine4_video_categories`
--

INSERT INTO `engine4_video_categories` (`category_id`, `user_id`, `category_name`) VALUES
(1, 0, 'Autos & Vehicles'),
(2, 0, 'Comedy'),
(3, 0, 'Education'),
(4, 0, 'Entertainment'),
(5, 0, 'Film & Animation'),
(6, 0, 'Gaming'),
(7, 0, 'Howto & Style'),
(8, 0, 'Music'),
(9, 0, 'News & Politics'),
(10, 0, 'Nonprofits & Activism'),
(11, 0, 'People & Blogs'),
(12, 0, 'Pets & Animals'),
(13, 0, 'Science & Technology'),
(14, 0, 'Sports'),
(15, 0, 'Travel & Events');

-- --------------------------------------------------------

--
-- Table structure for table `engine4_video_ratings`
--

CREATE TABLE IF NOT EXISTS `engine4_video_ratings` (
  `video_id` int(10) unsigned NOT NULL,
  `user_id` int(9) unsigned NOT NULL,
  `rating` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`video_id`,`user_id`),
  KEY `INDEX` (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `engine4_video_videos`
--

CREATE TABLE IF NOT EXISTS `engine4_video_videos` (
  `video_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '1',
  `owner_type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `parent_type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL,
  `code` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `photo_id` int(11) unsigned DEFAULT NULL,
  `rating` float NOT NULL,
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `duration` int(9) unsigned NOT NULL,
  `rotation` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`video_id`),
  KEY `owner_id` (`owner_id`,`owner_type`),
  KEY `search` (`search`),
  KEY `creation_date` (`creation_date`),
  KEY `view_count` (`view_count`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `engine4_video_videos`
--

INSERT INTO `engine4_video_videos` (`video_id`, `title`, `description`, `search`, `owner_type`, `owner_id`, `parent_type`, `parent_id`, `creation_date`, `modified_date`, `view_count`, `comment_count`, `type`, `code`, `photo_id`, `rating`, `category_id`, `status`, `file_id`, `duration`, `rotation`) VALUES
(1, 'Philips Presents: The Longest Night', 'Páll Pálsson has been a man of the sea for 36 years. With a life of fishing, comes a life of little sleep. Research shows that nearly half of Icelandic fishermen suffer from sleep-related disorders such as chronic fatigue and apnea. <br />  <br />  Executive Producers: Kaylee King-Balentine, Steven Richardson<br />  Producers: Raquel Bubar, Rupert Savage<br />  Post Producer: Lauren Tyson<br />  Production Assistants: Jenna Haddaway, Sarah Thomas<br />  Fixer: Daddi Haraldur<br />  Director: Gregory Hackett<br />  Focus Puller: Jon England<br />  Camera Assistant: Carolina Salas<br />  Drone Operator: Ozzo Photography<br />  1st Assistant Director: Steve Wingrove<br />  Photographer: Tristan Conor Holden<br />  Editor: Tim Swaby<br />  Grade: The Mill<br />  Composer: Rob Law<br />  Sound Designer: Martin Leitner, Wave Studios<br />  Produced by: T Brand Studio and Spindle<br />  Special Thanks: Páll Pálsson', 1, '', 1, NULL, NULL, '2016-01-01 12:29:13', '2016-01-01 12:29:16', 0, 1, 2, '148855955', 9, 0, 0, 1, 0, 228, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
